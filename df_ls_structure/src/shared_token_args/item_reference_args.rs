use df_ls_core::{Choose, Reference};
use df_ls_diagnostics::{hash_map, DiagnosticsInfo};
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct ItemReferenceArg {
    pub item_type: Reference, // ReferenceTo<ItemToken>
    pub item_subtype: Choose<NoSubtypeEnum, Reference>,
}

// Deserialize a token with following pattern: `[REF:item_type:item_subtype]`
df_ls_syntax_analysis::token_deserialize_unary_token!(ItemReferenceArg);

impl TryFromArgumentGroup for ItemReferenceArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Arg 0
        let item_type = Reference::try_from_argument_group(
            &mut token,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )?;
        // Arg 1
        let item_subtype = Choose::<NoSubtypeEnum, Reference>::try_from_argument_group(
            &mut token,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )?;

        Ok(ItemReferenceArg {
            item_type,
            item_subtype,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NoSubtypeEnum {
    #[token_de(token = "NONE", alias = "NO_SUBTYPE")]
    None,
}
impl Default for NoSubtypeEnum {
    fn default() -> Self {
        Self::None
    }
}
