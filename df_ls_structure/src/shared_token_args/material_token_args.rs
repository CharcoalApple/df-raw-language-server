use crate::{
    CreatureToken, InorganicToken, NoMatGlossEnum, NoneEnum, PlantToken, ReactionToken,
    ReagentToken,
};
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::{hash_map, DiagnosticsInfo};
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

/// Wiki page: https://dwarffortresswiki.org/index.php/Material_token
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct MaterialTokenArg {
    // |---0------|---------1--------|----2----|---3-----|
    // [TOKEN_NAME:LOCAL_CREATURE_MAT          :MUSCLE   ]
    // [TOKEN_NAME:PLANT_MAT         :APPLE    :FRUIT    ]
    // [TOKEN_NAME:CREATURE_MAT      :ANIMAL   :WOOL     ]
    // [TOKEN_NAME:INORGANIC                   :QUICKLIME]
    // [TOKEN_NAME:LYE                                   ]
    /// Argument group 1: with Enum arguments
    pub material: MaterialTypeEnum,
}

// Deserialize a token with following pattern: `[REF:material_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(MaterialTokenArg);

impl TryFromArgumentGroup for MaterialTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        // Arg 0
        let reference_arg0 = Reference::try_from_argument_group(
            &mut token,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )?;
        // Match on Arg 0
        let material_type = match reference_arg0.0.as_ref() {
            "INORGANIC" | "STONE" | "METAL" => {
                // TODO: add alias warning
                // Arg 1
                let material_name = ReferenceTo::<InorganicToken>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Inorganic(material_name)
            }
            "CREATURE_MAT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<CreatureToken>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name = Reference::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::CreatureMat((material_object_id, material_name))
            }
            "LOCAL_CREATURE_MAT" => {
                // Arg 1
                let material_name = Reference::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::LocalCreatureMat(material_name)
            }
            "PLANT_MAT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<PlantToken>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name = Reference::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::PlantMat((material_object_id, material_name))
            }
            "LOCAL_PLANT_MAT" => {
                // Arg 1
                let material_name = Reference::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::LocalPlantMat(material_name)
            }
            "GET_MATERIAL_FROM_REAGENT" => {
                // Arg 1
                let material_object_id = ReferenceTo::<ReactionToken>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                // Arg 2
                let material_name =
                    Choose::<NoneEnum, ReferenceTo<ReagentToken>>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                MaterialTypeEnum::GetMaterialFromReagent((material_object_id, material_name))
            }
            "AMBER" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Amber(material_name)
            }
            "CORAL" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Coral(material_name)
            }
            "GLASS_GREEN" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassGreen(material_name)
            }
            "GLASS_CLEAR" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassClear(material_name)
            }
            "GLASS_CRYSTAL" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::GlassCrystal(material_name)
            }
            "WATER" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Water(material_name)
            }
            "COAL" => {
                // Arg 1
                let material_name = CoalMaterialEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Coal(material_name)
            }
            "POTASH" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Potash(material_name)
            }
            "LYE" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Lye(material_name)
            }
            "ASH" => {
                // Arg 1
                let material_name =
                    Option::<Choose<NoneEnum, NoMatGlossEnum>>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                MaterialTypeEnum::Ash(material_name)
            }
            "PEARLASH" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Pearlash(material_name)
            }
            "MUD" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Mud(material_name)
            }
            "VOMIT" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Vomit(material_name)
            }
            "SALT" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Salt(material_name)
            }
            "FILTH_B" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::FilthB(material_name)
            }
            "FILTH_Y" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::FilthY(material_name)
            }
            "UNKNOWN_SUBSTANCE" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::UnknownSubstance(material_name)
            }
            "GRIME" => {
                // Arg 1
                let material_name = Option::<NoneEnum>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                MaterialTypeEnum::Grime(material_name)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec![
                        "INORGANIC",
                        "STONE",
                        "METAL",
                        "CREATURE_MAT",
                        "LOCAL_CREATURE_MAT",
                        "PLANT_MAT",
                        "LOCAL_PLANT_MAT",
                        "GET_MATERIAL_FROM_REAGENT",
                        "AMBER",
                        "CORAL",
                        "GLASS_GREEN",
                        "GLASS_CLEAR",
                        "GLASS_CRYSTAL",
                        "WATER",
                        "COAL",
                        "POTASH",
                        "LYE",
                        "ASH",
                        "PEARLASH",
                        "MUD",
                        "VOMIT",
                        "SALT",
                        "FILTH_B",
                        "FILTH_Y",
                        "UNKNOWN_SUBSTANCE",
                        "GRIME",
                    ],
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(MaterialTokenArg {
            material: material_type,
        })
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
/// The shape of the tissue
pub enum MaterialTypeEnum {
    /// Specifies a standalone inorganic material defined in the raws, generally a stone or metal.
    /// For example, `INORGANIC:IRON` refers to iron, and `INORGANIC:CERAMIC_PORCELAIN`
    /// refers to porcelain. The material name can be substituted with `USE_LAVA_STONE`
    /// to automatically select the local lava stone, which is normally obsidian.
    // #[token_de(token = "INORGANIC", alias = "STONE", alias = "METAL")]
    Inorganic(ReferenceTo<InorganicToken>),
    /// Specifies a material associated with a specific creature.
    /// Examples: `CREATURE_MAT:DWARF:SKIN` refers to dwarf skin.
    // #[token_de(token = "CREATURE_MAT")]
    CreatureMat((ReferenceTo<CreatureToken>, Reference)),
    /// Alias for `CREATURE_MAT:CREATURE_ID:MATERIAL_NAME`,
    /// where `CREATURE_ID` is the creature currently being defined;
    /// as such, it can only be used in creature definitions.
    // #[token_de(token = "LOCAL_CREATURE_MAT")]
    LocalCreatureMat(Reference),
    /// Specifies a material associated with a specific plant.
    /// Example: `PLANT_MAT:BUSH_QUARRY:LEAF` refers to quarry bush leaves.
    // #[token_de(token = "PLANT_MAT")]
    PlantMat((ReferenceTo<PlantToken>, Reference)),
    /// Alias for `PLANT_MAT:PLANT_ID:MATERIAL_NAME`,
    /// where `PLANT_ID` is the plant currently being defined;
    /// as such, it can only be used in plant definitions.
    // #[token_de(token = "LOCAL_PLANT_MAT")]
    LocalPlantMat(Reference),
    /// Specifies a material related to a reagent's material within a reaction.
    /// `REAGENT_ID` must match a `[REAGENT]`, and `REACTION_PRODUCT_ID` must either match a
    /// `[MATERIAL_REACTION_PRODUCT]` belonging to the reagent's material
    /// or be equal to `NONE` to use the reagent's material itself.
    // #[token_de(token = "GET_MATERIAL_FROM_REAGENT")]
    GetMaterialFromReagent(
        (
            ReferenceTo<ReactionToken>,
            Choose<NoneEnum, ReferenceTo<ReagentToken>>,
        ),
    ),
    //-----------Hardcoded materials--------
    /// Specifies one of the hardcoded materials.
    /// Amber is a type of material made from fossilized tree resin.
    // #[token_de(token = "AMBER")]
    Amber(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Coral is a type of material composed of the dead remains of corals,
    /// creatures that have not yet been implemented into the game.
    // #[token_de(token = "CORAL")]
    Coral(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_GREEN")]
    GlassGreen(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_CLEAR")]
    GlassClear(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[token_de(token = "GLASS_CRYSTAL")]
    GlassCrystal(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Water, when placed in buckets or when mining out ice.
    // #[token_de(token = "WATER")]
    Water(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Specifies a material that can be used as fuel - charcoal or coke.
    /// Specifying `NO_MATGLOSS` (not `NONE`) will make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    // #[token_de(token = "COAL")]
    Coal(CoalMaterialEnum),
    /// Specifies one of the hardcoded materials.
    /// Potash is a wood-based product which has applications in farming,
    /// as well as production of mid- and high-end glass products.
    // #[token_de(token = "POTASH")]
    Potash(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Ash is an intermediate good used to make potash, lye, or to glaze ceramics.
    // #[token_de(token = "ASH")]
    Ash(Option<Choose<NoneEnum, NoMatGlossEnum>>),
    /// Specifies one of the hardcoded materials.
    /// Pearlash is a wood-based product which is used primarily
    /// in the manufacture of clear and crystal glass.
    // #[token_de(token = "PEARLASH")]
    Pearlash(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Lye is a material used to make soap, and can also be used to make potash.
    // #[token_de(token = "LYE")]
    Lye(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Mud is a contaminant produced when an area is covered with water, and colors tiles brown.
    // #[token_de(token = "MUD")]
    Mud(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Under certain conditions, creatures (such as your dwarves) will vomit,
    /// creating a puddle of vomit.
    // #[token_de(token = "VOMIT")]
    Vomit(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Salt is a contaminant that makes oceanic water unsuitable for drinking.
    // #[token_de(token = "SALT")]
    Salt(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[token_de(token = "FILTH_B")]
    FilthB(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[token_de(token = "FILTH_Y")]
    FilthY(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Unknown substance is a hardcoded material that takes the form of a light gray liquid.
    /// No longer used in recent versions of game.
    // #[token_de(token = "UNKNOWN_SUBSTANCE")]
    UnknownSubstance(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Grime is a brown-colored contaminant that makes water from murky pools disgusting to drink.
    // #[token_de(token = "GRIME")]
    Grime(Option<NoneEnum>),
}
impl Default for MaterialTypeEnum {
    fn default() -> Self {
        Self::Inorganic(ReferenceTo::new(String::default()))
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum CoalMaterialEnum {
    #[token_de(token = "CHARCOAL")]
    Charcoal,
    #[token_de(token = "COKE")]
    Coke,
    /// Make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    #[token_de(token = "NO_MATGLOSS")]
    NoMatgloss,
}
impl Default for CoalMaterialEnum {
    fn default() -> Self {
        Self::Charcoal
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_material_correct() {
        let source = "header
            [REF:INORGANIC:IRON]
            [REF:STONE:SLADE]
            [REF:METAL:IRON]
            [REF:CREATURE_MAT:HONEY_BEE:HONEY]
            [REF:LOCAL_CREATURE_MAT:SKIN]
            [REF:PLANT_MAT:NETHER_CAP:WOOD]
            [REF:LOCAL_PLANT_MAT:LEAF]
            [REF:GET_MATERIAL_FROM_REAGENT:PLANT:PRESS_PAPER_MAT]
            [REF:GET_MATERIAL_FROM_REAGENT:BONE:NONE]

            [REF:AMBER:NONE]
            [REF:AMBER]
            [REF:CORAL:NONE]
            [REF:GLASS_GREEN:NONE]
            [REF:GLASS_GREEN]
            [REF:GLASS_CLEAR:NONE]
            [REF:GLASS_CRYSTAL:NONE]
            [REF:WATER:NONE]
            [REF:WATER]
            [REF:COAL:CHARCOAL]
            [REF:COAL:COKE]
            [REF:COAL:NO_MATGLOSS]
            [REF:POTASH:NONE]
            [REF:ASH:NONE] // Both look to be valid judging for RAW collection
            [REF:ASH:NO_MATGLOSS] // Both look to be valid judging for RAW collection
            [REF:PEARLASH:NONE]
            [REF:LYE]
            [REF:LYE:NONE]
            [REF:MUD]
            [REF:MUD:NONE]
            [REF:VOMIT]
            [REF:VOMIT:NONE]
            [REF:SALT:NONE]
            [REF:FILTH_B:NONE]
            [REF:FILTH_Y:NONE]
            [REF:UNKNOWN_SUBSTANCE:NONE]
            [REF:GRIME:NONE]
            ";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test `[REF:INORGANIC:IRON]` ---
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:STONE:SLADE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("SLADE".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:METAL:IRON]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Inorganic(ReferenceTo::new("IRON".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:CREATURE_MAT:HONEY_BEE:HONEY]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::CreatureMat((
                    ReferenceTo::new("HONEY_BEE".to_owned()),
                    Reference("HONEY".to_owned())
                )),
            },
            test_result
        );

        // ---- Test `[REF:LOCAL_CREATURE_MAT:SKIN]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::LocalCreatureMat(Reference("SKIN".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:PLANT_MAT:NETHER_CAP:WOOD]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::PlantMat((
                    ReferenceTo::new("NETHER_CAP".to_owned()),
                    Reference("WOOD".to_owned())
                )),
            },
            test_result
        );

        // ---- Test `[REF:LOCAL_PLANT_MAT:LEAF]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::LocalPlantMat(Reference("LEAF".to_owned())),
            },
            test_result
        );

        // ---- Test `[REF:GET_MATERIAL_FROM_REAGENT:PLANT:PRESS_PAPER_MAT]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GetMaterialFromReagent((
                    ReferenceTo::new("PLANT".to_owned()),
                    Choose::Choice2(ReferenceTo::new("PRESS_PAPER_MAT".to_owned()))
                )),
            },
            test_result
        );

        // ---- Test `[REF:GET_MATERIAL_FROM_REAGENT:BONE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GetMaterialFromReagent((
                    ReferenceTo::new("BONE".to_owned()),
                    Choose::Choice1(NoneEnum::None)
                )),
            },
            test_result
        );

        // ---- Test `[REF:AMBER:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Amber(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:AMBER]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Amber(None),
            },
            test_result
        );

        // ---- Test `[REF:CORAL:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coral(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_GREEN:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassGreen(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_GREEN]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassGreen(None),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_CLEAR:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassClear(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:GLASS_CRYSTAL:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::GlassCrystal(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:WATER:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Water(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:WATER]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Water(None),
            },
            test_result
        );

        // ---- Test `[REF:COAL:CHARCOAL]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::Charcoal),
            },
            test_result
        );

        // ---- Test `[REF:COAL:COKE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::Coke),
            },
            test_result
        );

        // ---- Test `[REF:COAL:NO_MATGLOSS]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Coal(CoalMaterialEnum::NoMatgloss),
            },
            test_result
        );

        // ---- Test `[REF:POTASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Potash(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:ASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Ash(Some(Choose::Choice1(NoneEnum::None))),
            },
            test_result
        );

        // ---- Test `[REF:ASH:NO_MATGLOSS]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Ash(Some(Choose::Choice2(NoMatGlossEnum::NoMatgloss))),
            },
            test_result
        );

        // ---- Test `[REF:PEARLASH:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Pearlash(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:LYE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Lye(None),
            },
            test_result
        );

        // ---- Test `[REF:LYE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Lye(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:MUD]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Mud(None),
            },
            test_result
        );

        // ---- Test `[REF:MUD:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Mud(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:VOMIT]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Vomit(None),
            },
            test_result
        );

        // ---- Test `[REF:VOMIT:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Vomit(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:SALT:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Salt(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:FILTH_B:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::FilthB(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:FILTH_Y:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::FilthY(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:UNKNOWN_SUBSTANCE:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::UnknownSubstance(Some(NoneEnum::None)),
            },
            test_result
        );

        // ---- Test `[REF:GRIME:NONE]` ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_result: MaterialTokenArg =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            MaterialTokenArg {
                material: MaterialTypeEnum::Grime(Some(NoneEnum::None)),
            },
            test_result
        );

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
