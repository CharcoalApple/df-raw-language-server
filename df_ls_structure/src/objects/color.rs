use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct ColorToken {
    /// Argument 1 of `COLOR`
    /// The reference for the Color in other RAW files and tokens
    #[token_de(token = "COLOR", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The name of the color.
    #[token_de(token = "NAME")]
    pub name: Option<String>,
    /// The word for the color, see the `WORD` token in `languages_X.txt`.
    #[token_de(token = "WORD")]
    pub word: Vec<ReferenceTo<crate::WordToken>>,
    /// The RGB (Red,Green,Blue) color value of the color.
    #[token_de(token = "RGB")]
    pub rgb: Option<(u8, u8, u8)>,
}
