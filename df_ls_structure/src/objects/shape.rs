use df_ls_core::{DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable,
)]
pub struct ShapeToken {
    /// Argument 1 of `[SHAPE:...]`
    #[token_de(token = "SHAPE", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The name of the shape. Is not always used all by itself, see `GEMS_USE_ADJ`.
    #[token_de(token = "NAME")]
    pub name: Option<(String, String)>,
    /// The tile the shape uses onscreen, as an engraving or as an item (gem, die).
    #[token_de(token = "TILE")]
    pub tile: Option<DFChar>,
    /// Makes gems in this shape use the syntax '`ADJ` + material' e.g. "conglomerate gizzard stone".
    /// This, `GEMS_USE_NOUN` or `GEMS_USE_ADJ_NOUN` must be used for the name of a gem in this shape to show up.
    #[token_de(token = "GEMS_USE_ADJ")]
    pub gems_use_adj: Option<()>,
    /// Makes gems in this shape use the syntax '`ADJ` + material + `NAME`' e.g. "smooth conglomerate cabochon".
    /// This, `GEMS_USE_ADJ` or `GEMS_USE_ADJ_NOUN` must be used for the name of a gem in this shape to show up.
    #[token_de(token = "GEMS_USE_ADJ_NOUN")]
    pub gems_use_adj_noun: Option<()>,
    /// Makes gems in this shape use the syntax 'material + `NAME`' e.g. "point cut conglomerate".
    /// This, `GEMS_USE_ADJ` or`GEMS_USE_ADJ_NOUN` must be used for the name of a gem in this shape to show up.
    #[token_de(token = "GEMS_USE_NOUN")]
    pub gems_use_noun: Option<()>,
    /// The amount of sides on the dice.
    #[token_de(token = "FACES")]
    pub faces: Option<u32>,
    /// Effect unknown.
    #[token_de(token = "WORD")]
    pub word: Option<ReferenceTo<crate::WordToken>>,
    /// An adjective to be paired with the name. Can be used multiple times,
    /// allowing for variants of the same shape e.g. "thin cross", "tall cross".
    #[token_de(token = "ADJ")]
    pub adj: Vec<String>,
    /// A category the shape belongs to, which can be used by the `TOOL` token `SHAPE_CATEGORY`.
    /// Vanilla categories are `SIMPLE`, `PLATONIC`, and `DICE`, but any arbitrary category name is allowed.
    #[token_de(token = "CATEGORY")]
    pub category: Vec<Reference>,
}
