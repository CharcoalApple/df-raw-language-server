use super::*;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::{
    LoopControl, Token, TokenDeserialize, TokenDeserializeBasics, TokenValue, TreeCursor,
};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[allow(clippy::upper_case_acronyms)]
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct DFRaw {
    pub header: String,
    pub object_tokens: Vec<ObjectToken>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq)]
#[token_de(second_par_check, token = "OBJECT")]
pub struct ObjectToken {
    #[token_de(token = "BODY")]
    pub body_tokens: Vec<BodyObjectToken>,
    #[token_de(token = "BODY_DETAIL_PLAN")]
    pub body_detail_plan_tokens: Vec<BodyDetailPlanToken>,
    #[token_de(token = "BUILDING")]
    pub building_tokens: Vec<BuildingToken>,
    #[token_de(token = "CREATURE")]
    pub creature_tokens: Vec<CreatureToken>,
    #[token_de(token = "CREATURE_VARIATION")]
    pub creature_variation_tokens: Vec<CreatureVariationToken>,
    #[token_de(token = "DESCRIPTOR_COLOR")]
    pub color_tokens: Vec<ColorToken>,
    #[token_de(token = "DESCRIPTOR_PATTERN")]
    pub pattern_tokens: Vec<PatternToken>,
    #[token_de(token = "DESCRIPTOR_SHAPE")]
    pub shape_tokens: Vec<ShapeToken>,
    #[token_de(token = "ENTITY")]
    pub entity_tokens: Vec<EntityToken>,
    #[token_de(token = "GRAPHICS")]
    pub graphics_tokens: Vec<GraphicsToken>,
    #[token_de(token = "INTERACTION")]
    pub interaction_tokens: Vec<InteractionToken>,
    #[token_de(token = "INORGANIC")]
    pub inorganic_tokens: Vec<InorganicToken>,
    #[token_de(token = "ITEM")]
    pub item_tokens: Vec<ItemToken>,
    #[token_de(token = "LANGUAGE")]
    pub language_tokens: Vec<LanguageToken>,
    #[token_de(token = "MATERIAL_TEMPLATE")]
    pub material_tokens: Vec<MaterialToken>,
    #[token_de(token = "PLANT")]
    pub plant_tokens: Vec<PlantToken>,
    #[token_de(token = "REACTION")]
    pub reaction_tokens: Vec<ReactionToken>,
    #[token_de(token = "TISSUE_TEMPLATE")]
    pub tissue_template_tokens: Vec<TissueToken>,
}

impl TokenDeserialize for DFRaw {
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        let mut df_raw = DFRaw::default();
        // Keep track of if the first object token was deserialized.
        let mut first_object_deserialized = false;
        loop {
            let node = cursor.node();
            match node.kind().as_ref() {
                "header" => {
                    if let Ok(value) = TokenDeserializeBasics::deserialize_tokens(
                        &mut cursor,
                        &source,
                        &mut diagnostics,
                    ) {
                        df_raw.header = value
                    }
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "general_token" => {
                    // if first token was already correctly deserialized,
                    // we are on the second `[OBJECT:..]` token or some other token
                    if first_object_deserialized {
                        // Mark current node as `unknown_token`
                        let token =
                            Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: node.get_range(),
                                message_template_data: hash_map! {
                                    "token_name" => token.get_current_arg_opt().unwrap().value.to_string(),
                                },
                            },
                            "unknown_token",
                        );
                        df_ls_syntax_analysis::mark_rest_of_file_as_unchecked(
                            &mut cursor,
                            &mut diagnostics,
                            &node,
                        );
                        break;
                    }
                    if let Ok(value) =
                        TokenDeserialize::deserialize_tokens(&mut cursor, &source, &mut diagnostics)
                    {
                        df_raw.object_tokens = value;
                        first_object_deserialized = true;
                        // Only go to next sibling if there is one, if none: break.
                        // We have reached the end of the file, so have to go up the stack
                        let new_node = cursor.node();
                        if new_node.next_sibling().is_none() {
                            cursor.goto_parent();
                            break;
                        }
                        // If node did not change: break
                        // This will prevent infinite loops
                        if new_node == node {
                            break;
                        }
                        continue;
                    } else {
                        return Err(());
                    }
                }
                "comment" => {
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "EOF" => break,
                others => {
                    error!("Found an unknown node of kind: {}", others);
                    break;
                }
            }
            // If node did not change: break
            // This will prevent infinite loops
            let new_node = cursor.node();
            if new_node == node {
                break;
            }
        }
        Ok(df_raw)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}
