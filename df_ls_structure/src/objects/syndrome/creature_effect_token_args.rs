use crate::{
    BodyAttributeEnum, BpCriteriaTokenArg, CasteFlagEnum, CreatureFlagEnum, CreatureToken,
    FacetEnum, InteractionToken, PersonalityTraitEnum, SoulAttributeEnum,
    SynTransmittionMethodEnum, TargetPropertyEnum,
};
use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

// region: CE args for CE_BODY_MAT_INTERACTION ====================================================
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum CeBodyMatInteractionTokenArg {
    /// Used to specify what must be done with the body material to trigger the interaction.
    /// Multiple instances of this tag may be used to specify different valid transmission routes.
    ///
    /// However, even though it should accept `SYN_INGESTED`,`SYN_INJECTED`, `SYN_CONTACT`, or
    /// `SYN_INHALED`, `SYN_INGESTED` appears to be the only one that works at present.
    SyndromeTag(SynTransmittionMethodEnum),
    /// Used to specify which interaction is to be run. Appropriate interaction effects
    /// with a creature target (such as `ADD_SYNDROME`) will be inflicted upon the unit who interacts
    /// with the body material as specified above.
    ///
    /// Note that the linked interaction must have an `[I_SOURCE:INGESTION]` token for this to work.
    Interaction(ReferenceTo<InteractionToken>),
}
impl Default for CeBodyMatInteractionTokenArg {
    fn default() -> Self {
        Self::SyndromeTag(SynTransmittionMethodEnum::SynIngested)
    }
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeBodyMatInteractionTokenArg);

impl TryFromArgumentGroup for CeBodyMatInteractionTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 = Reference::try_from_argument_group(
            &mut token,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )?;
        let ce_token_arg = match reference_arg0.0.as_ref() {
            "SYNDROME_TAG" => {
                let syndrome_tag = SynTransmittionMethodEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyMatInteractionTokenArg::SyndromeTag(syndrome_tag)
            }
            "INTERACTION" => {
                let interaction = ReferenceTo::<InteractionToken>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyMatInteractionTokenArg::Interaction(interaction)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["SYNDROME_TAG", "INTERACTION"],
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(ce_token_arg)
    }
}
// endregion ======================================================================================

// region: CE args for CE_BODY_TRANSFORMATION =====================================================
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
pub enum CeBodyTransformationTokenArg {
    /// This can be used to specify a specific target creature of a specific caste, to transform
    /// into (for example, `[CE:CREATURE:DWARF:FEMALE]`). `ALL` or `ANY` can be used in place of
    /// a specific caste to randomise the caste for every transformation.
    ///
    /// Do note that using `ALL` or `ANY` for transformation castes will make the creature transform
    /// over and over again with the interval depending on the `START` token. This can lead to an
    /// unending transformation loop.
    /// [However, there is a way to get around this](https://dwarffortresswiki.org/index.php/Syndrome#looping_problem).
    Creature((ReferenceTo<CreatureToken>, Reference)), // TODO: this ref is `ANY`, `ALL`, or a caste for the creature
    /// Narrows down the selection to creatures which have the specified creature flag. May be used
    /// multiple times per transformation effect; creatures which lack any of the indicated flags
    /// will never be transformed into.
    CreatureFlag(CreatureFlagEnum),
    /// Excludes creatures with the specified creature flag from the random selection pool. May be
    /// used multiple times per transformation effect; creatures which possess any of the indicated
    /// flags will never be transformed into.
    ForbiddenCreatureFlag(CreatureFlagEnum),
    /// Narrows down the selection to creatures which have the specified caste flag. May be used
    /// multiple times per transformation effect; creatures which lack any of the indicated flags
    /// will never be transformed into.
    CreatureCasteFlag(CasteFlagEnum),
    /// Excludes any creature with the specified caste flag from the random selection pool. May be
    /// used multiple times per transformation effect; creatures which possess any of the indicated
    /// flags will never be transformed into.
    ForbiddenCreatureCasteFlag(CasteFlagEnum),
    /// Narrows down the selection to creatures which have at least one gait with an `<energy expenditure>` of
    /// 0 and a `<max speed>` less than or equal to the specified `<minimum gait speed>` ("less than"
    /// because lower is faster in the scale used for gait speed). This is used in generated
    /// divination curses to prevent the player from being transformed into a creature that is
    /// frustratingly slow to play as.
    HaveFastEffortlessGaitSpeed(u32),
    /// Excludes any creatures which have at least one gait with an `<energy expenditure>` of 0 and
    /// a `<max speed>` value less than or equal to the specified `<maximum gait speed>` (note that
    /// larger values are slower in the scale used for gait speed).
    AllSlowEffortlessGaitSpeed(u32),
}
impl Default for CeBodyTransformationTokenArg {
    fn default() -> Self {
        Self::Creature((
            ReferenceTo::<CreatureToken>::default(),
            Reference::default(),
        ))
    }
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeBodyTransformationTokenArg);

impl TryFromArgumentGroup for CeBodyTransformationTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 = Reference::try_from_argument_group(
            &mut token,
            source,
            &mut diagnostics,
            add_diagnostics_on_err,
        )?;
        let ce_token_arg = match reference_arg0.0.as_ref() {
            "CREATURE" => {
                let creature = <(ReferenceTo<CreatureToken>, Reference)>::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::Creature(creature)
            }
            "CREATURE_FLAG" => {
                let creature_flag = CreatureFlagEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::CreatureFlag(creature_flag)
            }
            "FORBIDDEN_CREATURE_FLAG" => {
                let forbidden_creature_flag = CreatureFlagEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::ForbiddenCreatureFlag(forbidden_creature_flag)
            }
            "CREATURE_CASTE_FLAG" => {
                let creature_caste_flag = CasteFlagEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::CreatureCasteFlag(creature_caste_flag)
            }
            "FORBIDDEN_CREATURE_CASTE_FLAG" => {
                let forbidden_creature_caste_flag = CasteFlagEnum::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::ForbiddenCreatureCasteFlag(
                    forbidden_creature_caste_flag,
                )
            }
            "HAVE_FAST_EFFORTLESS_GAIT_SPEED" => {
                let have_fast_effortless_gait_speed = u32::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::HaveFastEffortlessGaitSpeed(
                    have_fast_effortless_gait_speed,
                )
            }
            "ALL_SLOW_EFFORTLESS_GAIT_SPEED" => {
                let all_slow_effortless_gait_speed = u32::try_from_argument_group(
                    &mut token,
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                CeBodyTransformationTokenArg::HaveFastEffortlessGaitSpeed(
                    all_slow_effortless_gait_speed,
                )
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec![
                        "CREATURE",
                        "CREATURE_FLAG",
                        "FORBIDDEN_CREATURE_FLAG",
                        "CREATURE_CASTE_FLAG",
                        "FORBIDDEN_CREATURE_CASTE_FLAG",
                        "HAVE_FAST_EFFORTLESS_GAIT_SPEED",
                        "ALL_SLOW_EFFORTLESS_GAIT_SPEED",
                    ],
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(ce_token_arg)
    }
}
// endregion ======================================================================================

// region: Full CE_X args =========================================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeXTokenArg {
    /// The severity of the effect. Higher values appear to be worse, with `SEV:1000` `CE_NECROSIS`
    /// causing a part to near-instantly become rotten.
    pub sev: Option<u32>,
    /// The probability of the effect actually manifesting in the victim, as a percentage. 100 means
    /// always, 1 means a 1 in 100 chance.
    pub prob: Option<u8>,
    /// This tag overrides any `BP` tokens (so it is incompatible with them) and then forces the game to attempt to
    /// apply the effect to the limb that came into contact with the contagion - i.e. the part that
    /// was bitten by the creature injecting the syndrome material, or the one that was splattered
    /// by a contact contagion.
    ///
    /// If an effect can not be applied to the contacted limb (such as `IMPAIR_FUNCTION` on a
    /// non-organ) then this token makes the syndrome have no effect. This token also makes
    /// inhaled syndromes have no effect.
    pub localized: Option<()>,
    /// Specifies which body parts and tissues are to be affected by the syndrome. You can specify
    /// by category, by type, or by token, and then specify a specific tissue within that category,
    /// type or token.
    ///
    /// For example, if you wanted to target the lungs of a creature, you would use
    /// `BP:BY_CATEGORY:LUNG:ALL`. The syndrome would act on all bodyparts within the creature with
    /// the `CATEGORY` tag `LUNG`, and the `ALL` means it would affect all tissue layers. For another
    /// example, say you wanted to cause the skin to rot off a creature - you could use
    /// `BP:BY_CATEGORY:ALL:SKIN`, targeting the `SKIN` tissue on all bodyparts.
    ///
    /// This is one of the most powerful and useful aspects of the syndrome system, as it allows you
    /// to selectively target bodyparts relevant to the contagion, like lungs for coal dust inhalation,
    /// or the eyes for exposure to an acid gas. Multiple targets can be given in one syndrome by
    /// placing the `BP` tokens end to end.
    ///
    /// This tag is overidden by (and therefore incompatible with) `LOCALIZED`.
    pub bp: Vec<(BpCriteriaTokenArg, Reference)>,
    /// This tag makes the syndrome only affect tissue layers with the `VASCULAR` token.
    pub vascular_only: Option<()>,
    /// This tag makes the syndrome only affects tissue layers with the `MUSCULAR` token.
    pub muscular_only: Option<()>,
    /// This argument presumably causes the effects of the syndrome to scale with the size of the
    /// creature compared to the size of the dose of contagion they received, but has yet to be
    /// extensively tested.
    pub size_dilutes: Option<()>,
    /// This argument has yet to be tested fully, but presumably delays the onset of a syndrome
    /// according to the size of the victim.
    pub size_delays: Option<()>,
    /// Multiplies the duration of the syndrome by X in Fortress mode. If X is 144, syndrome
    /// will last the same amount of ticks in fortress and adventure mode.
    pub dwf_stretch: Option<u32>,
    /// Makes the symptom begin immediately rather than ramping up.
    pub abrupt: Option<()>,
    /// Can be hidden by a unit assuming a secret identity, such as a
    /// [vampire](https://dwarffortresswiki.org/index.php/Vampire).
    pub can_be_hidden: Option<()>,
    /// Determines if the effect can be hindered by the target creature's [disease resistance
    /// attribute](https://dwarffortresswiki.org/index.php/Attribute#Disease_Resistance). Without
    /// this argument, disease resistance is ignored.
    pub resistable: Option<()>,
    /// Determines the number of ticks after which this syndrome effect begins to affect a creature.
    pub start: Option<u32>,
    /// Determines the number of ticks this syndrome lasts.
    pub end: Option<u32>,
    /// Determines when this syndrome effect "peaks", reaching its highest severity.
    pub peak: Option<u32>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeXTokenArg);

impl TryFromArgumentGroup for CeXTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();
        while let Some(arg) = token.clone().get_current_arg_opt() {
            // Safe first argument for error case
            let arg0 = match token.get_current_arg() {
                Ok(arg) => Ok(arg.clone()),
                Err(err) => Err(err),
            };
            let next = Reference::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;
            let mut arg_is_duplicate: bool = false;

            match next.0.as_ref() {
                "SEV" => {
                    if result.sev.is_none() {
                        let sev = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.sev = Some(sev);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "PROB" => {
                    if result.prob.is_none() {
                        let prob = u8::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.prob = Some(prob);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "LOCALIZED" => {
                    // TODO: semantic, warn that this overwrites BP
                    if result.localized.is_none() {
                        result.localized = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "BP" => {
                    // TODO: semantic, warn that this is overwritten by LOCALIZED
                    let body_part_target =
                        <(BpCriteriaTokenArg, Reference)>::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                    result.bp.push(body_part_target);
                }
                "VASCULAR_ONLY" => {
                    if result.vascular_only.is_none() {
                        result.vascular_only = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "MUSCULAR_ONLY" => {
                    if result.muscular_only.is_none() {
                        result.muscular_only = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DILUTES" => {
                    if result.size_dilutes.is_none() {
                        result.size_dilutes = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DELAYS" => {
                    if result.size_delays.is_none() {
                        result.size_delays = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "DWF_STRETCH" => {
                    if result.dwf_stretch.is_none() {
                        let dwf_stretch = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.dwf_stretch = Some(dwf_stretch);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "ABRUPT" => {
                    if result.abrupt.is_none() {
                        result.abrupt = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "CAN_BE_HIDDEN" => {
                    if result.can_be_hidden.is_none() {
                        result.can_be_hidden = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "RESISTABLE" => {
                    if result.resistable.is_none() {
                        result.resistable = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "START" => {
                    if result.start.is_none() {
                        let start = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.start = Some(start);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "END" => {
                    if result.end.is_none() {
                        let end = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.end = Some(end);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "PEAK" => {
                    if result.peak.is_none() {
                        let peak = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.peak = Some(peak);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                _ => {
                    Self::diagnostics_wrong_enum_type(
                        &arg0?,
                        vec![
                            "SEV",
                            "PROB",
                            "LOCALIZED",
                            "BP",
                            "VASCULAR_ONLY",
                            "MUSCULAR_ONLY",
                            "SIZE_DILUTES",
                            "SIZE_DELAYS",
                            "DFW_STRETCH",
                            "ABRUPT",
                            "CAN_BE_HIDDEN",
                            "RESISTABLE",
                            "START",
                            "END",
                            "PEAK",
                        ],
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    );
                    return Err(());
                }
            };
            if arg_is_duplicate {
                diagnostics.add_message(
                    DMExtraInfo {
                        range: arg.node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => arg.value.to_string(),
                            "parent_token" => token.get_argument(0).unwrap().value.to_string(),
                        },
                    },
                    "duplicate_token_warn",
                );
            }
        }
        // TODO: semantic, give an error if the `START` tag is missing; it's required.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_X args with no target params ========================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeXNoTargetTokenArg {
    /// The severity of the effect. Higher values appear to be worse, with `SEV:1000` `CE_NECROSIS`
    /// causing a part to near-instantly become rotten.
    pub sev: Option<u32>,
    /// The probability of the effect actually manifesting in the victim, as a percentage. 100 means
    /// always, 1 means a 1 in 100 chance.
    pub prob: Option<u8>,
    /// This argument presumably causes the effects of the syndrome to scale with the size of the
    /// creature compared to the size of the dose of contagion they received, but has yet to be
    /// extensively tested.
    pub size_dilutes: Option<()>,
    /// This argument has yet to be tested fully, but presumably delays the onset of a syndrome
    /// according to the size of the victim.
    pub size_delays: Option<()>,
    /// Multiplies the duration of the syndrome by X in Fortress mode. If X is 144, syndrome
    /// will last the same amount of ticks in fortress and adventure mode.
    pub dwf_stretch: Option<u32>,
    /// Makes the symptom begin immediately rather than ramping up.
    pub abrupt: Option<()>,
    /// Can be hidden by a unit assuming a secret identity, such as a
    /// [vampire](https://dwarffortresswiki.org/index.php/Vampire).
    pub can_be_hidden: Option<()>,
    /// Determines if the effect can be hindered by the target creature's [disease resistance
    /// attribute](https://dwarffortresswiki.org/index.php/Attribute#Disease_Resistance). Without
    /// this argument, disease resistance is ignored.
    pub resistable: Option<()>,
    /// Determines the number of ticks after which this syndrome effect begins to affect a creature.
    pub start: Option<u32>,
    /// Determines the number of ticks this syndrome lasts.
    pub end: Option<u32>,
    /// Determines when this syndrome effect "peaks", reaching its highest severity.
    pub peak: Option<u32>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeXNoTargetTokenArg);

impl TryFromArgumentGroup for CeXNoTargetTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();
        while let Some(arg) = token.clone().get_current_arg_opt() {
            // Safe first argument for error case
            let arg0 = match token.get_current_arg() {
                Ok(arg) => Ok(arg.clone()),
                Err(err) => Err(err),
            };
            let next = Reference::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;
            let mut arg_is_duplicate: bool = false;

            match next.0.as_ref() {
                "SEV" => {
                    if result.sev.is_none() {
                        let sev = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.sev = Some(sev);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "PROB" => {
                    if result.prob.is_none() {
                        let prob = u8::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.prob = Some(prob);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DILUTES" => {
                    if result.size_dilutes.is_none() {
                        result.size_dilutes = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DELAYS" => {
                    if result.size_delays.is_none() {
                        result.size_delays = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "DWF_STRETCH" => {
                    if result.dwf_stretch.is_none() {
                        let dwf_stretch = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.dwf_stretch = Some(dwf_stretch);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "ABRUPT" => {
                    if result.abrupt.is_none() {
                        result.abrupt = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "CAN_BE_HIDDEN" => {
                    if result.can_be_hidden.is_none() {
                        result.can_be_hidden = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "RESISTABLE" => {
                    if result.resistable.is_none() {
                        result.resistable = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "START" => {
                    if result.start.is_none() {
                        let start = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.start = Some(start);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "END" => {
                    if result.end.is_none() {
                        let end = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.end = Some(end);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "PEAK" => {
                    if result.peak.is_none() {
                        let peak = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.peak = Some(peak);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                _ => {
                    Self::diagnostics_wrong_enum_type(
                        &arg0?,
                        vec![
                            "SEV",
                            "PROB",
                            "SIZE_DILUTES",
                            "SIZE_DELAYS",
                            "DFW_STRETCH",
                            "ABRUPT",
                            "CAN_BE_HIDDEN",
                            "RESISTABLE",
                            "START",
                            "END",
                            "PEAK",
                        ],
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    );
                    return Err(());
                }
            };
            if arg_is_duplicate {
                diagnostics.add_message(
                    DMExtraInfo {
                        range: arg.node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => arg.value.to_string(),
                            "parent_token" => token.get_argument(0).unwrap().value.to_string(),
                        },
                    },
                    "duplicate_token_warn",
                );
            }
        }
        // TODO: semantic, give an error if the `START` tag is missing; it's required.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_X args with no SEV or target parameters =============================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeXNoSevTokenArg {
    /// The probability of the effect actually manifesting in the victim, as a percentage. 100 means
    /// always, 1 means a 1 in 100 chance.
    pub prob: Option<u8>,
    /// This argument presumably causes the effects of the syndrome to scale with the size of the
    /// creature compared to the size of the dose of contagion they received, but has yet to be
    /// extensively tested.
    pub size_dilutes: Option<()>,
    /// This argument has yet to be tested fully, but presumably delays the onset of a syndrome
    /// according to the size of the victim.
    pub size_delays: Option<()>,
    /// Multiplies the duration of the syndrome by X in Fortress mode. If X is 144, syndrome
    /// will last the same amount of ticks in fortress and adventure mode.
    pub dwf_stretch: Option<u32>,
    /// Makes the symptom begin immediately rather than ramping up.
    pub abrupt: Option<()>,
    /// Can be hidden by a unit assuming a secret identity, such as a
    /// [vampire](https://dwarffortresswiki.org/index.php/Vampire).
    pub can_be_hidden: Option<()>,
    /// Determines if the effect can be hindered by the target creature's [disease resistance
    /// attribute](https://dwarffortresswiki.org/index.php/Attribute#Disease_Resistance). Without
    /// this argument, disease resistance is ignored.
    pub resistable: Option<()>,
    /// Determines the number of ticks after which this syndrome effect begins to affect a creature.
    pub start: Option<u32>,
    /// Determines the number of ticks this syndrome lasts.
    pub end: Option<u32>,
    /// Determines when this syndrome effect "peaks", reaching its highest severity.
    pub peak: Option<u32>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeXNoSevTokenArg);

impl TryFromArgumentGroup for CeXNoSevTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();
        while let Some(arg) = token.clone().get_current_arg_opt() {
            // Safe first argument for error case
            let arg0 = match token.get_current_arg() {
                Ok(arg) => Ok(arg.clone()),
                Err(err) => Err(err),
            };
            let next = Reference::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;
            let mut arg_is_duplicate: bool = false;

            match next.0.as_ref() {
                "PROB" => {
                    if result.prob.is_none() {
                        let prob = u8::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.prob = Some(prob);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DILUTES" => {
                    if result.size_dilutes.is_none() {
                        result.size_dilutes = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "SIZE_DELAYS" => {
                    if result.size_delays.is_none() {
                        result.size_delays = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "DWF_STRETCH" => {
                    if result.dwf_stretch.is_none() {
                        let dwf_stretch = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.dwf_stretch = Some(dwf_stretch);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "ABRUPT" => {
                    if result.abrupt.is_none() {
                        result.abrupt = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "CAN_BE_HIDDEN" => {
                    if result.can_be_hidden.is_none() {
                        result.can_be_hidden = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "RESISTABLE" => {
                    if result.resistable.is_none() {
                        result.resistable = Some(());
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "START" => {
                    if result.start.is_none() {
                        let start = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.start = Some(start);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "END" => {
                    if result.end.is_none() {
                        let end = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.end = Some(end);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                "PEAK" => {
                    if result.peak.is_none() {
                        let peak = u32::try_from_argument_group(
                            &mut token,
                            source,
                            &mut diagnostics,
                            add_diagnostics_on_err,
                        )?;
                        result.peak = Some(peak);
                    } else {
                        arg_is_duplicate = true;
                    }
                }
                _ => {
                    Self::diagnostics_wrong_enum_type(
                        &arg0?,
                        vec![
                            "PROB",
                            "SIZE_DILUTES",
                            "SIZE_DELAYS",
                            "DFW_STRETCH",
                            "ABRUPT",
                            "CAN_BE_HIDDEN",
                            "RESISTABLE",
                            "START",
                            "END",
                            "PEAK",
                        ],
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    );
                    return Err(());
                }
            };
            if arg_is_duplicate {
                diagnostics.add_message(
                    DMExtraInfo {
                        range: arg.node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => arg.value.to_string(),
                            "parent_token" => token.get_argument(0).unwrap().value.to_string(),
                        },
                    },
                    "duplicate_token_warn",
                );
            }
        }
        // TODO: semantic, give an error if the `START` tag is missing; it's required.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_CHANGE_PERSONALITY args =============================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeChangePersonalityTokenArg {
    pub facets: Vec<(FacetEnum, PersonalityTraitEnum, i8)>,
    pub general_cex: Option<CeXNoSevTokenArg>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeChangePersonalityTokenArg);

impl TryFromArgumentGroup for CeChangePersonalityTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();

        loop {
            if token.get_current_arg_opt().is_none() {
                // No more args
                break;
            }

            let mut token_clone = token.clone();
            let next = Reference::try_from_argument_group(
                &mut token_clone,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;

            match next.0.as_ref() {
                "FACET" => {
                    let facet = <(FacetEnum, PersonalityTraitEnum, i8)>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.facets.push(facet);
                }
                _ => {
                    let general_cex = <CeXNoSevTokenArg>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.general_cex = Some(general_cex);
                    // The general_cex tokens should all be in one go
                    break;
                }
            };
        }
        // TODO: give an error if `general_cex` is not used; it's required.
        // Maybe that's something for semantic to deal with, maybe not.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_(ADD/REMOVE)_TAG args ===============================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeTagsTokenArg {
    pub tags: Vec<TargetPropertyEnum>,
    pub general_cex: Option<CeXNoSevTokenArg>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeTagsTokenArg);

impl TryFromArgumentGroup for CeTagsTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();

        loop {
            if token.get_current_arg_opt().is_none() {
                // No more args
                break;
            }

            let mut token_clone = token.clone();
            let next = Reference::try_from_argument_group(
                &mut token_clone,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;

            match next.0.as_ref() {
                "PROB" | "SIZE_DILUTES" | "SIZE_DELAYS" | "DFW_STRETCH" | "ABRUPT"
                | "CAN_BE_HIDDEN" | "RESISTABLE" | "START" | "END" | "PEAK" => {
                    let general_cex = <CeXNoSevTokenArg>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.general_cex = Some(general_cex);
                    // The general_cex tokens should all be in one go
                    break;
                }
                _ => {
                    let tag = TargetPropertyEnum::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.tags.push(tag);
                }
            };
        }
        // TODO: give an error if `general_cex` is not used; it's required.
        // Maybe that's something for semantic to deal with, maybe not.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_PHYS_ATT_CHANGE args ================================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CePhysAttChangeTokenArg {
    pub body_attributes: Vec<(BodyAttributeEnum, u32, u32)>,
    pub general_cex: Option<CeXNoSevTokenArg>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CePhysAttChangeTokenArg);

impl TryFromArgumentGroup for CePhysAttChangeTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();

        loop {
            if token.get_current_arg_opt().is_none() {
                // No more args
                break;
            }

            let mut token_clone = token.clone();
            let next = Reference::try_from_argument_group(
                &mut token_clone,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;

            match next.0.as_ref() {
                "PROB" | "SIZE_DILUTES" | "SIZE_DELAYS" | "DFW_STRETCH" | "ABRUPT"
                | "CAN_BE_HIDDEN" | "RESISTABLE" | "START" | "END" | "PEAK" => {
                    let general_cex = <CeXNoSevTokenArg>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.general_cex = Some(general_cex);
                    // The general_cex tokens should all be in one go
                    break;
                }
                _ => {
                    let body_attribute = <(BodyAttributeEnum, u32, u32)>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.body_attributes.push(body_attribute);
                }
            };
        }
        // TODO: give an error if `general_cex` is not used; it's required.
        // Maybe that's something for semantic to deal with, maybe not.
        Ok(result)
    }
}
// endregion ======================================================================================

// region: CE_MENT_ATT_CHANGE args ================================================================
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq)]
pub struct CeMentAttChangeTokenArg {
    pub soul_attributes: Vec<(SoulAttributeEnum, u32, u32)>,
    pub general_cex: Option<CeXNoSevTokenArg>,
}

// Deserialize a token with following pattern: `[REF:creature_effect_token_args:...]`
df_ls_syntax_analysis::token_deserialize_unary_token!(CeMentAttChangeTokenArg);

impl TryFromArgumentGroup for CeMentAttChangeTokenArg {
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = Self::default();

        loop {
            if token.get_current_arg_opt().is_none() {
                // No more args
                break;
            }

            let mut token_clone = token.clone();
            let next = Reference::try_from_argument_group(
                &mut token_clone,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;

            match next.0.as_ref() {
                "PROB" | "SIZE_DILUTES" | "SIZE_DELAYS" | "DFW_STRETCH" | "ABRUPT"
                | "CAN_BE_HIDDEN" | "RESISTABLE" | "START" | "END" | "PEAK" => {
                    let general_cex = <CeXNoSevTokenArg>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.general_cex = Some(general_cex);
                    // The general_cex tokens should all be in one go
                    break;
                }
                _ => {
                    let soul_attribute = <(SoulAttributeEnum, u32, u32)>::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    )?;
                    result.soul_attributes.push(soul_attribute);
                }
            };
        }
        // TODO: give an error if `general_cex` is not used; it's required.
        // Maybe that's something for semantic to deal with, maybe not.
        Ok(result)
    }
}
// endregion ======================================================================================
