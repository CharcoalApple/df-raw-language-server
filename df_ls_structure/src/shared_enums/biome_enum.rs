use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum BiomeEnum {
    /// Mountain (ID: 0)
    #[token_de(token = "MOUNTAIN", alias = "MOUNTAINS")]
    Mountain,
    /// Glacier (ID: 1)
    #[token_de(token = "GLACIER")]
    Glacier,
    /// Tundra (ID: 2)
    #[token_de(token = "TUNDRA")]
    Tundra,
    /// Temperate Freshwater Swamp (ID: 3)
    #[token_de(token = "SWAMP_TEMPERATE_FRESHWATER")]
    SwampTemperateFreshwater,
    /// Temperate Saltwater Swamp (ID: 4)
    #[token_de(token = "SWAMP_TEMPERATE_SALTWATER")]
    SwampTemperateSaltwater,
    /// Temperate Freshwater Marsh (ID: 5)
    #[token_de(token = "MARSH_TEMPERATE_FRESHWATER")]
    MarshTemperateFreshwater,
    /// Temperate Saltwater Marsh (ID: 6)
    #[token_de(token = "MARSH_TEMPERATE_SALTWATER")]
    MarshTemperateSaltwater,
    /// Tropical Freshwater Swamp (ID: 7)
    #[token_de(token = "SWAMP_TROPICAL_FRESHWATER")]
    SwampTropicalFreshwater,
    /// Tropical Saltwater Swamp (ID: 8)
    #[token_de(token = "SWAMP_TROPICAL_SALTWATER")]
    SwampTropicalSaltwater,
    /// Mangrove Swamp (ID: 9)
    #[token_de(token = "SWAMP_MANGROVE")]
    SwampMangrove,
    /// Tropical Freshwater Marsh (ID: 10)
    #[token_de(token = "MARSH_TROPICAL_FRESHWATER")]
    MarshTropicalFreshwater,
    /// Tropical Saltwater Marsh (ID: 11)
    #[token_de(token = "MARSH_TROPICAL_SALTWATER")]
    MarshTropicalSaltwater,
    /// Taiga (ID: 12)
    #[token_de(token = "FOREST_TAIGA", alias = "TAIGA")]
    ForestTaiga,
    /// Temperate Coniferous Forest (ID: 13)
    #[token_de(token = "FOREST_TEMPERATE_CONIFER")]
    ForestTemperateConifer,
    /// Temperate Broadleaf Forest (ID: 14)
    #[token_de(token = "FOREST_TEMPERATE_BROADLEAF")]
    ForestTemperateBroadleaf,
    /// Tropical Coniferous Forest (ID: 15)
    #[token_de(token = "FOREST_TROPICAL_CONIFER")]
    ForestTropicalConifer,
    /// Tropical Dry Broadleaf Forest (ID: 16)
    #[token_de(token = "FOREST_TROPICAL_DRY_BROADLEAF")]
    ForestTropicalDryBroadleaf,
    /// Tropical Moist Broadleaf Forest (ID: 17)
    #[token_de(token = "FOREST_TROPICAL_MOIST_BROADLEAF")]
    ForestTropicalMoistBroadleaf,
    /// Temperate Grassland (ID: 18)
    #[token_de(token = "GRASSLAND_TEMPERATE")]
    GrasslandTemperate,
    /// Temperate Savanna (ID: 19)
    #[token_de(token = "SAVANNA_TEMPERATE")]
    SavannaTemperate,
    /// Temperate Shrubland (ID: 20)
    #[token_de(token = "SHRUBLAND_TEMPERATE")]
    ShrublandTemperate,
    /// Tropical Grassland (ID: 21)
    #[token_de(token = "GRASSLAND_TROPICAL")]
    GrasslandTropical,
    /// Tropical Savanna (ID: 22)
    #[token_de(token = "SAVANNA_TROPICAL")]
    SavannaTropical,
    /// Tropical Shrubland (ID: 23)
    #[token_de(token = "SHRUBLAND_TROPICAL")]
    ShrublandTropical,
    /// Badlands (ID: 24)
    #[token_de(token = "DESERT_BADLAND")]
    DesertBadland,
    /// Rocky Wasteland (ID: 25)
    #[token_de(token = "DESERT_ROCK")]
    DesertRock,
    /// Sand Desert (ID: 26)
    #[token_de(token = "DESERT_SAND")]
    DesertSand,
    /// Tropical Ocean (ID: 27)
    #[token_de(token = "OCEAN_TROPICAL")]
    OceanTropical,
    /// Temperate Ocean (ID: 28)
    #[token_de(token = "OCEAN_TEMPERATE")]
    OceanTemperate,
    /// Arctic Ocean (ID: 29)
    #[token_de(token = "OCEAN_ARCTIC")]
    OceanArctic,
    /// Temperate Freshwater Pool (ID: 30)
    #[token_de(token = "POOL_TEMPERATE_FRESHWATER")]
    PoolTemperateFreshwater,
    /// Temperate Brackish Pool (ID: 31)
    #[token_de(token = "POOL_TEMPERATE_BRACKISHWATER")]
    PoolTemperateBrackishwater,
    /// Temperate Saltwater Pool (ID: 32)
    #[token_de(token = "POOL_TEMPERATE_SALTWATER")]
    PoolTemperateSaltwater,
    /// Tropical Freshwater Pool (ID: 33)
    #[token_de(token = "POOL_TROPICAL_FRESHWATER")]
    PoolTropicalFreshwater,
    /// Tropical Brackish Pool (ID: 34)
    #[token_de(token = "POOL_TROPICAL_BRACKISHWATER")]
    PoolTropicalBrackishwater,
    /// Tropical Saltwater Pool (ID: 35)
    #[token_de(token = "POOL_TROPICAL_SALTWATER")]
    PoolTropicalSaltwater,
    /// Temperate Freshwater Lake (ID: 36)
    #[token_de(token = "LAKE_TEMPERATE_FRESHWATER")]
    LakeTemperateFreshwater,
    /// Temperate Brackish Lake (ID: 37)
    #[token_de(token = "LAKE_TEMPERATE_BRACKISHWATER")]
    LakeTemperateBrackishwater,
    /// Temperate Saltwater Lake (ID: 38)
    #[token_de(token = "LAKE_TEMPERATE_SALTWATER")]
    LakeTemperateSaltwater,
    /// Tropical Freshwater Lake (ID: 39)
    #[token_de(token = "LAKE_TROPICAL_FRESHWATER")]
    LakeTropicalFreshwater,
    /// Tropical Brackish Lake (ID: 40)
    #[token_de(token = "LAKE_TROPICAL_BRACKISHWATER")]
    LakeTropicalBrackishwater,
    /// Tropical Saltwater Lake (ID: 41)
    #[token_de(token = "LAKE_TROPICAL_SALTWATER")]
    LakeTropicalSaltwater,
    /// Temperate Freshwater River (ID: 42)
    #[token_de(token = "RIVER_TEMPERATE_FRESHWATER")]
    RiverTemperateFreshwater,
    /// Temperate Brackish River (ID: 43)
    #[token_de(token = "RIVER_TEMPERATE_BRACKISHWATER")]
    RiverTemperateBrackishwater,
    /// Temperate Saltwater River (ID: 44)
    #[token_de(token = "RIVER_TEMPERATE_SALTWATER")]
    RiverTemperateSaltwater,
    /// Tropical Freshwater River (ID: 45)
    #[token_de(token = "RIVER_TROPICAL_FRESHWATER")]
    RiverTropicalFreshwater,
    /// Tropical Brackish River (ID: 46)
    #[token_de(token = "RIVER_TROPICAL_BRACKISHWATER")]
    RiverTropicalBrackishwater,
    /// Tropical Saltwater River (ID: 47)
    #[token_de(token = "RIVER_TROPICAL_SALTWATER")]
    RiverTropicalSaltwater,
    /// Underground caverns (in water) (ID: 48)
    #[token_de(token = "SUBTERRANEAN_WATER")]
    SubterraneanWater,
    /// Underground caverns (out of water) (ID: 49)
    #[token_de(token = "SUBTERRANEAN_CHASM")]
    SubterraneanChasm,
    /// Magma sea (ID: 50)
    #[token_de(token = "SUBTERRANEAN_LAVA")]
    SubterraneanLava,
    /// All biomes excluding pools, rivers, and underground features (ID: 0-29, 36-41)
    #[token_de(token = "ALL_MAIN")]
    AllMain,
    /// All main biomes excluding oceans and lakes (ID: 0-26)
    #[token_de(token = "ANY_LAND")]
    AnyLand,
    /// All ocean biomes (ID: 27-29)
    #[token_de(token = "ANY_OCEAN")]
    AnyOcean,
    /// All lake biomes (ID: 36-41)
    #[token_de(token = "ANY_LAKE")]
    AnyLake,
    /// All temperate lake biomes (ID: 36-38)
    #[token_de(token = "ANY_TEMPERATE_LAKE")]
    AnyTemperateLake,
    /// All tropical lake biomes (ID: 39-41)
    #[token_de(token = "ANY_TROPICAL_LAKE")]
    AnyTropicalLake,
    /// All river biomes (ID: 42-47)
    #[token_de(token = "ANY_RIVER")]
    AnyRiver,
    /// All temperate river biomes (ID: 42-44)
    #[token_de(token = "ANY_TEMPERATE_RIVER")]
    AnyTemperateRiver,
    /// All tropical river biomes (ID: 45-47)
    #[token_de(token = "ANY_TROPICAL_RIVER")]
    AnyTropicalRiver,
    /// All pool biomes (ID: 30-35)
    #[token_de(token = "ANY_POOL")]
    AnyPool,
    /// All land biomes excluding Mountain, Glacier, and Tundra (ID: 3-26)
    #[token_de(token = "NOT_FREEZING")]
    NotFreezing,
    /// All Temperate land biomes - marshes, swamps, forests, grassland, savanna, and shrubland
    /// (ID: 3-6, 13-14, 18-20)
    #[token_de(token = "ANY_TEMPERATE")]
    AnyTemperate,
    /// All Tropical land biomes - marshes, swamps (including Mangrove), forests, grassland,
    /// savanna, and shrubland (ID: 7-11, 15-17, 21-23)
    #[token_de(token = "ANY_TROPICAL")]
    AnyTropical,
    /// All Forest biomes (excluding Taiga) (ID: 13-17)
    #[token_de(token = "ANY_FOREST")]
    AnyForest,
    /// Temperate and Tropical Shrubland (ID: 20, 23)
    #[token_de(token = "ANY_SHRUBLAND")]
    AnyShrubland,
    /// Temperate and Tropical Grassland (ID: 18, 21)
    #[token_de(token = "ANY_GRASSLAND")]
    AnyGrassland,
    /// Temperate and Tropical Savanna (ID: 19, 22)
    #[token_de(token = "ANY_SAVANNA")]
    AnySavanna,
    /// Temperate Coniferous and Broadleaf Forests (ID: 13-14)
    #[token_de(token = "ANY_TEMPERATE_FOREST")]
    AnyTemperateForest,
    /// Tropical Coniferous and Dry/Moist Broadleaf Forests (ID: 15-17)
    #[token_de(token = "ANY_TROPICAL_FOREST")]
    AnyTropicalForest,
    /// Temperate Broadleaf Forest, Grassland/Savanna/Shrubland, Swamps, and Marshes (ID: 3-6, 14, 18-20)
    #[token_de(token = "ANY_TEMPERATE_BROADLEAF")]
    AnyTemperateBroadleaf,
    /// Tropical Dry/Moist Broadleaf Forest, Grassland/Savanna/Shrubland, Swamps (including Mangrove),
    /// and Marshes (ID: 7-11, 16-17, 21-23)
    #[token_de(token = "ANY_TROPICAL_BROADLEAF")]
    AnyTropicalBroadleaf,
    /// All swamps and marshes (ID: 3-11)
    #[token_de(token = "ANY_WETLAND")]
    AnyWetland,
    /// All temperate swamps and marshes (ID: 3-6)
    #[token_de(token = "ANY_TEMPERATE_WETLAND")]
    AnyTemperateWetland,
    /// All tropical swamps and marshes (ID: 7-11)
    #[token_de(token = "ANY_TROPICAL_WETLAND")]
    AnyTropicalWetland,
    /// All tropical marshes (ID: 10-11)
    #[token_de(token = "ANY_TROPICAL_MARSH")]
    AnyTropicalMarsh,
    /// All temperate marshes (ID: 5-6)
    #[token_de(token = "ANY_TEMPERATE_MARSH")]
    AnyTemperateMarsh,
    /// All tropical swamps (including Mangrove) (ID: 7-9)
    #[token_de(token = "ANY_TROPICAL_SWAMP")]
    AnyTropicalSwamp,
    /// All temperate swamps (ID: 3-4)
    #[token_de(token = "ANY_TEMPERATE_SWAMP")]
    AnyTemperateSwamp,
    /// Badlands, Rocky Wasteland, and Sand Desert (ID: 24-26)
    #[token_de(token = "ANY_DESERT")]
    AnyDesert,
}

impl Default for BiomeEnum {
    fn default() -> Self {
        Self::Mountain
    }
}
