use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

// TODO: investigate if these are hardcoded to only be applicable to cerain body part CATEGORY's;
// the wiki groups these according to body part category, but categories are arbitrary and user definable!
// TODO: Fill this out; this likely matches to specific description string ingame; maybe use that here.
#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum BpAppModifersEnum {
    /// The height of the body part.
    #[token_de(token = "HEIGHT")]
    Height,
    /// The length of the body part.
    #[token_de(token = "LENGTH")]
    Length,
    /// The broadness of the body part.
    #[token_de(token = "BROADNESS")]
    Broadness,
    #[token_de(token = "CLOSE_SET")]
    CloseSet,
    #[token_de(token = "DEEP_SET")]
    DeepSet,
    #[token_de(token = "ROUND_VS_NARROW")]
    RoundVsNarrow,
    #[token_de(token = "LARGE_IRIS")]
    LargeIris,
    #[token_de(token = "THICKNESS")]
    Thickness,
    #[token_de(token = "UPTURNED")]
    Upturned,
    #[token_de(token = "CONVEX")]
    Convex,
    #[token_de(token = "SPLAYED_OUT")]
    SplayedOut,
    #[token_de(token = "HANGING_LOBES")]
    HangingLobes,
    #[token_de(token = "GAPS")]
    Gaps,
    #[token_de(token = "HIGH_CHEEKBONES")]
    HighCheekbones,
    #[token_de(token = "BROAD_CHIN")]
    BroadChin,
    #[token_de(token = "JUTTING_CHIN")]
    JuttingChin,
    #[token_de(token = "SQUARE_CHIN")]
    SquareChin,
    #[token_de(token = "DEEP_VOICE")]
    DeepVoice,
    #[token_de(token = "RASPY_VOICE")]
    RaspyVoice,
}
impl Default for BpAppModifersEnum {
    fn default() -> Self {
        Self::Height
    }
}
