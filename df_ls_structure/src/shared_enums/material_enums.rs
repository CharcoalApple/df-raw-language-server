use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum AllOrAllSolidEnum {
    /// All material states.
    #[token_de(token = "ALL")]
    All,
    /// All solid material states.
    #[token_de(token = "ALL_SOLID")]
    AllSolid,
}
impl Default for AllOrAllSolidEnum {
    fn default() -> Self {
        Self::All
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum MaterialStateEnum {
    /// Material is in a solid state.
    /// Like:
    /// - Iron at room temperature
    /// - Water blow freezing (ice)
    #[token_de(token = "SOLID")]
    Solid,
    /// Material is in a liquid state.
    /// Like:
    /// - Iron at very high temperatures (molten iron)
    /// - Water at room temperature
    #[token_de(token = "LIQUID")]
    Liquid,
    /// Material is in a gas state.
    /// Like:
    /// - Oxygen at room temperature (air)
    /// - Water above its boiling point (steam)
    #[token_de(token = "GAS")]
    Gas,
    /// Material is ground to a powder.
    #[token_de(token = "POWDER")]
    Powder,
    /// Unknown
    #[token_de(token = "SOLID_POWDER")]
    SolidPowder,
    /// Unknown
    #[token_de(token = "PASTE")]
    Paste,
    /// Unknown
    #[token_de(token = "SOLID_PASTE")]
    SolidPaste,
    /// Unknown
    #[token_de(token = "PRESSED")]
    Pressed,
    /// Unknown
    #[token_de(token = "SOLID_PRESSED")]
    SolidPressed,
}
impl Default for MaterialStateEnum {
    fn default() -> Self {
        Self::Solid
    }
}
