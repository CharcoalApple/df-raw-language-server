use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum AllEnum {
    #[token_de(token = "ALL")]
    All,
}

impl Default for AllEnum {
    fn default() -> Self {
        Self::All
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NoneEnum {
    #[token_de(token = "NONE")]
    None,
}

impl Default for NoneEnum {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum StandardPluralEnum {
    #[token_de(token = "STP")]
    Stp,
}
impl Default for StandardPluralEnum {
    fn default() -> Self {
        Self::Stp
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NoMatGlossEnum {
    #[token_de(token = "NO_MATGLOSS")]
    NoMatgloss,
}

impl Default for NoMatGlossEnum {
    fn default() -> Self {
        Self::NoMatgloss
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum NotApplicableEnum {
    #[token_de(token = "NA")]
    NotApplicable,
}
impl Default for NotApplicableEnum {
    fn default() -> Self {
        Self::NotApplicable
    }
}
