use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

// TODO: maybe add descriptions for these where possible/practical;
// copy from applicable creature tokens.
#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum CreatureFlagEnum {
    #[token_de(token = "ALL_CASTES_ALIVE")]
    AllCastesAlive,
    #[token_de(token = "ARTIFICIAL_HIVEABLE")]
    ArtificialHiveable,
    #[token_de(token = "BIOME_DESERT_BADLAND")]
    BiomeDesertBadland,
    #[token_de(token = "BIOME_DESERT_ROCK")]
    BiomeDesertRock,
    #[token_de(token = "BIOME_DESERT_SAND")]
    BiomeDesertSand,
    #[token_de(token = "BIOME_FOREST_TAIGA")]
    BiomeForestTaiga,
    #[token_de(token = "BIOME_FOREST_TEMPERATE_BROADLEAF")]
    BiomeForestTemperateBroadleaf,
    #[token_de(token = "BIOME_FOREST_TEMPERATE_CONIFER")]
    BiomeForestTemperateConifer,
    #[token_de(token = "BIOME_FOREST_TROPICAL_CONIFER")]
    BiomeForestTropicalConifer,
    #[token_de(token = "BIOME_FOREST_TROPICAL_DRY_BROADLEAF")]
    BiomeForestTropicalDryBroadleaf,
    #[token_de(token = "BIOME_FOREST_TROPICAL_MOIST_BROADLEAF")]
    BiomeForestTropicalMoistBroadleaf,
    #[token_de(token = "BIOME_GLACIER")]
    BiomeGlacier,
    #[token_de(token = "BIOME_GRASSLAND_TEMPERATE")]
    BiomeGrasslandTemperate,
    #[token_de(token = "BIOME_GRASSLAND_TROPICAL")]
    BiomeGrasslandTropical,
    #[token_de(token = "BIOME_LAKE_TEMPERATE_BRACKISHWATER")]
    BiomeLakeTemperateBrackishwater,
    #[token_de(token = "BIOME_LAKE_TEMPERATE_FRESHWATER")]
    BiomeLakeTemperateFreshwater,
    #[token_de(token = "BIOME_LAKE_TEMPERATE_SALTWATER")]
    BiomeLakeTemperateSaltwater,
    #[token_de(token = "BIOME_LAKE_TROPICAL_BRACKISHWATER")]
    BiomeLakeTropicalBrackishwater,
    #[token_de(token = "BIOME_LAKE_TROPICAL_FRESHWATER")]
    BiomeLakeTropicalFreshwater,
    #[token_de(token = "BIOME_LAKE_TROPICAL_SALTWATER")]
    BiomeLakeTropicalSaltwater,
    #[token_de(token = "BIOME_MARSH_TEMPERATE_FRESHWATER")]
    BiomeMarshTemperateFreshwater,
    #[token_de(token = "BIOME_MARSH_TEMPERATE_SALTWATER")]
    BiomeMarshTemperateSaltwater,
    #[token_de(token = "BIOME_MARSH_TROPICAL_FRESHWATER")]
    BiomeMarshTropicalFreshwater,
    #[token_de(token = "BIOME_MARSH_TROPICAL_SALTWATER")]
    BiomeMarshTropicalSaltwater,
    #[token_de(token = "BIOME_MOUNTAIN")]
    BiomeMountain,
    #[token_de(token = "BIOME_OCEAN_ARCTIC")]
    BiomeOceanArctic,
    #[token_de(token = "BIOME_OCEAN_TEMPERATE")]
    BiomeOceanTemperate,
    #[token_de(token = "BIOME_OCEAN_TROPICAL")]
    BiomeOceanTropical,
    #[token_de(token = "BIOME_POOL_TEMPERATE_BRACKISHWATER")]
    BiomePoolTemperateBrackishwater,
    #[token_de(token = "BIOME_POOL_TEMPERATE_FRESHWATER")]
    BiomePoolTemperateFreshwater,
    #[token_de(token = "BIOME_POOL_TEMPERATE_SALTWATER")]
    BiomePoolTemperateSaltwater,
    #[token_de(token = "BIOME_POOL_TROPICAL_BRACKISHWATER")]
    BiomePoolTropicalBrackishwater,
    #[token_de(token = "BIOME_POOL_TROPICAL_FRESHWATER")]
    BiomePoolTropicalFreshwater,
    #[token_de(token = "BIOME_POOL_TROPICAL_SALTWATER")]
    BiomePoolTropicalSaltwater,
    #[token_de(token = "BIOME_RIVER_TEMPERATE_BRACKISHWATER")]
    BiomeRiverTemperateBrackishwater,
    #[token_de(token = "BIOME_RIVER_TEMPERATE_FRESHWATER")]
    BiomeRiverTemperateFreshwater,
    #[token_de(token = "BIOME_RIVER_TEMPERATE_SALTWATER")]
    BiomeRiverTemperateSaltwater,
    #[token_de(token = "BIOME_RIVER_TROPICAL_BRACKISHWATER")]
    BiomeRiverTropicalBrackishwater,
    #[token_de(token = "BIOME_RIVER_TROPICAL_FRESHWATER")]
    BiomeRiverTropicalFreshwater,
    #[token_de(token = "BIOME_RIVER_TROPICAL_SALTWATER")]
    BiomeRiverTropicalSaltwater,
    #[token_de(token = "BIOME_SAVANNA_TEMPERATE")]
    BiomeSavannaTemperate,
    #[token_de(token = "BIOME_SAVANNA_TROPICAL")]
    BiomeSavannaTropical,
    #[token_de(token = "BIOME_SHRUBLAND_TEMPERATE")]
    BiomeShrublandTemperate,
    #[token_de(token = "BIOME_SHRUBLAND_TROPICAL")]
    BiomeShrublandTropical,
    #[token_de(token = "BIOME_SUBTERRANEAN_CHASM")]
    BiomeSubterraneanChasm,
    #[token_de(token = "BIOME_SUBTERRANEAN_LAVA")]
    BiomeSubterraneanLava,
    #[token_de(token = "BIOME_SUBTERRANEAN_WATER")]
    BiomeSubterraneanWater,
    #[token_de(token = "BIOME_SWAMP_MANGROVE")]
    BiomeSwampMangrove,
    #[token_de(token = "BIOME_SWAMP_TEMPERATE_FRESHWATER")]
    BiomeSwampTemperateFreshwater,
    #[token_de(token = "BIOME_SWAMP_TEMPERATE_SALTWATER")]
    BiomeSwampTemperateSaltwater,
    #[token_de(token = "BIOME_SWAMP_TROPICAL_FRESHWATER")]
    BiomeSwampTropicalFreshwater,
    #[token_de(token = "BIOME_SWAMP_TROPICAL_SALTWATER")]
    BiomeSwampTropicalSaltwater,
    #[token_de(token = "BIOME_TUNDRA")]
    BiomeTundra,
    #[token_de(token = "DOES_NOT_EXIST")]
    DoesNotExist,
    #[token_de(token = "EQUIPMENT")]
    Equipment,
    #[token_de(token = "EQUIPMENT_WAGON")]
    EquipmentWagon,
    #[token_de(token = "EVIL")]
    Evil,
    #[token_de(token = "FANCIFUL")]
    Fanciful,
    #[token_de(token = "GENERATED")]
    Generated,
    #[token_de(token = "GOOD")]
    Good,
    #[token_de(token = "HAS_ANY_BENIGN")]
    HasAnyBenign,
    #[token_de(token = "HAS_ANY_CANNOT_BREATHE_AIR")]
    HasAnyCannotBreatheAir,
    #[token_de(token = "HAS_ANY_CANNOT_BREATHE_WATER")]
    HasAnyCannotBreatheWater,
    #[token_de(token = "HAS_ANY_CAN_SWIM")]
    HasAnyCanSwim,
    #[token_de(token = "HAS_ANY_CARNIVORE")]
    HasAnyCarnivore,
    #[token_de(token = "HAS_ANY_COMMON_DOMESTIC")]
    HasAnyCommonDomestic,
    #[token_de(token = "HAS_ANY_CURIOUS_BEAST")]
    HasAnyCuriousBeast,
    #[token_de(token = "HAS_ANY_DEMON")]
    HasAnyDemon,
    #[token_de(token = "HAS_ANY_FEATURE_BEAST")]
    HasAnyFeatureBeast,
    #[token_de(token = "HAS_ANY_FLIER")]
    HasAnyFlier,
    #[token_de(token = "HAS_ANY_FLY_RACE_GAIT")]
    HasAnyFlyRaceGait,
    #[token_de(token = "HAS_ANY_GRASP")]
    HasAnyGrasp,
    #[token_de(token = "HAS_ANY_GRAZER")]
    HasAnyGrazer,
    #[token_de(token = "HAS_ANY_HAS_BLOOD")]
    HasAnyHasBlood,
    #[token_de(token = "HAS_ANY_IMMOBILE")]
    HasAnyImmobile,
    #[token_de(token = "HAS_ANY_INTELLIGENT_LEARNS")]
    HasAnyIntelligentLearns,
    #[token_de(token = "HAS_ANY_INTELLIGENT_SPEAKS")]
    HasAnyIntelligentSpeaks,
    #[token_de(token = "HAS_ANY_LARGE_PREDATOR")]
    HasAnyLargePredator,
    #[token_de(token = "HAS_ANY_LOCAL_POPS_CONTROLLABLE")]
    HasAnyLocalPopsControllable,
    #[token_de(token = "HAS_ANY_LOCAL_POPS_PRODUCE_HEROES")]
    HasAnyLocalPopsProduceHeroes,
    #[token_de(token = "HAS_ANY_MEGABEAST")]
    HasAnyMegabeast,
    #[token_de(token = "HAS_ANY_MISCHIEVIOUS")]
    HasAnyMischievious,
    #[token_de(token = "HAS_ANY_NATURAL_ANIMAL")]
    HasAnyNaturalAnimal,
    #[token_de(token = "HAS_ANY_NIGHT_CREATURE")]
    HasAnyNightCreature,
    #[token_de(token = "HAS_ANY_NIGHT_CREATURE_BOGEYMAN")]
    HasAnyNightCreatureBogeyman,
    #[token_de(token = "HAS_ANY_NIGHT_CREATURE_EXPERIMENTER")]
    HasAnyNightCreatureExperimenter,
    #[token_de(token = "HAS_ANY_NIGHT_CREATURE_HUNTER")]
    HasAnyNightCreatureHunter,
    #[token_de(token = "HAS_ANY_NIGHT_CREATURE_NIGHTMARE")]
    HasAnyNightCreatureNightmare,
    #[token_de(token = "HAS_ANY_NOT_FIREIMMUNE")]
    HasAnyNotFireimmune,
    #[token_de(token = "HAS_ANY_NOT_FLIER")]
    HasAnyNotFlier,
    #[token_de(token = "HAS_ANY_NOT_LIVING")]
    HasAnyNotLiving,
    #[token_de(token = "HAS_ANY_OUTSIDER_CONTROLLABLE")]
    HasAnyOutsiderControllable,
    #[token_de(token = "HAS_ANY_POWER")]
    HasAnyPower,
    #[token_de(token = "HAS_ANY_RACE_GAIT")]
    HasAnyRaceGait,
    #[token_de(token = "HAS_ANY_SEMIMEGABEAST")]
    HasAnySemimegabeast,
    #[token_de(token = "HAS_ANY_SLOW_LEARNER")]
    HasAnySlowLearner,
    #[token_de(token = "HAS_ANY_SUPERNATURAL")]
    HasAnySupernatural,
    #[token_de(token = "HAS_ANY_TITAN")]
    HasAnyTitan,
    #[token_de(token = "HAS_ANY_UNIQUE_DEMON")]
    HasAnyUniqueDemon,
    #[token_de(token = "HAS_ANY_UTTERANCES")]
    HasAnyUtterances,
    #[token_de(token = "HAS_ANY_VERMIN_HATEABLE")]
    HasAnyVerminHateable,
    #[token_de(token = "HAS_ANY_VERMIN_MICRO")]
    HasAnyVerminMicro,
    #[token_de(token = "HAS_FEMALE")]
    HasFemale,
    #[token_de(token = "HAS_MALE")]
    HasMale,
    #[token_de(token = "LARGE_ROAMING")]
    LargeRoaming,
    #[token_de(token = "LOOSE_CLUSTERS")]
    LooseClusters,
    #[token_de(token = "MATES_TO_BREED")]
    MatesToBreed,
    #[token_de(token = "MUNDANE")]
    Mundane,
    #[token_de(token = "OCCURS_AS_ENTITY_RACE")]
    OccursAsEntityRace,
    #[token_de(token = "SAVAGE")]
    Savage,
    /// Applies to any vermin creature.
    #[token_de(token = "SMALL_RACE")]
    SmallRace,
    #[token_de(token = "TWO_GENDERS")]
    TwoGenders,
    #[token_de(token = "UBIQUITOUS")]
    Ubiquitous,
    #[token_de(token = "VERMIN_EATER")]
    VerminEater,
    #[token_de(token = "VERMIN_FISH")]
    VerminFish,
    #[token_de(token = "VERMIN_GROUNDER")]
    VerminGrounder,
    #[token_de(token = "VERMIN_ROTTER")]
    VerminRotter,
    #[token_de(token = "VERMIN_SOIL")]
    VerminSoil,
    #[token_de(token = "VERMIN_SOIL_COLONY")]
    VerminSoilColony,
}
impl Default for CreatureFlagEnum {
    fn default() -> Self {
        Self::AllCastesAlive
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum CasteFlagEnum {
    #[token_de(token = "ADOPTS_OWNER")]
    AdoptsOwner,
    #[token_de(token = "ALCOHOL_DEPENDENT")]
    AlcoholDependent,
    #[token_de(token = "ALL_ACTIVE")]
    AllActive,
    #[token_de(token = "AMBUSHPREDATOR")]
    Ambushpredator,
    #[token_de(token = "AQUATIC_UNDERSWIM")]
    AquaticUnderswim,
    #[token_de(token = "ARENA_RESTRICTED")]
    ArenaRestricted,
    #[token_de(token = "AT_PEACE_WITH_WILDLIFE")]
    AtPeaceWithWildlife,
    #[token_de(token = "BENIGN")]
    Benign,
    #[token_de(token = "BLOODSUCKER")]
    Bloodsucker,
    #[token_de(token = "BONECARN")]
    BoneCarn,
    #[token_de(token = "CANNOT_BREATHE_AIR")]
    CannotBreatheAir,
    #[token_de(token = "CANNOT_CLIMB")]
    CannotClimb,
    #[token_de(token = "CANNOT_JUMP")]
    CannotJump,
    #[token_de(token = "CANOPENDOORS")]
    CanOpenDoors,
    #[token_de(token = "CAN_BREATHE_WATER")]
    CanBreatheWater,
    #[token_de(token = "CAN_LEARN", alias = "INTELLIGENT_LEARNS")]
    CanLearn,
    #[token_de(token = "CAN_SPEAK", alias = "INTELLIGENT_SPEAKS")]
    CanSpeak,
    #[token_de(token = "CAN_SWIM")]
    CanSwim,
    #[token_de(token = "CAN_SWIM_INNATE")]
    CanSwimInnate,
    #[token_de(token = "CARNIVORE")]
    Carnivore,
    #[token_de(token = "CAVE_ADAPT")]
    CaveAdapt,
    #[token_de(token = "COLONY_EXTERNAL")]
    ColonyExternal,
    #[token_de(token = "COMMON_DOMESTIC")]
    CommonDomestic,
    #[token_de(token = "CONVERTED_SPOUSE")]
    ConvertedSpouse,
    #[token_de(token = "COOKABLE_LIVE")]
    CookableLive,
    #[token_de(token = "CRAZED")]
    Crazed,
    #[token_de(token = "CREPUSCULAR")]
    Crepuscular,
    #[token_de(token = "CURIOUS_BEAST")]
    CuriousBeast,
    #[token_de(token = "CURIOUS_BEAST_EATER")]
    CuriousBeastEater,
    #[token_de(token = "CURIOUS_BEAST_GUZZLER")]
    CuriousBeastGuzzler,
    #[token_de(token = "CURIOUS_BEAST_ITEM")]
    CuriousBeastItem,
    #[token_de(token = "DEMON")]
    Demon,
    #[token_de(token = "DIE_WHEN_VERMIN_BITE")]
    DieWhenVerminBite,
    #[token_de(token = "DIURNAL")]
    Diurnal,
    #[token_de(token = "DIVE_HUNTS_VERMIN")]
    DiveHuntsVermin,
    #[token_de(token = "EQUIPS")]
    Equips,
    #[token_de(token = "EXTRAVISION")]
    Extravision,
    #[token_de(token = "FEATURE_ATTACK_GROUP")]
    FeatureAttackGroup,
    #[token_de(token = "FEATURE_BEAST")]
    FeatureBeast,
    #[token_de(token = "FIREIMMUNE")]
    Fireimmune,
    #[token_de(token = "FIREIMMUNE_SUPER")]
    FireimmuneSuper,
    #[token_de(token = "FISHITEM")]
    FishItem,
    #[token_de(token = "FLEEQUICK")]
    FleeQuick,
    #[token_de(token = "FLIER")]
    Flier,
    #[token_de(token = "GELDABLE")]
    Geldable,
    #[token_de(token = "GETS_INFECTIONS_FROM_ROT")]
    GetsInfectionsFromRot,
    #[token_de(token = "GETS_WOUND_INFECTIONS")]
    GetsWoundInfections,
    #[token_de(token = "GNAWER")]
    Gnawer,
    #[token_de(token = "GRAZER")]
    Grazer,
    #[token_de(token = "HASSHELL")]
    HasShell,
    #[token_de(token = "HAS_BABYSTATE")]
    HasBabystate,
    #[token_de(token = "HAS_BLOOD")]
    HasBlood,
    #[token_de(token = "HAS_CHILDSTATE")]
    HasChildstate,
    #[token_de(token = "HAS_COLOR")]
    HasColor,
    #[token_de(token = "HAS_FLY_RACE_GAIT")]
    HasFlyRaceGait,
    #[token_de(token = "HAS_GLOW_COLOR")]
    HasGlowColor,
    #[token_de(token = "HAS_GLOW_TILE")]
    HasGlowTile,
    #[token_de(token = "HAS_GRASP")]
    HasGrasp,
    #[token_de(token = "HAS_NERVES")]
    HasNerves,
    #[token_de(token = "HAS_PUS")]
    HasPus,
    #[token_de(token = "HAS_RACE_GAIT")]
    HasRaceGait,
    #[token_de(token = "HAS_ROTTABLE")]
    HasRottable,
    #[token_de(token = "HAS_SECRETION")]
    HasSecretion,
    #[token_de(token = "HAS_SOLDIER_TILE")]
    HasSoldierTile,
    #[token_de(token = "HAS_SOUND_ALERT")]
    HasSoundAlert,
    #[token_de(token = "HAS_SOUND_PEACEFUL_INTERMITTENT")]
    HasSoundPeacefulIntermittent,
    #[token_de(token = "HAS_TILE")]
    HasTile,
    #[token_de(token = "HUNTS_VERMIN")]
    HuntsVermin,
    #[token_de(token = "IMMOBILE")]
    Immobile,
    #[token_de(token = "IMMOBILE_LAND")]
    ImmobileLand,
    #[token_de(token = "IMMOLATE")]
    Immolate,
    #[token_de(token = "ITEMCORPSE")]
    ItemCorpse,
    #[token_de(token = "LAIR_HUNTER")]
    LairHunter,
    #[token_de(token = "LARGE_PREDATOR")]
    LargePredator,
    #[token_de(token = "LAYS_EGGS")]
    LaysEggs,
    #[token_de(token = "LAYS_UNUSUAL_EGGS")]
    LaysUnusualEggs,
    #[token_de(token = "LIGAMENTS")]
    Ligaments,
    #[token_de(token = "LIGHT_GEN")]
    LightGen,
    #[token_de(token = "LISP")]
    Lisp,
    #[token_de(token = "LOCAL_POPS_CONTROLLABLE")]
    LocalPopsControllable,
    #[token_de(token = "LOCAL_POPS_PRODUCE_HEROES")]
    LocalPopsProduceHeroes,
    #[token_de(token = "LOCKPICKER")]
    Lockpicker,
    #[token_de(token = "MAGICAL")]
    Magical,
    #[token_de(token = "MAGMA_VISION")]
    MagmaVision,
    #[token_de(token = "MANNERISM_BREATH")]
    MannerismBreath,
    #[token_de(token = "MANNERISM_EYELIDS")]
    MannerismEyelids,
    #[token_de(token = "MANNERISM_LAUGH")]
    MannerismLaugh,
    #[token_de(token = "MANNERISM_POSTURE")]
    MannerismPosture,
    #[token_de(token = "MANNERISM_SIT")]
    MannerismSit,
    #[token_de(token = "MANNERISM_SMILE")]
    MannerismSmile,
    #[token_de(token = "MANNERISM_STRETCH")]
    MannerismStretch,
    #[token_de(token = "MANNERISM_WALK")]
    MannerismWalk,
    #[token_de(token = "MATUTINAL")]
    Matutinal,
    #[token_de(token = "MEANDERER")]
    Meanderer,
    #[token_de(token = "MEGABEAST")]
    Megabeast,
    #[token_de(token = "MILKABLE")]
    Milkable,
    #[token_de(token = "MISCHIEVIOUS")]
    Mischievious,
    #[token_de(token = "MOUNT")]
    Mount,
    #[token_de(token = "MOUNT_EXOTIC")]
    MountExotic,
    #[token_de(token = "MULTIPART_FULL_VISION")]
    MultipartFullVision,
    #[token_de(token = "MULTIPLE_LITTER_RARE")]
    MultipleLitterRare,
    #[token_de(token = "NATURAL_ANIMAL")]
    NaturalAnimal,
    #[token_de(token = "NIGHT_CREATURE")]
    NightCreature,
    #[token_de(token = "NIGHT_CREATURE_BOGEYMAN")]
    NightCreatureBogeyman,
    #[token_de(token = "NIGHT_CREATURE_EXPERIMENTER")]
    NightCreatureExperimenter,
    #[token_de(token = "NIGHT_CREATURE_HUNTER")]
    NightCreatureHunter,
    #[token_de(token = "NIGHT_CREATURE_NIGHTMARE")]
    NightCreatureNightmare,
    #[token_de(token = "NOBONES")]
    NoBones,
    #[token_de(token = "NOBREATHE")]
    NoBreathe,
    #[token_de(token = "NOCTURNAL")]
    Nocturnal,
    #[token_de(token = "NOEMOTION")]
    NoEmotion,
    #[token_de(token = "NOEXERT")]
    NoExert,
    #[token_de(token = "NOFEAR")]
    NoFear,
    #[token_de(token = "NOMEAT")]
    NoMeat,
    #[token_de(token = "NONAUSEA")]
    NoNausea,
    #[token_de(token = "NOPAIN")]
    NoPain,
    #[token_de(token = "NOSKIN")]
    NoSkin,
    #[token_de(token = "NOSKULL")]
    NoSkull,
    #[token_de(token = "NOSMELLYROT")]
    NoSmellyRot,
    #[token_de(token = "NOSTUCKINS")]
    NoStuckins,
    #[token_de(token = "NOSTUN")]
    NoStun,
    #[token_de(token = "NOTHOUGHT")]
    NoThought,
    #[token_de(token = "NOT_BUTCHERABLE")]
    NotButcherable,
    #[token_de(token = "NOT_LIVING")]
    NotLiving,
    #[token_de(token = "NO_AUTUMN")]
    NoAutumn,
    #[token_de(token = "NO_CONNECTIONS_FOR_MOVEMENT")]
    NoConnectionsForMovement,
    #[token_de(token = "NO_DIZZINESS")]
    NoDizziness,
    #[token_de(token = "NO_DRINK")]
    NoDrink,
    #[token_de(token = "NO_EAT")]
    NoEat,
    #[token_de(token = "NO_FEVERS")]
    NoFevers,
    #[token_de(token = "NO_PHYS_ATT_GAIN")]
    NoPhysAttGain,
    #[token_de(token = "NO_PHYS_ATT_RUST")]
    NoPhysAttRust,
    #[token_de(token = "NO_SLEEP")]
    NoSleep,
    #[token_de(token = "NO_SPRING")]
    NoSpring,
    #[token_de(token = "NO_SUMMER")]
    NoSummer,
    #[token_de(token = "NO_THOUGHT_CENTER_FOR_MOVEMENT")]
    NoThoughtCenterForMovement,
    #[token_de(token = "NO_UNIT_TYPE_COLOR")]
    NoUnitTypeColor,
    #[token_de(token = "NO_VEGETATION_PERTURB")]
    NoVegetationPerturb,
    #[token_de(token = "NO_WINTER")]
    NoWinter,
    #[token_de(token = "OPPOSED_TO_LIFE")]
    OpposedToLife,
    #[token_de(token = "OUTSIDER_CONTROLLABLE")]
    OutsiderControllable,
    #[token_de(token = "PACK_ANIMAL")]
    PackAnimal,
    #[token_de(token = "PARALYZEIMMUNE")]
    Paralyzeimmune,
    #[token_de(token = "PATTERNFLIER")]
    Patternflier,
    #[token_de(token = "PEARL")]
    Pearl,
    #[token_de(token = "PET")]
    Pet,
    #[token_de(token = "PET_EXOTIC")]
    PetExotic,
    #[token_de(token = "POWER")]
    Power,
    #[token_de(token = "REMAINS_ON_VERMIN_BITE_DEATH")]
    RemainsOnVerminBiteDeath,
    #[token_de(token = "REMAINS_UNDETERMINED")]
    RemainsUndetermined,
    #[token_de(token = "RETURNS_VERMIN_KILLS_TO_OWNER")]
    ReturnsVerminKillsToOwner,
    #[token_de(token = "SEMIMEGABEAST")]
    SemiMegabeast,
    #[token_de(token = "SLOW_LEARNER")]
    SlowLearner,
    #[token_de(token = "SPOUSE_CONVERSION_TARGET")]
    SpouseConversionTarget,
    #[token_de(token = "SPOUSE_CONVERTER")]
    SpouseConverter,
    #[token_de(token = "SPREAD_EVIL_SPHERES_IF_RULER")]
    SpreadEvilSpheresIfRuler,
    #[token_de(token = "STANCE_CLIMBER")]
    StanceClimber,
    #[token_de(token = "STRANGE_MOODS")]
    StrangeMoods,
    #[token_de(token = "SUPERNATURAL")]
    Supernatural,
    #[token_de(token = "TENDONS")]
    Tendons,
    #[token_de(token = "THICKWEB")]
    Thickweb,
    #[token_de(token = "TITAN")]
    Titan,
    #[token_de(token = "TRAINABLE_HUNTING")]
    TrainableHunting,
    #[token_de(token = "TRAINABLE_WAR")]
    TrainableWar,
    #[token_de(token = "TRANCES")]
    Trances,
    #[token_de(token = "TRAPAVOID")]
    Trapavoid,
    #[token_de(token = "UNIQUE_DEMON")]
    UniqueDemon,
    #[token_de(token = "UTTERANCES")]
    Utterances,
    #[token_de(token = "VEGETATION")]
    Vegetation,
    #[token_de(token = "VERMIN_GOBBLER")]
    VerminGobbler,
    #[token_de(token = "VERMIN_HATEABLE")]
    VerminHateable,
    #[token_de(token = "VERMIN_MICRO")]
    VerminMicro,
    #[token_de(token = "VERMIN_NOFISH")]
    VerminNoFish,
    #[token_de(token = "VERMIN_NOROAM")]
    VerminNoRoam,
    #[token_de(token = "VERMIN_NOTRAP")]
    VerminNoTrap,
    #[token_de(token = "VESPERTINE")]
    Vespertine,
    #[token_de(token = "WAGON_PULLER")]
    WagonPuller,
    #[token_de(token = "WEBBER")]
    Webber,
    #[token_de(token = "WEBIMMUNE")]
    Webimmune,
}
impl Default for CasteFlagEnum {
    fn default() -> Self {
        Self::AdoptsOwner
    }
}
