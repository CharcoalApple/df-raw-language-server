use df_ls_syntax_analysis::TokenDeserialize;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum LaborEnum {
    /// [Mining](https://dwarffortresswiki.org/index.php/Mining)
    #[token_de(token = "MINE")]
    Mine,
    /// [Stone hauling](https://dwarffortresswiki.org/index.php/Stone_hauling)
    #[token_de(token = "HAUL_STONE")]
    HaulStone,
    /// [Wood hauling](https://dwarffortresswiki.org/index.php/Wood_hauling)
    #[token_de(token = "HAUL_WOOD")]
    HaulWood,
    /// [Burial](https://dwarffortresswiki.org/index.php/Burial)
    #[token_de(token = "HAUL_BODY")]
    HaulBody,
    /// [Food hauling](https://dwarffortresswiki.org/index.php/Food_hauling)
    #[token_de(token = "HAUL_FOOD")]
    HaulFood,
    /// [Refuse hauling](https://dwarffortresswiki.org/index.php/Refuse_hauling)
    #[token_de(token = "HAUL_REFUSE")]
    HaulRefuse,
    /// [Item hauling](https://dwarffortresswiki.org/index.php/Item_hauling)
    #[token_de(token = "HAUL_ITEM")]
    HaulItem,
    /// [Furniture hauling](https://dwarffortresswiki.org/index.php/Furniture_hauling)
    #[token_de(token = "HAUL_FURNITURE")]
    HaulFurniture,
    /// [Animal hauling](https://dwarffortresswiki.org/index.php/Animal_hauling)
    #[token_de(token = "HAUL_ANIMALS")]
    HaulAnimals,
    /// [Cleaning](https://dwarffortresswiki.org/index.php/Cleaning)
    #[token_de(token = "CLEAN")]
    Clean,
    /// [Wood cutting](https://dwarffortresswiki.org/index.php/Wood_cutting)
    #[token_de(token = "CUTWOOD")]
    CutWood,
    /// [Carpentry](https://dwarffortresswiki.org/index.php/Carpentry)
    #[token_de(token = "CARPENTER")]
    Carpenter,
    /// [Stone detailing](https://dwarffortresswiki.org/index.php/Stone_detailing)
    #[token_de(token = "DETAIL")]
    Detail,
    /// [Masonry](https://dwarffortresswiki.org/index.php/Masonry)
    #[token_de(token = "MASON")]
    Mason,
    /// [Architecture](https://dwarffortresswiki.org/index.php/Architecture)
    #[token_de(token = "ARCHITECT")]
    Architect,
    /// [Animal training](https://dwarffortresswiki.org/index.php/Animal_training)
    #[token_de(token = "ANIMALTRAIN")]
    AnimalTrainer,
    /// [Animal care](https://dwarffortresswiki.org/index.php/Animal_care)
    #[token_de(token = "ANIMALCARE")]
    AnimalCare,
    /// [Diagnosis](https://dwarffortresswiki.org/index.php/Diagnosis)
    #[token_de(token = "DIAGNOSE")]
    Diagnoser,
    /// [Surgery](https://dwarffortresswiki.org/index.php/Surgery)
    #[token_de(token = "SURGERY")]
    Surgery,
    /// [Setting bones](https://dwarffortresswiki.org/index.php/Setting_bones)
    #[token_de(token = "BONE_SETTING")]
    BoneSetting,
    /// [Suturing](https://dwarffortresswiki.org/index.php/Suturing)
    #[token_de(token = "SUTURING")]
    Suturing,
    /// [Dressing wounds](https://dwarffortresswiki.org/index.php/Dressing_wounds)
    #[token_de(token = "DRESSING_WOUNDS")]
    DressingWounds,
    /// [Feed patients/prisoners](https://dwarffortresswiki.org/index.php/Feed_patients/prisoners)
    #[token_de(token = "FEED_WATER_CIVILIANS")]
    FeedWaterCivilians,
    /// [Recovering wounded](https://dwarffortresswiki.org/index.php/Recovering_wounded)
    #[token_de(token = "RECOVER_WOUNDED")]
    RecoveringWounded,
    /// [Butchery](https://dwarffortresswiki.org/index.php/Butchery)
    #[token_de(token = "BUTCHER")]
    Butchery,
    /// [Trapper](https://dwarffortresswiki.org/index.php/Trapper)
    #[token_de(token = "TRAPPER")]
    Trapper,
    /// [Small animal dissection](https://dwarffortresswiki.org/index.php/Small_animal_dissection)
    #[token_de(token = "DISSECT_VERMIN")]
    SmallAnimalDissection,
    /// [Leatherworking](https://dwarffortresswiki.org/index.php/Leatherworking)
    #[token_de(token = "LEATHER")]
    Leatherworker,
    /// [Tanning](https://dwarffortresswiki.org/index.php/Tanning)
    #[token_de(token = "TANNER")]
    Tanning,
    /// [Brewing](https://dwarffortresswiki.org/index.php/Brewing)
    #[token_de(token = "BREWER")]
    Brewing,
    /// [Alchemy](https://dwarffortresswiki.org/index.php/Alchemy)
    #[token_de(token = "ALCHEMIST")]
    Alchemy,
    /// [Soap making](https://dwarffortresswiki.org/index.php/Soap_making)
    #[token_de(token = "SOAP_MAKER")]
    SoapMaking,
    /// [Weaving](https://dwarffortresswiki.org/index.php/Weaving)
    #[token_de(token = "WEAVER")]
    Weaving,
    /// [Clothesmaking](https://dwarffortresswiki.org/index.php/Clothesmaking)
    #[token_de(token = "CLOTHESMAKER")]
    ClothesMaking,
    /// [Milling](https://dwarffortresswiki.org/index.php/Milling)
    #[token_de(token = "MILLER")]
    Milling,
    /// [Plant processing](https://dwarffortresswiki.org/index.php/Plant_processing)
    #[token_de(token = "PROCESS_PLANT")]
    PlantProcessing,
    /// [Cheese making](https://dwarffortresswiki.org/index.php/Cheese_making)
    #[token_de(token = "MAKE_CHEESE")]
    CheeseMaking,
    /// [Milking](https://dwarffortresswiki.org/index.php/Milking)
    #[token_de(token = "MILK")]
    Milking,
    /// [Cooking](https://dwarffortresswiki.org/index.php/Cooking)
    #[token_de(token = "COOK")]
    Cooking,
    /// [Farming (fields)](https://dwarffortresswiki.org/index.php/Farming_(fields))
    #[token_de(token = "PLANT")]
    Farming,
    /// [Plant gathering](https://dwarffortresswiki.org/index.php/Plant_gathering)
    #[token_de(token = "HERBALIST")]
    PlantGathering,
    /// [Fishing](https://dwarffortresswiki.org/index.php/Fishing)
    #[token_de(token = "FISH")]
    Fishing,
    /// [Fish cleaning](https://dwarffortresswiki.org/index.php/Fish_cleaning)
    #[token_de(token = "CLEAN_FISH")]
    FishCleaning,
    /// [Fish dissection](https://dwarffortresswiki.org/index.php/Fish_dissection)
    #[token_de(token = "DISSECT_FISH")]
    FishDissection,
    /// [Hunting](https://dwarffortresswiki.org/index.php/Hunting)
    #[token_de(token = "HUNT")]
    Hunting,
    /// [Furnace operating](https://dwarffortresswiki.org/index.php/Furnace_operating)
    #[token_de(token = "SMELT")]
    FurnaceOperating,
    /// [Weaponsmithing](https://dwarffortresswiki.org/index.php/Weaponsmithing)
    #[token_de(token = "FORGE_WEAPON")]
    WeaponSmithing,
    /// [Armoring](https://dwarffortresswiki.org/index.php/Armoring)
    #[token_de(token = "FORGE_ARMOR")]
    Armoring,
    /// [Blacksmithing](https://dwarffortresswiki.org/index.php/Blacksmithing)
    #[token_de(token = "FORGE_FURNITURE")]
    Blacksmithing,
    /// [Metalcrafting](https://dwarffortresswiki.org/index.php/Metalcrafting)
    #[token_de(token = "METAL_CRAFT")]
    MetalCrafting,
    /// [Gem cutting](https://dwarffortresswiki.org/index.php/Gem_cutting)
    #[token_de(token = "CUT_GEM")]
    GemCutting,
    /// [Gem setting](https://dwarffortresswiki.org/index.php/Gem_setting)
    #[token_de(token = "ENCRUST_GEM")]
    GemSetting,
    /// [Woodcrafting](https://dwarffortresswiki.org/index.php/Woodcrafting)
    #[token_de(token = "WOOD_CRAFT")]
    WoodCrafting,
    /// [Stonecrafting](https://dwarffortresswiki.org/index.php/Stonecrafting)
    #[token_de(token = "STONE_CRAFT")]
    StoneCrafting,
    /// [Bone carving](https://dwarffortresswiki.org/index.php/Bone_carving)
    #[token_de(token = "BONE_CARVE")]
    BoneCarving,
    /// [Glass making](https://dwarffortresswiki.org/index.php/Glassmaking)
    #[token_de(token = "GLASSMAKER")]
    GlassMaking,
    /// [Strand extraction](https://dwarffortresswiki.org/index.php/Strand_extraction)
    #[token_de(token = "EXTRACT_STRAND")]
    StrandExtraction,
    /// [Siege engineering](https://dwarffortresswiki.org/index.php/Siege_engineering)
    #[token_de(token = "SIEGECRAFT")]
    SiegeEngineering,
    /// [Siege operating](https://dwarffortresswiki.org/index.php/Siege_operating)
    #[token_de(token = "SIEGEOPERATE")]
    SiegeOperating,
    /// [Bowyer](https://dwarffortresswiki.org/index.php/Bowyer)
    #[token_de(token = "BOWYER")]
    Bowyer,
    /// [Mechanics](https://dwarffortresswiki.org/index.php/Mechanics)
    #[token_de(token = "MECHANIC")]
    Mechanics,
    /// [Potash making](https://dwarffortresswiki.org/index.php/Potash_making)
    #[token_de(token = "POTASH_MAKING")]
    PotashMaking,
    /// [Lye making](https://dwarffortresswiki.org/index.php/Lye_making)
    #[token_de(token = "LYE_MAKING")]
    LyeMaking,
    /// [Dyeing](https://dwarffortresswiki.org/index.php/Dyeing)
    #[token_de(token = "DYER")]
    Dyeing,
    /// [Wood burning](https://dwarffortresswiki.org/index.php/Wood_burning)
    #[token_de(token = "BURN_WOOD")]
    WoodBurning,
    /// [Pump operating](https://dwarffortresswiki.org/index.php/Pump_operating)
    #[token_de(token = "OPERATE_PUMP")]
    PumpOperating,
    /// [Shearing](https://dwarffortresswiki.org/index.php/Shearing)
    #[token_de(token = "SHEARER")]
    Shearing,
    /// [Spinning](https://dwarffortresswiki.org/index.php/Spinning)
    #[token_de(token = "SPINNER")]
    Spinning,
    /// [Pottery](https://dwarffortresswiki.org/index.php/Pottery)
    #[token_de(token = "POTTERY")]
    Pottery,
    /// [Glazing](https://dwarffortresswiki.org/index.php/Glazing)
    #[token_de(token = "GLAZING")]
    Glazing,
    /// [Pressing](https://dwarffortresswiki.org/index.php/Pressing)
    #[token_de(token = "PRESSING")]
    Pressing,
    /// [Beekeeping](https://dwarffortresswiki.org/index.php/Beekeeping)
    #[token_de(token = "BEEKEEPING")]
    Beekeeping,
    /// [Wax working](https://dwarffortresswiki.org/index.php/Wax_working)
    #[token_de(token = "WAX_WORKING")]
    WaxWorking,
    /// [Push/haul vehicles](https://dwarffortresswiki.org/index.php/Push/haul_vehicles)
    #[token_de(token = "HANDLE_VEHICLES")]
    HandleVehicles,
    /// [Trade](https://dwarffortresswiki.org/index.php/Trade)
    #[token_de(token = "HAUL_TRADE")]
    HaulTrade,
    /// [Lever](https://dwarffortresswiki.org/index.php/Lever)
    #[token_de(token = "PULL_LEVER")]
    PullLever,
    /// [Teardown and building disassembly](https://dwarffortresswiki.org/index.php/Construction)
    #[token_de(token = "REMOVE_CONSTRUCTION")]
    RemoveConstruction,
    /// Haul water from [activity zone](https://dwarffortresswiki.org/index.php/Activity_zone)
    #[token_de(token = "HAUL_WATER")]
    HaulWater,
    /// [Gelding](https://dwarffortresswiki.org/index.php/Gelding)
    #[token_de(token = "GELD")]
    Gelding,
    /// [Road](https://dwarffortresswiki.org/index.php/Road)
    #[token_de(token = "BUILD_ROAD")]
    BuildRoad,
    /// [Construction](https://dwarffortresswiki.org/index.php/Construction)
    #[token_de(token = "BUILD_CONSTRUCTION")]
    BuildConstruction,
}

impl Default for LaborEnum {
    fn default() -> Self {
        Self::Mine
    }
}
