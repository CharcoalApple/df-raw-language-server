mod common;
use df_ls_core::{Choose, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_material() {
    let source = "material_template

    [OBJECT:MATERIAL_TEMPLATE]

    [MATERIAL_TEMPLATE:STONE_TEMPLATE]
        [STATE_COLOR:ALL_SOLID:GRAY]
        [STATE_NAME:ALL_SOLID:stone]
        [STATE_ADJ:ALL_SOLID:stone]
        [STATE_COLOR:LIQUID:ORANGE]
        [STATE_NAME:LIQUID:magma]
        [STATE_ADJ:LIQUID:magma]
        [DISPLAY_COLOR:7:0:0]
        [MATERIAL_VALUE:1]
        [SPEC_HEAT:450]
        [IGNITE_POINT:NONE]
        [MELTING_POINT:12768]

    [MATERIAL_TEMPLATE:WOOD_TEMPLATE]
	    [STATE_COLOR:ALL_SOLID:BROWN]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "material_template".to_owned(),
            object_tokens: vec![ObjectToken {
                material_tokens: vec![
                    MaterialToken {
                        reference: Some(ReferenceTo::new("STONE_TEMPLATE".to_owned())),
                        display_color: Some((7, 0, 0)),
                        state_color: vec![
                            (
                                Choose::Choice2(AllOrAllSolidEnum::AllSolid),
                                ReferenceTo::new("GRAY".to_owned())
                            ),
                            (
                                Choose::Choice1(MaterialStateEnum::Liquid),
                                ReferenceTo::new("ORANGE".to_owned())
                            )
                        ],
                        state_name: vec![
                            (
                                Choose::Choice2(AllOrAllSolidEnum::AllSolid),
                                "stone".to_owned()
                            ),
                            (
                                Choose::Choice1(MaterialStateEnum::Liquid),
                                "magma".to_owned()
                            )
                        ],
                        state_adj: vec![
                            (
                                Choose::Choice2(AllOrAllSolidEnum::AllSolid),
                                "stone".to_owned()
                            ),
                            (
                                Choose::Choice1(MaterialStateEnum::Liquid),
                                "magma".to_owned()
                            )
                        ],
                        material_value: Some(1),
                        spec_heat: Some(Choose::Choice1(450)),
                        ignite_point: Some(Choose::Choice2(NoneEnum::None)),
                        melting_point: Some(Choose::Choice1(12768)),
                        reaction_class: vec![],
                        ..Default::default()
                    },
                    MaterialToken {
                        reference: Some(ReferenceTo::new("WOOD_TEMPLATE".to_owned())),
                        state_color: vec![(
                            Choose::Choice2(AllOrAllSolidEnum::AllSolid),
                            ReferenceTo::new("BROWN".to_owned())
                        )],
                        ..Default::default()
                    },
                ],
                ..Default::default()
            }],
        }
    );
}
