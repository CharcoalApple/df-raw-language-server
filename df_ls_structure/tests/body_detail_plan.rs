mod common;
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_structure::*;
use pretty_assertions::assert_eq;

#[test]
fn test_body_detail_plan() {
    let source = "body_detail_plan_header

    [OBJECT:BODY_DETAIL_PLAN]

    [BODY_DETAIL_PLAN:STANDARD_MATERIALS]
        [ADD_MATERIAL:SKIN:SKIN_TEMPLATE]

    [BODY_DETAIL_PLAN:STANDARD_TISSUES]
        [ADD_TISSUE:SKIN:SKIN_TEMPLATE]

    [BODY_DETAIL_PLAN:SHELL_POSITIONS]
        [BP_POSITION:BY_CATEGORY:SHELL:TOP]
        [BP_RELATION:BY_CATEGORY:SHELL:AROUND:BY_CATEGORY:UPPERBODY:50]

    [BODY_DETAIL_PLAN:HUMANOID_RELSIZES]
        [BP_RELSIZE:BY_CATEGORY:BODY:2000]

    [BODY_DETAIL_PLAN:FACIAL_HAIR_TISSUE_LAYERS]
        [BP_LAYERS:BY_CATEGORY:HEAD:MOUSTACHE:1:BELOW:BY_CATEGORY:NOSE]
        [BP_LAYERS:BY_CATEGORY:HEAD:CHIN_WHISKERS:1:BOTTOM]
        [BP_LAYERS:BY_CATEGORY:CHEEK:CHEEK_WHISKERS:1]
    
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (DFRaw, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    assert_eq!(diagnostic_list_lexer, vec![]);
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
    assert_eq!(
        structure,
        DFRaw {
            header: "body_detail_plan_header".to_owned(),
            object_tokens: vec![ObjectToken {
                body_detail_plan_tokens: vec![
                    BodyDetailPlanToken {
                        reference: Some(ReferenceTo::new("STANDARD_MATERIALS".to_owned())),
                        add_material: vec![(
                            Reference("SKIN".to_owned()),
                            ReferenceTo::new("SKIN_TEMPLATE".to_owned())
                        )],
                        ..Default::default()
                    },
                    BodyDetailPlanToken {
                        reference: Some(ReferenceTo::new("STANDARD_TISSUES".to_owned())),
                        add_tissue: vec![(
                            Reference("SKIN".to_owned()),
                            ReferenceTo::new("SKIN_TEMPLATE".to_owned())
                        )],
                        ..Default::default()
                    },
                    BodyDetailPlanToken {
                        reference: Some(ReferenceTo::new("SHELL_POSITIONS".to_owned())),
                        bp_position: vec![(
                            BpCriteriaTokenArg::ByCategory(Reference("SHELL".to_owned())),
                            PositionEnum::Top
                        )],
                        bp_relation: vec![(
                            BpCriteriaTokenArg::ByCategory(Reference("SHELL".to_owned())),
                            BpRelationEnum::Around,
                            BpCriteriaTokenArg::ByCategory(Reference("UPPERBODY".to_owned())),
                            Some(50),
                        )],
                        bp_relsize: vec![],
                        ..Default::default()
                    },
                    BodyDetailPlanToken {
                        reference: Some(ReferenceTo::new("HUMANOID_RELSIZES".to_owned())),
                        bp_relsize: vec![(
                            BpCriteriaTokenArg::ByCategory(Reference("BODY".to_owned())),
                            2000
                        )],
                        ..Default::default()
                    },
                    BodyDetailPlanToken {
                        reference: Some(ReferenceTo::new("FACIAL_HAIR_TISSUE_LAYERS".to_owned())),
                        bp_layers: vec![
                            (
                                BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                vec![BpLayerTokenArg {
                                    tissue: (Choose::Choice2(Reference("MOUSTACHE".to_owned())), 1),
                                    position_or_relation: Some(Choose::Choice2((
                                        BpRelationEnum::Below,
                                        BpCriteriaTokenArg::ByCategory(Reference(
                                            "NOSE".to_owned()
                                        )),
                                        None,
                                    ))),
                                }],
                            ),
                            (
                                BpCriteriaTokenArg::ByCategory(Reference("HEAD".to_owned())),
                                vec![BpLayerTokenArg {
                                    tissue: (
                                        Choose::Choice2(Reference("CHIN_WHISKERS".to_owned())),
                                        1
                                    ),
                                    position_or_relation: Some(Choose::Choice1(
                                        PositionEnum::Bottom
                                    )),
                                }],
                            ),
                            (
                                BpCriteriaTokenArg::ByCategory(Reference("CHEEK".to_owned())),
                                vec![BpLayerTokenArg {
                                    tissue: (
                                        Choose::Choice2(Reference("CHEEK_WHISKERS".to_owned())),
                                        1
                                    ),
                                    position_or_relation: None,
                                }],
                            ),
                        ],
                        ..Default::default()
                    },
                ],
                ..Default::default()
            }],
        }
    );
}
