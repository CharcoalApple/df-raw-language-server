#![forbid(unsafe_code)]
#![deny(clippy::all)]

use colored::*;
use df_ls_structure::DFRaw;
use log::LevelFilter;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use lsp_types::Diagnostic;
use structopt::StructOpt;

mod server;
mod server_log;

/// The available command line parameters.
#[derive(StructOpt, Debug)]
#[structopt(
    name = "df_language_server",
    about = "A language server for Dwarf Fortress RAW files."
)]
struct Opts {
    /// Activates debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Activates quiet mode, no log message in std out
    #[structopt(short, long)]
    quiet: bool,

    /// Verbose mode (-v, -vv)
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    /// All available subcommand
    #[structopt(subcommand)]
    cmd: Commands,
}

/// All available subcommands in DF Language server.
#[derive(StructOpt, Debug)]
enum Commands {
    /// Start Language server
    Start {
        /// Specify the port to start the server.
        ///
        /// The default debug port is `2087`.
        /// If no port is specified it will use an available port requested from the OS.
        #[structopt(short, long)]
        port: Option<u16>,
    },
    /// Parse Debug file
    // #[cfg(debug_assertions)]
    Debug {},
}

#[tokio::main]
async fn main() {
    let opts = Opts::from_args();
    // Setup logger and log level
    log::set_logger(&server_log::LOGGER).unwrap();
    if opts.debug {
        log::set_max_level(LevelFilter::Debug);
        if opts.verbose >= 2 {
            log::set_max_level(LevelFilter::Trace);
        }
    } else if opts.quiet {
        log::set_max_level(LevelFilter::Off);
    } else {
        match opts.verbose {
            0 => log::set_max_level(LevelFilter::Info),
            1 => log::set_max_level(LevelFilter::Debug),
            _ => log::set_max_level(LevelFilter::Trace),
        }
    }
    trace!("Command arguments: {:#?}", opts);
    // Check what subcommand we want to run
    match opts.cmd {
        Commands::Start { port } => {
            println!("Start Language server");
            server::start_language_server(port, opts.debug).await;
        }
        Commands::Debug {} => {
            println!("Debug");
            // Load source code from file
            let filename = "debug-test.txt";
            let source_code = std::fs::read(filename).expect("Could not read test file");
            check_source(&source_code, opts.debug);
        }
    }
    println!("Done!");
}

/// Check the source of errors and warnings
pub fn check_source(source: &[u8], debug: bool) -> Vec<Diagnostic> {
    use std::time::Instant;
    let now = Instant::now();
    // Convert source from CP437 to UTF-8
    let source_bytes = df_cp437::convert_cp437_to_utf8(source);
    let source = String::from_utf8(source_bytes).expect("Non UTF-8 characters found");
    // 1: Do Lexical Analysis (Tokenizer)
    print_in_hline("Start Lexical Analysis", debug);
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
    if debug {
        df_ls_lexical_analysis::print_ast(&tree, &source);
        println!("Diagnostics Lexer: {:#?}", diagnostic_list_lexer);
    }
    print_in_hline("End Lexical Analysis", debug);
    // 2: Do Syntax Analysis
    print_in_hline("Start Syntax Analysis", debug);
    let (structure, diagnostic_list_syntax): (DFRaw, Vec<Diagnostic>) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, &source);
    if debug {
        println!("Struct: {:#?}", structure);
        println!("Diagnostics syntax: {:#?}", diagnostic_list_syntax);
        df_ls_syntax_analysis::print_source_with_diagnostics(&source, &diagnostic_list_syntax);
    }
    print_in_hline("End Syntax Analysis", debug);

    // Disabled for initial release
    // 3: Do Semantic Analysis
    // print_in_hline("Start Semantic Analysis", debug);
    // let diagnostic_list_semantics = df_ls_semantic_analysis::do_semantic_analysis(&structure);
    // if debug {
    //     println!("Diagnostics semantics: {:#?}", diagnostic_list_semantics);
    // }
    // print_in_hline("End Semantic Analysis", debug);

    println!("This took: {} millisec", now.elapsed().as_millis());
    vec![
        diagnostic_list_lexer,
        diagnostic_list_syntax,
        // diagnostic_list_semantics,
    ]
    .concat()
}

fn print_in_hline(text: &str, debug: bool) {
    if debug {
        println!(
            "{}",
            format!("-------------------{}-------------------", text).bright_cyan()
        );
    }
}

#[test]
fn test_basic_df_raw() {
    use pretty_assertions::assert_eq;
    let source = "creature_domestic

    [OBJECT:CREATURE]
    
    [CREATURE:DOG]
        [CASTE:FEMALE]
            [FEMALE]
        [CASTE:MALE]
            [MALE]
        [SELECT_CASTE:ALL]
            [NATURAL]
    ";
    let diagnostic_list = crate::check_source(source.as_bytes(), true);
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
}

#[test]
fn test_basic_cp437_df_raw() {
    use pretty_assertions::assert_eq;
    let source = b"descriptor_color_standard

    [OBJECT:DESCRIPTOR_COLOR]
    
    Simple test\x9ctest
    
    [COLOR:AMBER]
        [NAME:ambe\x8cr]
        [RGB:255:191:0]
        [WORD:AMBER]
    ";
    let diagnostic_list = crate::check_source(source, true);
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(diagnostic_list, vec![]);
}
