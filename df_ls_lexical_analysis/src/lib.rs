#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![allow(clippy::manual_map)]

pub mod format;
mod tokenizer;

use df_ls_diagnostics::lsp_types::Diagnostic;
use std::rc::Rc;
pub use tokenizer::{Node, Point, Range, Tree, TreeCursor};
// Only used for fuzzing
pub use tokenizer::tokenize_df_raw_file;

pub fn do_lexical_analysis(source: &str) -> (Rc<Tree>, Vec<Diagnostic>) {
    tokenizer::tokenize_df_raw_file(source.to_owned(), true)
}

pub fn print_ast(tree: &Tree, source: &str) {
    let root_node = tree.root_node();
    format::print_ast(root_node, &source);
}
