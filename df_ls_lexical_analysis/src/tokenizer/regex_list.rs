use regex::Regex;

#[allow(dead_code)]
#[derive(Clone, Debug)]
pub(crate) struct RegexList {
    pub newline: Regex,
    pub line_space: Regex,
    pub comment: Regex,

    pub token_value_integer: Regex,
    pub token_value_reference: Regex,
    pub token_value_character: Regex,
    pub token_value_string: Regex,

    pub token_open_bracket: Regex,
    pub token_close_bracket: Regex,
    pub token_separator: Regex,

    pub header: Regex,
}

impl RegexList {
    #[allow(clippy::trivial_regex)]
    pub fn new() -> Self {
        Self {
            newline: Regex::new(r"\r?\n").unwrap(),
            line_space: Regex::new(r"[ \t]+").unwrap(),
            comment: Regex::new(r"[^\[\]]+").unwrap(),

            token_value_integer: Regex::new(r"-?[0-9]+").unwrap(),
            // Because of issue #38 this had to be updated.
            // Old regex: r"[0-9]*[A-Z][A-Z_0-9]*"
            token_value_reference: Regex::new(r"(?:VOLUME_mB)|(?:[0-9]*[A-Z][A-Z_0-9]*)").unwrap(),
            token_value_character: Regex::new(r"('.')").unwrap(),
            token_value_string: Regex::new(r"[^\[\]:\r\n]+").unwrap(),

            token_open_bracket: Regex::new(r"\[").unwrap(),
            token_close_bracket: Regex::new(r"\]").unwrap(),
            token_separator: Regex::new(r":").unwrap(),

            // Allow something that starts with word ([A-Za-z0-9_])
            // followed by any character except `\r`,`\n`,`[` or `]`.
            header: Regex::new(r"\w[^\r\n\[\]]*").unwrap(),
        }
    }
}
