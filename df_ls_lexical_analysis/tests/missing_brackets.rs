mod common;

use df_ls_diagnostics::lsp_types::*;
use pretty_assertions::assert_eq;

#[test]
fn test_missing_brackets_start() {
    let source = "descriptor_color_standard

    NAME:Amber]
    [NAME:Amber]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["unexpected_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 14,
            },
            end: Position {
                line: 2,
                character: 15,
            },
        }],
    );
}

#[test]
fn test_missing_brackets_end() {
    let source = "descriptor_color_standard

    [NAME:Amber
    [NAME:Amber]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_string)
    )
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 2,
                character: 15,
            },
            end: Position {
                line: 2,
                character: 15,
            },
        }],
    );
}

#[test]
fn test_wrong_missing_brackets() {
    let source = "creature_domestic

    [[]
    ]
    [[
    ][[][
    
    [CREAT:DOG]
        [CASTE:FEMALE]
            [FA[LE]
            [FEMALE]
    ";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 6,
                },
                end: Position {
                    line: 2,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 5,
                },
                end: Position {
                    line: 4,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 5,
                },
                end: Position {
                    line: 4,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 6,
                },
                end: Position {
                    line: 4,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 6,
                },
                end: Position {
                    line: 4,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 4,
                },
                end: Position {
                    line: 5,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 6,
                },
                end: Position {
                    line: 5,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 6,
                },
                end: Position {
                    line: 5,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 7,
                },
                end: Position {
                    line: 5,
                    character: 7,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 9,
                },
                end: Position {
                    line: 5,
                    character: 9,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 9,
                },
                end: Position {
                    line: 5,
                    character: 9,
                },
            },
            Range {
                start: Position {
                    line: 9,
                    character: 15,
                },
                end: Position {
                    line: 9,
                    character: 15,
                },
            },
        ],
    );
}

#[test]
fn test_missing_brackets_end_2() {
    let source = "creature_domestic

    [999";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (comment)
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
        ],
    );
}

#[test]
fn test_missing_brackets_end_3() {
    let source = "creature_domestic
[REF";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 1,
                character: 4,
            },
            end: Position {
                line: 1,
                character: 4,
            },
        }],
    );
}

#[test]
fn test_missing_brackets_extra_bracket() {
    let source = "creature_domestic
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITE[M:T1]";
    let (tree, diagnostic_list) = df_ls_lexical_analysis::do_lexical_analysis(source);
    println!("{}", tree.root_node().to_sexp(0));
    println!("Diagnostics: {:#?}", diagnostic_list);
    assert_eq!(
        tree.root_node().to_sexp(0),
        "(raw_file
  (header)
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (comment)
  (general_token
    ([)
    (token_value_reference)
    (])
  )
  (general_token
    ([)
    (token_value_reference)
    (general_token_values
      (:)
      (token_value_reference)
    )
    (])
  )
  (EOF)
)
"
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["missing_end_bracket".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 4,
                character: 12,
            },
            end: Position {
                line: 4,
                character: 12,
            },
        }],
    );
}
