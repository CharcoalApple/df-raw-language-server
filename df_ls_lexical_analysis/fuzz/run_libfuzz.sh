#!/bin/sh

# Install https://crates.io/crates/cargo-fuzz

# cargo install cargo-fuzz

cd ..
# Options `-D -j 4`
cargo +nightly fuzz run fuzz_lexer_source
