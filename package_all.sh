#!/bin/bash

# This script will build and package all extentions depending on the settings

# exit when any command fails
set -e

# List of all extentions that will be build
## Build the Language server (needed for all other extentions)
BUILD_LS=false
## Build and package the VSCode extension
PACKAGE_VSCODE=false

# Check what extentions need to be build
if [ "$1" = "all" ]; then
    # Build everything in order
    BUILD_LS=true
    PACKAGE_VSCODE=true
elif [ "$1" = "vscode" ]; then
    BUILD_LS=false
    PACKAGE_VSCODE=true
else
    : # No-op
fi


# Build (Rust) Language server
#
# Requirements:
# - `rustup` and `cargo` need to be installed
build_language_server () {
    if ! $BUILD_LS ; then
        echo "Not building Language server"
        return 0
    fi
    echo "Start building language server"
    # Build rust code using cargo
    # Because of multiple platform support we need to compile all targets

    # Linux
    rustup target install x86_64-unknown-linux-gnu
    # Windows
    # rustup target install x86_64-pc-windows-gnu
    # Mac OS
    # rustup target install x86_64-apple-darwin

    # In case build fails enable the following command to clear the `target` folder
    # cargo clean

    # Compile code for all targets
    # Linux
    cargo build --release --manifest-path df_language_server/Cargo.toml \
      --bin df_language_server --target x86_64-unknown-linux-gnu
    # Windows
    # cargo build --release --manifest-path df_language_server/Cargo.toml \
    #   --bin df_language_server --target x86_64-pc-windows-gnu
    # Mac OS
    # cargo build --release --manifest-path df_language_server/Cargo.toml \
    #   --bin df_language_server --target x86_64-apple-darwin

    # Now move all binaries to place where we can easally access them.
    mkdir -p ./artifacts
    # Copy all executables to this folder
    cp ./target/x86_64-unknown-linux-gnu/release/df_language_server \
        ./artifacts/df_language_server-x86_64-unknown-linux-gnu
    # cp ./target/x86_64-pc-windows-gnu/release/df_language_server.exe \
    #    ./artifacts/df_language_server-x86_64-pc-windows-gnu.exe
    # cp ./target/x86_64-apple-darwin/release/df_language_server \
    #    ./artifacts/df_language_server-x86_64-apple-darwin

    echo "Done building language server"
}


# Package VSCode extension
#
# Requirements:
# - Node.js and `npm` need to be installed
# - TypeScript compiler (`tsc`) need to be installed
# - Install `vsce` (used for packaging extension) `npm install -g vsce`
package_vscode_extension () {
    if ! $PACKAGE_VSCODE ; then
        echo "Not packaging VSCode extension"
        return 0
    fi
    echo "Start packaging VSCode extension"
    # Set client path for easy access
    CLIENT_PATH="./df_ls_clients/df_ls_vscode"
    # Save current path so we can return back later
    CURRENT_PATH=$(pwd)

    VSCE_PACKAGE_NAME="dwarf-fortress-raw-vscode-latest.vsix"

    # Creating output folder
    mkdir -p "${CLIENT_PATH}/out"
    # Copy executables
    cp ./artifacts/df_language_server-x86_64-unknown-linux-gnu "${CLIENT_PATH}/out/"
    cp ./artifacts/df_language_server-x86_64-pc-windows-gnu.exe "${CLIENT_PATH}/out/"
    # cp ./artifacts/df_language_server-x86_64-apple-darwin "${CLIENT_PATH}/out/"

    # Change to client folder
    cd $CLIENT_PATH

    # Install needed npm packages in order to build everything
    npm ci

    # Build TypeScript code (down by next command too)
    # npm run vscode:prepublish

    # Package extension using `vsce`
    # Install using: npm install -g vsce
    vsce package -o "$VSCE_PACKAGE_NAME"

    cp "$VSCE_PACKAGE_NAME" "${CURRENT_PATH}/artifacts/"

    # Change back to previous dir
    cd $CURRENT_PATH

    echo "Done packaging VSCode extension"
}

# Run build steps:

build_language_server

package_vscode_extension