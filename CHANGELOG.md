# Change log
In this file you will find all changes of the Dwarf Fortress language server.
This application follows the [Semantic Versioning standard](https://semver.org/).

## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## Version 0.2.0-alpha.1 (2021-08-01)

This is the initial version of the language server.
This version includes the following:
- Lexical Analysis for DF Raw files.
- Semantic Analysis for some of the DF Raw language parts:
body, body_detail_plan, building, creature (work in progress), descriptor_color, descriptor_pattern,
descriptor_shape, entity, interaction, item (some), language, material_template, plant, reaction,
tissue_template.
- This version is based on: Dwarf Fortress Classic 0.47.05 (2021-01-28)
- VSCode client
- Current supported features:
  - Syntax highlighting
  - Error reporting
