mod common;

use common::*;
use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_reference() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [PROFESSIONS:SLEEPING:20]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                professions: vec![(ReferenceTo::new("SLEEPING".to_owned()), 20u8)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

#[test]
fn test_reference_is_string() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [PROFESSIONS:SLeePING:20]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                professions: vec![(ReferenceTo::new("SLeePING".to_owned()), 20u8)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["reference_is_string".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 5,
                character: 21,
            },
            end: Position {
                line: 5,
                character: 29,
            },
        }],
    );
}

// Just to make sure not everything is allowed
#[test]
fn test_reference_is_number() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [PROFESSIONS:60:20]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 5,
                character: 21,
            },
            end: Position {
                line: 5,
                character: 23,
            },
        }],
    );
}
