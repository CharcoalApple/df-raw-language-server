use df_ls_core::{AllowEmpty, Choose, DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_derive::TokenDeserialize;
use df_ls_syntax_analysis::*;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct RefToken {
    /// Argument 1
    #[token_de(token = "REF")]
    pub reference: Option<Reference>,
    /// Token `NAME`
    #[token_de(token = "NAME")]
    pub name: Vec<String>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq)]
#[token_de(second_par_check, token = "MAIN")]
pub struct MainToken {
    #[token_de(token = "TYPE1")]
    pub type_1: Vec<Type1Token>,
    #[token_de(token = "TYPE2")]
    pub type_2: Vec<Type2Token>,
    #[token_de(token = "TYPE3")]
    pub type_3: Vec<String>,
    #[token_de(token = "TYPE4")]
    pub type_4: Vec<Type4Token>,
    #[token_de(token = "PROFESSION")]
    pub professions: Vec<ProfessionToken>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq)]
pub struct Type1Token {
    /// Argument 1
    #[token_de(token = "TYPE1", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    /// Token `NAME`
    #[token_de(token = "ITEM")]
    pub list: Vec<Reference>,
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq)]
pub enum Type2Token {
    #[token_de(token = "UNIPEDAL")]
    Unipedal(String),
    #[token_de(token = "BIPEDAL")]
    Bipedal(HumanToken),
    #[token_de(token = "TRIPEDAL")]
    Tripedal(RefToken),
    #[token_de(token = "4LEGS")]
    Quadrupedal(RefToken),
}
impl Default for Type2Token {
    fn default() -> Self {
        Self::Unipedal(String::default())
    }
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable)]
pub struct HumanToken {
    /// Argument 1
    #[token_de(token = "BIPEDAL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// Token `NAME`
    #[token_de(token = "NAME")]
    pub name: Option<String>,
    #[token_de(token = "DEAD", on_duplicate_error)]
    pub death: Option<()>,
    #[token_de(token = "SYMBOL")]
    pub symbol: Option<AllowEmpty<char>>,
    #[token_de(token = "GENDER")]
    pub gender: Option<GenderEnum>,
    #[token_de(token = "AGE")]
    pub age: Option<u8>,
    #[token_de(token = "JOB")]
    pub jobs: Vec<String>,
    #[token_de(token = "EDUCATION")]
    pub educations: Vec<(String, String, u8)>,
    #[token_de(token = "HOBBY")]
    pub hobbies: Vec<Choose<Reference, String>>,
    #[token_de(token = "MAIN_PROFESSION")]
    pub main_profession: Option<Choose<Reference, String>>,
    #[token_de(token = "MULTI")]
    pub multi_choose: Option<Choose<String, Choose<Reference, Choose<Choose<u32, i32>, DFChar>>>>,
    #[token_de(token = "PROFESSION")]
    pub profession: Option<ProfessionToken>,
    #[token_de(token = "PROFESSIONS")]
    pub professions: Vec<(ReferenceTo<ProfessionToken>, u8)>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable)]
pub struct ProfessionToken {
    #[token_de(token = "PROFESSION", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[token_de(token = "NAME", alias = "JOB")]
    pub name: Vec<Reference>,
    #[token_de(token = "GENDER")]
    pub genders: Vec<GenderEnum>,
    #[token_de(token = "HAND")]
    pub handedness: Option<Choose<u8, HandednessEnum>>,
    /// Optional arguments
    #[token_de(token = "ARG")]
    pub argument: Option<(String, Option<String>)>,
    /// Optional arguments
    #[token_de(token = "ARG_VEC")]
    pub arguments: Vec<(String, Option<String>)>,
    #[token_de(token = "SYMBOL")]
    pub symbol: Option<DFChar>,
    #[token_de(token = "DIFFICULTY")]
    pub difficulty: Option<u8>,
    #[token_de(token = "DIFFICULTY_HARD", on_duplicate_error)]
    pub difficulty_hard: Option<u8>,
    #[token_de(token = "TOOL")]
    pub tool: Option<ToolToken>,
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable)]
pub struct ToolToken {
    #[token_de(token = "TOOL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[token_de(token = "NAME")]
    pub name: Option<String>,
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum GenderEnum {
    #[token_de(token = "MALE")]
    Male,
    #[token_de(token = "FEMALE")]
    Female,
    #[token_de(token = "OTHER")]
    Other,
}

impl Default for GenderEnum {
    fn default() -> Self {
        Self::Other
    }
}

#[derive(Clone, Debug, TokenDeserialize, PartialEq)]
#[token_de(enum_value)]
pub enum HandednessEnum {
    #[token_de(token = "LEFT")]
    Left,
    #[token_de(token = "RIGHT")]
    Right,
    #[token_de(token = "AMBIDEXTROUS")]
    Ambidextrous,
    #[token_de(token = "NO_HANDS")]
    NoHands,
}

impl Default for HandednessEnum {
    fn default() -> Self {
        Self::Right
    }
}

#[derive(Clone, Debug, Default, TokenDeserialize, PartialEq, Referenceable)]
pub struct Type4Token {
    /// Argument 1
    #[token_de(token = "TYPE4", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<(String, ReferenceTo<Self>, String, String)>,
    /// Token `NAME`
    #[token_de(token = "NAME")]
    pub name: Option<String>,
}
