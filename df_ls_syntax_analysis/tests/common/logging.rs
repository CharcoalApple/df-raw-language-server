use colored::*;
use log::{Level, Metadata, Record};

/// An instance of the `Logger`.
pub static LOGGER: Logger = Logger;
/// The log collector and handler for most printed messages in terminal.
pub struct Logger;

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        // All messages need to be Trace or lower
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{:<5}:{} - {}",
                match record.level() {
                    Level::Error => "ERROR".bright_red(),
                    Level::Warn => "WARN".bright_yellow(),
                    Level::Info => "INFO".bright_blue(),
                    Level::Debug => "DEBUG".bright_green(),
                    Level::Trace => "TRACE".bright_magenta(),
                },
                record.target(),
                record.args()
            );
        }
    }

    fn flush(&self) {}
}
