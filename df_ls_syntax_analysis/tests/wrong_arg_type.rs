mod common;

use common::*;
use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

/// Testing `wrong_arg_type` error is produced when an int is given instead of a Ref
#[test]
fn wrong_arg_type_ref_to_int() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    [MAIN:TYPE1]

    [TYPE1:111]
        [ITEM:T1]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "wrong_arg_type".to_owned(),
            "token_is_missing".to_owned(),
            "unknown_token".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 8,
                    character: 11,
                },
                end: Position {
                    line: 8,
                    character: 14,
                },
            },
            Range {
                start: Position {
                    line: 9,
                    character: 8,
                },
                end: Position {
                    line: 9,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 9,
                    character: 8,
                },
                end: Position {
                    line: 9,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 9,
                    character: 17,
                },
                end: Position {
                    line: 10,
                    character: 4,
                },
            },
        ],
    );
}

/// Testing `wrong_arg_type` error is produced
/// when a char is given instead of a Ref
#[test]
fn wrong_arg_type_ref_to_char() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:'c']
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            type_2: vec![],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "wrong_arg_type".to_owned(),
            "token_is_missing".to_owned(),
            "token_not_expected".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 11,
                },
                end: Position {
                    line: 3,
                    character: 14,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 11,
                    character: 4,
                },
            },
        ],
    );
}

/// Testing `wrong_arg_type` error is produced when a string is given instead of a Ref
/// This should be allowed now with a warning, see #32
#[test]
fn wrong_arg_type_ref_to_string() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:something]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("something".to_owned())),
                list: vec![Reference("T1".to_owned())]
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["reference_is_string".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 3,
                character: 11,
            },
            end: Position {
                line: 3,
                character: 20,
            },
        }],
    );
}

/// Testing `wrong_arg_type` error is produced when an "empty" is given instead of a Ref
#[test]
fn wrong_arg_type_ref_to_empty() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![],
            type_2: vec![],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec![
            "wrong_arg_type".to_owned(),
            "token_is_missing".to_owned(),
            "token_not_expected".to_owned(),
            "unchecked_code".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 11,
                },
                end: Position {
                    line: 3,
                    character: 11,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 11,
                    character: 4,
                },
            },
        ],
    );
}

/// Testing `wrong_arg_type` error is produced when an int is given instead of a string
#[test]
fn wrong_arg_type_string_to_int() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:111]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: None,
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 9,
                character: 14,
            },
            end: Position {
                line: 9,
                character: 17,
            },
        }],
    );
}

/// Testing `wrong_arg_type` error is produced
/// when a char is given instead of a string
#[test]
fn wrong_arg_type_string_to_char() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:'A']
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: None,
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 9,
                character: 14,
            },
            end: Position {
                line: 9,
                character: 17,
            },
        }],
    );
}

/// Testing `wrong_arg_type` error is produced when a reference is given instead of a string
#[test]
fn wrong_arg_type_string_to_ref() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:NO_NAME]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: None,
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 9,
                character: 14,
            },
            end: Position {
                line: 9,
                character: 21,
            },
        }],
    );
}

/// Testing `wrong_arg_type` error is produced when an "empty" is given instead of a string
#[test]
fn wrong_arg_type_string_to_empty() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:]
        [EDUCATION:inn:beer:5]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: None,
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 9,
                character: 14,
            },
            end: Position {
                line: 9,
                character: 14,
            },
        }],
    );
}

// Testing `wrong_arg_type` errors are produced
// when a string is given instead an int in a multi-int token
#[test]
fn wrong_arg_type_int() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:my name]
        [EDUCATION:inn:beer:to long]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("my name".to_owned()),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 10,
                character: 28,
            },
            end: Position {
                line: 10,
                character: 35,
            },
        }],
    );
}

// Testing 3 `wrong_arg_type` errors are produced
// when a char, reference and string are given instead of 3 ints
#[test]
fn wrong_arg_type_int_multiple() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]

    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:my name]
        [EDUCATION:inn:beer:'c']
        [EDUCATION:inn:beer:REF]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("my name".to_owned()),
                ..Default::default()
            })],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["wrong_arg_type".to_owned(), "wrong_arg_type".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 10,
                    character: 28,
                },
                end: Position {
                    line: 10,
                    character: 31,
                },
            },
            Range {
                start: Position {
                    line: 11,
                    character: 28,
                },
                end: Position {
                    line: 11,
                    character: 31,
                },
            },
        ],
    );
}
