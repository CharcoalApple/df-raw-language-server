mod common;

use common::*;
use df_ls_core::{DFChar, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_df_char_value() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [SYMBOL:'M']
    [PROFESSION:MONSTER2]
        [SYMBOL:1]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    symbol: Some(DFChar('M')),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    symbol: Some(DFChar('☺')),
                    ..Default::default()
                }
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

#[test]
fn test_df_char_error() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [SYMBOL:600]
    [PROFESSION:MONSTER2]
        [SYMBOL:-5]
    [PROFESSION:FIRE_MONSTER]
        [SYMBOL:'🔥']
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    symbol: None,
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    symbol: None,
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("FIRE_MONSTER".to_owned())),
                    symbol: Some(DFChar('🔥')), //TODO: This is not an allowed character (see #67)
                    ..Default::default()
                },
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["too_large_int".to_owned(), "too_small_int".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 16,
                },
                end: Position {
                    line: 4,
                    character: 19,
                },
            },
            Range {
                start: Position {
                    line: 6,
                    character: 16,
                },
                end: Position {
                    line: 6,
                    character: 18,
                },
            },
        ],
    );
}
