mod common;

use common::*;
use df_ls_core::Reference;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

/// Regression test for issue #22
#[test]
fn missing_close_brackets_object() {
    let source = "
    [MAIN:TYPE1

    [TYPE1:DOG]
        [ITEM:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 1,
                    character: 15,
                },
                end: Position {
                    line: 1,
                    character: 15,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

/// Regression test for issue #22
#[test]
fn missing_open_brackets_object() {
    let source = "
    MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 1,
                    character: 14,
                },
                end: Position {
                    line: 1,
                    character: 15,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(structure, MainToken::default());
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["token_not_expected".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 15,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 15,
                },
                end: Position {
                    line: 4,
                    character: 17,
                },
            },
        ],
    );
}

/// Regression test for issue #23
#[test]
fn extra_brackets_object() {
    let source = "

    [[]
    ]
    [[
    ][[][

    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITEM:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "unexpected_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "unexpected_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
            "missing_token_name".to_owned(),
            "missing_token_name".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 6,
                },
                end: Position {
                    line: 2,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 5,
                },
                end: Position {
                    line: 4,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 5,
                },
                end: Position {
                    line: 4,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 6,
                },
                end: Position {
                    line: 4,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 6,
                },
                end: Position {
                    line: 4,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 4,
                },
                end: Position {
                    line: 5,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 6,
                },
                end: Position {
                    line: 5,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 6,
                },
                end: Position {
                    line: 5,
                    character: 6,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 7,
                },
                end: Position {
                    line: 5,
                    character: 7,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 9,
                },
                end: Position {
                    line: 5,
                    character: 9,
                },
            },
            Range {
                start: Position {
                    line: 5,
                    character: 9,
                },
                end: Position {
                    line: 5,
                    character: 9,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(structure, MainToken::default());
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["token_not_expected".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 2,
                    character: 4,
                },
                end: Position {
                    line: 2,
                    character: 5,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 5,
                },
                end: Position {
                    line: 10,
                    character: 17,
                },
            },
        ],
    );
}

/// Regression test for issue #23
#[test]
fn extra_open_bracket_tokens() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITE[M:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "missing_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 12,
                },
                end: Position {
                    line: 4,
                    character: 12,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["unknown_token".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 12,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 12,
                },
                end: Position {
                    line: 4,
                    character: 18,
                },
            },
        ],
    );
}

/// Regression test for issue #23
#[test]
fn extra_close_bracket_tokens() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITE]M:T1]";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(
        &diagnostic_list_lexer,
        vec![
            "missing_header".to_owned(),
            "unexpected_end_bracket".to_owned(),
        ],
    );
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![
            Range {
                start: Position {
                    line: 0,
                    character: 0,
                },
                end: Position {
                    line: 0,
                    character: 0,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 4,
                    character: 18,
                },
            },
        ],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["unknown_token".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 13,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 13,
                },
                end: Position {
                    line: 4,
                    character: 18,
                },
            },
        ],
    )
}
