mod common;

use common::*;
use df_ls_core::Reference;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

/// Regression test for issue #3 and #2
#[test]
fn misspelled_token() {
    let source = "
    [MAIN:TYPE1]

    [TYPE1:DOG]
        [ITM:T1]
        [ITEM:T1]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["unknown_token".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 8,
                },
                end: Position {
                    line: 4,
                    character: 16,
                },
            },
            Range {
                start: Position {
                    line: 4,
                    character: 16,
                },
                end: Position {
                    line: 6,
                    character: 4,
                },
            },
        ],
    )
}

/// Regression test for issue #21
#[test]
fn misspelled_object_token() {
    let source = "
    [MAIN:TYPE1]

    [TYPE:DOG]
        [ITEM:T1]
        [ITEM:T1]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(structure, MainToken::default());
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["token_not_expected".to_owned(), "unchecked_code".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 3,
                    character: 4,
                },
                end: Position {
                    line: 3,
                    character: 14,
                },
            },
            Range {
                start: Position {
                    line: 3,
                    character: 14,
                },
                end: Position {
                    line: 6,
                    character: 4,
                },
            },
        ],
    )
}
