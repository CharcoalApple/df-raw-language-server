mod common;

use common::*;
use df_ls_core::{Reference, ReferenceTo};
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_multi_level_back_up_stack() {
    let source = "
    [MAIN:TYPE2]

    [BIPEDAL:MONSTER]
        [NAME:test]
        [PROFESSION:SLEEPING]
            [NAME:SLEEPING]
            [TOOL:BED]
                [NAME:bed]
    [BIPEDAL:HUMAN]
        [NAME:also sleepy]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_2: vec![
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    name: Some("test".to_owned()),
                    profession: Some(ProfessionToken {
                        reference: Some(ReferenceTo::new("SLEEPING".to_owned())),
                        name: vec![Reference("SLEEPING".to_owned())],
                        tool: Some(ToolToken {
                            reference: Some(ReferenceTo::new("BED".to_owned())),
                            name: Some("bed".to_owned()),
                        }),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("HUMAN".to_owned())),
                    name: Some("also sleepy".to_owned()),
                    ..Default::default()
                })
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
