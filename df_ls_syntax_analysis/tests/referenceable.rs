mod common;

use common::*;
use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_reference_to_self_tuple() {
    let source = "
    [MAIN:TYPE4]

    [TYPE4:string:REF:String:More]
        [NAME:some name]
    [TYPE4:string:REF2:String:More]
        [NAME:some name]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            type_4: vec![
                Type4Token {
                    reference: Some((
                        "string".to_owned(),
                        ReferenceTo::new("REF".to_owned()),
                        "String".to_owned(),
                        "More".to_owned()
                    )),
                    name: Some("some name".to_owned()),
                    ..Default::default()
                },
                Type4Token {
                    reference: Some((
                        "string".to_owned(),
                        ReferenceTo::new("REF2".to_owned()),
                        "String".to_owned(),
                        "More".to_owned()
                    )),
                    name: Some("some name".to_owned()),
                    ..Default::default()
                }
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}
