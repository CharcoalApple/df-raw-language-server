mod common;

use common::*;
use df_ls_core::ReferenceTo;
use df_ls_diagnostics::lsp_types::*;
use df_ls_syntax_analysis::*;
use pretty_assertions::assert_eq;

#[test]
fn test_option_value() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [ARG:string:string]
    [PROFESSION:MONSTER2]
        [ARG:string]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    argument: Some(("string".to_owned(), Some("string".to_owned()))),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    argument: Some(("string".to_owned(), None)),
                    ..Default::default()
                }
            ],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec![]);
    common::assert_diagnostic_range(&diagnostic_list, vec![]);
}

#[test]
fn test_option_error() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [ARG:string:string:string]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                argument: None,
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_number".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![Range {
            start: Position {
                line: 4,
                character: 8,
            },
            end: Position {
                line: 4,
                character: 34,
            },
        }],
    );
}

#[test]
fn test_option_vec_with_error() {
    let source = "
    [MAIN:PROFESSION]

    [PROFESSION:MONSTER]
        [ARG_VEC:REF:REF]
        [ARG_VEC:String:String]
        [ARG_VEC:666:666]
    ";
    let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(source);
    let (structure, diagnostic_list): (MainToken, _) = do_syntax_analysis(&tree, source);

    println!("Lexer: {:#?}", diagnostic_list_lexer);
    common::assert_diagnostic_codes(&diagnostic_list_lexer, vec!["missing_header".to_owned()]);
    common::assert_diagnostic_range(
        &diagnostic_list_lexer,
        vec![Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        }],
    );
    println!("{:#?}", structure);
    println!("{:#?}", diagnostic_list);
    assert_eq!(
        structure,
        MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                arguments: vec![("String".to_owned(), Some("String".to_owned())),],
                ..Default::default()
            }],
            ..Default::default()
        }
    );
    common::assert_diagnostic_codes(
        &diagnostic_list,
        vec!["wrong_arg_type".to_owned(), "wrong_arg_type".to_owned()],
    );
    common::assert_diagnostic_range(
        &diagnostic_list,
        vec![
            Range {
                start: Position {
                    line: 4,
                    character: 17,
                },
                end: Position {
                    line: 4,
                    character: 20,
                },
            },
            Range {
                start: Position {
                    line: 6,
                    character: 17,
                },
                end: Position {
                    line: 6,
                    character: 20,
                },
            },
        ],
    );
}
