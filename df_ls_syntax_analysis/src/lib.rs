#![forbid(unsafe_code)]
#![deny(clippy::all)]
// TODO: Remove this at later point can create custom errors.
#![allow(clippy::result_unit_err)]

mod token_deserializers;

use colored::*;
pub use df_ls_derive::TokenDeserialize;
use df_ls_diagnostics::lsp_types::{Diagnostic, DiagnosticSeverity};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticMessageSet, DiagnosticsInfo};
pub use df_ls_lexical_analysis::{Node, Tree, TreeCursor};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
pub use token_deserializers::{
    argument_to_token_name, Argument, LoopControl, Token, TokenDeserialize, TokenDeserializeBasics,
    TokenValue, TryFromArgument, TryFromArgumentGroup,
};

pub fn do_syntax_analysis<T: TokenDeserialize>(tree: &Tree, source: &str) -> (T, Vec<Diagnostic>) {
    let mut tree_cursor = tree.walk();
    tree_cursor.goto_first_child();
    let mut diagnostic_info = DiagnosticsInfo::load_from_file(
        DiagnosticMessageSet::Syntax,
        Some("DF RAW Language Server".to_owned()),
    );
    let structure =
        match TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
        {
            Ok(value) => {
                // Check if EOF is reached
                let new_node = tree_cursor.node();
                if new_node.next_sibling().is_none() {
                    // EOF was reached
                    tree_cursor.goto_parent();
                } else {
                    // EOF was Not reached, there is still more to parse.
                    // So parsing stopped on Error or unknown token
                    // mark token as not expected too
                    // TODO others errors might be possible here.
                    let token =
                        Token::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                            .expect("Token could not be deserialized.");
                    let token_name = token.get_current_arg_opt().unwrap().value.to_string();
                    diagnostic_info.add_message(
                        DMExtraInfo {
                            range: new_node.get_range(),
                            message_template_data: hash_map! {
                                "token_name" => token_name,
                            },
                        },
                        "unknown_token",
                    );
                    mark_rest_of_file_as_unchecked(
                        &mut tree_cursor,
                        &mut diagnostic_info,
                        &new_node,
                    );
                }
                value
            }
            Err(_) => {
                let new_node = tree_cursor.node();
                // mark token as not expected too
                diagnostic_info
                    .add_message(DMExtraInfo::new(new_node.get_range()), "token_not_expected");
                mark_rest_of_file_as_unchecked(&mut tree_cursor, &mut diagnostic_info, &new_node);
                T::default()
            }
        };
    (structure, diagnostic_info.diagnostics)
}

pub fn mark_rest_of_file_as_unchecked(
    cursor: &mut TreeCursor,
    diagnostics: &mut DiagnosticsInfo,
    node: &Node,
) {
    use df_ls_diagnostics::lsp_types::*;
    // Get root node of AST
    let root_node = {
        while cursor.goto_parent() {}
        cursor.node()
    };
    let start_pos = Position {
        line: node.end_position().row as u32,
        character: node.end_position().column as u32,
    };
    let end_pos = Position {
        line: root_node.end_position().row as u32,
        character: root_node.end_position().column as u32,
    };
    // Check if there are characters left to not check
    if start_pos == end_pos {
        // EOF is here, so do not add message
        return;
    }
    diagnostics.add_message(
        DMExtraInfo::new(Range {
            start: start_pos,
            end: end_pos,
        }),
        "unchecked_code",
    );
}

pub fn print_source_with_diagnostics(source: &str, diagnostics: &[Diagnostic]) {
    println!("--------Output---------");
    for (line_nr, line) in source.split('\n').enumerate() {
        let mut new_line = "".to_owned();
        for (ch_nr, ch) in line.chars().enumerate() {
            match get_pos_severity(line_nr as u32, ch_nr as u32, diagnostics) {
                Some(severity) => match severity {
                    DiagnosticSeverity::Information => {
                        new_line.push_str(&ch.to_string().bright_blue().to_string())
                    }
                    DiagnosticSeverity::Hint => {
                        new_line.push_str(&ch.to_string().bright_blue().to_string())
                    }
                    DiagnosticSeverity::Warning => {
                        new_line.push_str(&ch.to_string().bright_yellow().to_string())
                    }
                    DiagnosticSeverity::Error => {
                        new_line.push_str(&ch.to_string().bright_red().to_string())
                    }
                },
                None => new_line.push(ch),
            }
        }
        println!("{}", new_line);
    }
    println!("----------------------");
}

fn get_pos_severity(
    line: u32,
    character: u32,
    diagnostics: &[Diagnostic],
) -> Option<DiagnosticSeverity> {
    let mut severity = None;
    for item in diagnostics {
        // check start
        if line >= item.range.start.line && line <= item.range.end.line {
            // Middle of multiple lines
            if line > item.range.start.line && line < item.range.end.line {
                // get severity
                if severity < item.severity || severity.is_none() {
                    severity = item.severity;
                }
            }
            // First line of multiple
            else if line < item.range.end.line {
                if character >= item.range.start.character {
                    // get severity
                    if severity < item.severity || severity.is_none() {
                        severity = item.severity;
                    }
                }
            }
            // Last line of multiple
            else if line > item.range.start.line {
                if character < item.range.end.character {
                    // get severity
                    if severity < item.severity || severity.is_none() {
                        severity = item.severity;
                    }
                }
            }
            // Only one line
            else if character >= item.range.start.character
                && character < item.range.end.character
            {
                // get severity
                if severity < item.severity || severity.is_none() {
                    severity = item.severity;
                }
            }
        }
    }
    severity
}
