/// Implement `TokenDeserialize` for a token with `[REF:ty]`
/// There `ty` is the type given in the argument
/// So it adds: `impl TokenDeserialize for ty {...}`
#[macro_export]
macro_rules! token_deserialize_unary_token {
    ( $x:ty ) => {
        /// Deserialize a token with following pattern: `[REF:ty]`
        impl TokenDeserialize for $x {
            fn deserialize_tokens(
                mut cursor: &mut $crate::TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
            ) -> Result<Self, ()> {
                // Get arguments from token
                let mut token =
                    $crate::Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                $crate::Token::consume_token(&mut cursor)?;
                // Arg 0
                token.check_token_arg0(source, &mut diagnostics, true)?;
                token.consume_argument();
                // Arg 1
                let result =
                    Self::try_from_argument_group(&mut token, source, &mut diagnostics, true);
                // Just check if all tokens are consumed if no other errors are present
                if result.is_ok() {
                    token.check_all_arg_consumed(source, &mut diagnostics, true)?;
                }
                result
            }

            fn deserialize_general_token(
                _cursor: &mut $crate::TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                new_self: Self,
            ) -> ($crate::LoopControl, Self) {
                ($crate::LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> $crate::LoopControl {
                $crate::LoopControl::DoNothing
            }

            fn get_allowed_tokens() -> Option<Vec<$crate::TokenValue>> {
                None
            }
        }
    };
    ( $x:ty, $t:tt ) => {
        /// Deserialize a token with following pattern: `[REF:ty]`
        impl<$t> TokenDeserialize for $x
        where
            $t: Default + $crate::TryFromArgumentGroup,
            $x: $crate::TryFromArgumentGroup + Default,
        {
            fn deserialize_tokens(
                mut cursor: &mut $crate::TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
            ) -> Result<Self, ()> {
                // Get arguments from token
                let mut token =
                    $crate::Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                $crate::Token::consume_token(&mut cursor)?;
                // Arg 0
                token.check_token_arg0(source, &mut diagnostics, true)?;
                token.consume_argument();
                // Arg 1
                let result =
                    Self::try_from_argument_group(&mut token, source, &mut diagnostics, true);
                token.check_all_arg_consumed(source, &mut diagnostics, true)?;
                result
            }

            fn deserialize_general_token(
                _cursor: &mut $crate::TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                new_self: Self,
            ) -> ($crate::LoopControl, Self) {
                ($crate::LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> $crate::LoopControl {
                $crate::LoopControl::DoNothing
            }

            fn get_allowed_tokens() -> Option<Vec<$crate::TokenValue>> {
                None
            }
        }
    };
}
