use super::{argument_to_token_name, TokenValue};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::{Node, TreeCursor};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Token {
    pub node: Option<Node>,
    pub(crate) arguments: Vec<Argument>,
    pub current_argument_index: usize,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Argument {
    pub node: Node,
    pub value: TokenValue,
}

impl Token {
    pub fn get_argument(&self, index: usize) -> Result<&Argument, ()> {
        match self.arguments.get(index) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }

    /// Get current argument
    pub fn get_current_arg(&self) -> Result<&Argument, ()> {
        match self.arguments.get(self.current_argument_index) {
            Some(v) => Ok(v),
            None => Err(()),
        }
    }

    /// Get current argument as an `Option`
    pub fn get_current_arg_opt(&self) -> Option<&Argument> {
        self.arguments.get(self.current_argument_index)
    }

    /// Get current argument and make sure it exists
    /// If it does not exists, add diagnostics error
    pub fn checked_get_current_arg(
        &self,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Argument, ()> {
        match self.arguments.get(self.current_argument_index) {
            Some(arg) => Ok(arg.clone()),
            None => {
                if add_diagnostics_on_err {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: self.node.as_ref().unwrap().get_range(),
                            message_template_data: hash_map! {
                                "expected_parameters_num" => self.current_argument_index.to_string(),
                                "found_parameters_num" => self.arguments.len().to_string(),
                            },
                        },
                        "wrong_arg_number",
                    );
                }
                Err(())
            }
        }
    }

    /// Consume current argument and set index to next argument
    pub fn consume_argument(&mut self) {
        self.current_argument_index += 1;
    }

    /// Make sure the current argument exists
    /// If it does not exists, add a diagnostics error
    pub fn check_as_required_argument(
        &self,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        if self.get_current_arg_opt().is_some() {
            Ok(())
        } else if add_diagnostics_on_err {
            error!("err in consume_required_argument");
            diagnostics.add_message(
                DMExtraInfo {
                    range: self.node.as_ref().unwrap().get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters_num" => format!("at least {}", self.current_argument_index),
                        "found_parameters_num" => (self.arguments.len()-1).to_string(),
                    },
                },
                "wrong_arg_number",
            );
            Err(())
        } else {
            Err(())
        }
    }

    /// Make sure all arguments in the token are consumed
    /// and there are no extras.
    pub fn check_all_arg_consumed(
        &self,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        if self.current_argument_index == self.arguments.len() {
            Ok(())
        } else if add_diagnostics_on_err {
            error!("err in check_all_arg_consumed");
            diagnostics.add_message(
                DMExtraInfo {
                    range: self.node.as_ref().unwrap().get_range(),
                    message_template_data: hash_map! {
                        "expected_parameters_num" => (self.current_argument_index-1).to_string(),
                        "found_parameters_num" => (self.arguments.len()-1).to_string(),
                    },
                },
                "wrong_arg_number",
            );
            Err(())
        } else {
            Err(())
        }
    }

    /// Check if the first argument (token name) exists and is of the right type
    pub fn check_token_arg0(
        &self,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<(), ()> {
        if self.current_argument_index != 0 {
            error!("checking arg0 but `current_argument_index` is not on arg 0.");
            debug_assert!(
                false,
                "checking arg0 but `current_argument_index` is not on arg 0."
            );
            return Err(());
        }
        // First Token is always a REF
        let first_arg = self.get_current_arg()?;
        if let TokenValue::TVReference(_) = first_arg.value {
            // Correct
            Ok(())
        } else {
            // Incorrect
            if add_diagnostics_on_err {
                diagnostics.add_message(
                    DMExtraInfo {
                        range: first_arg.node.get_range(),
                        message_template_data: hash_map! {
                            "expected_parameters" => "Reference".to_owned(),
                            "found_parameters" => argument_to_token_name(&first_arg.value),
                        },
                    },
                    "wrong_arg_type",
                );
            }
            Err(())
        }
    }

    pub fn consume_token(cursor: &mut TreeCursor) -> Result<(), ()> {
        match cursor.goto_next_sibling() {
            true => Ok(()),
            false => Err(()),
        }
    }
}
