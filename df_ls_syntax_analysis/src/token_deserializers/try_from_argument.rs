use super::Argument;
use df_ls_diagnostics::DiagnosticsInfo;

pub trait TryFromArgument: Sized {
    /// Convert 1 argument to `Self`.
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()>;

    fn expected_argument_types() -> String;
}
