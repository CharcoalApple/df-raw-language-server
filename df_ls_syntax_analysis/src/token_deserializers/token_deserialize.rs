use super::{Token, TokenValue};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

mod td_allow_empty;
mod td_bool;
mod td_char;
mod td_choose;
mod td_df_char;
mod td_int;
mod td_option;
mod td_reference;
mod td_reference_to;
mod td_string;
mod td_token;
mod td_token_value;
mod td_tuple;
mod td_vec;

#[derive(Clone, Debug)]
pub enum LoopControl {
    DoNothing,
    Break,
    Continue,
    ErrBreak,
}

impl Default for LoopControl {
    fn default() -> Self {
        LoopControl::DoNothing
    }
}

pub trait TokenDeserialize: Sized
where
    Self: Default,
{
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        let mut new_self = Self::default();
        loop {
            let node = cursor.node();
            match node.kind().as_ref() {
                "general_token" => {
                    match Self::deserialize_general_token(
                        &mut cursor,
                        &source,
                        &mut diagnostics,
                        new_self,
                    ) {
                        (LoopControl::DoNothing, new_self_result) => {
                            // Do nothing
                            new_self = new_self_result;
                        }
                        (LoopControl::Break, new_self_result) => {
                            new_self = new_self_result;
                            break;
                        }
                        (LoopControl::Continue, new_self_result) => {
                            new_self = new_self_result;
                            // Only go to next sibling if there is one, if none: break.
                            // We have reached the end of the file, so have to go up the stack
                            let new_node = cursor.node();
                            if new_node.next_sibling().is_none() {
                                cursor.goto_parent();
                                break;
                            }
                            // If node did not change: break
                            // This will prevent infinite loops
                            if new_node == node {
                                break;
                            }
                            continue;
                        }
                        (LoopControl::ErrBreak, _new_self_result) => {
                            return Err(());
                        }
                    }
                }
                "comment" => {
                    if Token::consume_token(&mut cursor).is_err() {
                        break;
                    }
                }
                "EOF" => break,
                others => {
                    error!("Found an unknown node of kind: {}", others);
                    break;
                }
            }
            // If node did not change: break
            // This will prevent infinite loops
            let new_node = cursor.node();
            if new_node == node {
                break;
            }
        }
        Ok(new_self)
    }

    fn deserialize_general_token(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self);

    fn get_allowed_tokens() -> Option<Vec<TokenValue>>;

    /// Should return `Continue` in most cases,
    /// `DoNothing` in case of String, i32, Tuples and type likes that
    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::Continue
    }
}
