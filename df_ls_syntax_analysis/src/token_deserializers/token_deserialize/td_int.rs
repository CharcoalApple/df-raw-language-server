use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::TokenDeserialize;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Prevent having to copy post following code with little changes
// for each of the integer types
macro_rules! token_de_integer {
    ( $x:ty ) => {
        // Deserialize a token with following pattern: `[REF:INT]`
        crate::token_deserialize_unary_token!($x);
    };
}

// Big numbers like i128 should not be used.
// token_de_integer!(i128);
token_de_integer!(i64);
token_de_integer!(i32);
token_de_integer!(i16);
token_de_integer!(i8);

// Big numbers like u128 should not be used.
// token_de_integer!(u128);
// Can not cast i64 to u64 without loss
// token_de_integer!(u64);
token_de_integer!(u32);
token_de_integer!(u16);
token_de_integer!(u8);

// ------------------------- Convert a group of arguments to Self -----------------------

// Prevent having to copy post following code with little changes
// for each of the integer types
macro_rules! try_from_argument_group {
    ( $x:ty ) => {
        impl TryFromArgumentGroup for $x {
            fn try_from_argument_group(
                token: &mut Token,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
                add_diagnostics_on_err: bool,
            ) -> Result<Self, ()> {
                token.check_as_required_argument(
                    source,
                    &mut diagnostics,
                    add_diagnostics_on_err,
                )?;
                let arg = token.get_current_arg_opt();
                let result =
                    Self::try_from_argument(arg, source, &mut diagnostics, add_diagnostics_on_err);
                token.consume_argument();
                result
            }
        }
    };
}

// try_from_argument_group!(i128);
try_from_argument_group!(i64);
try_from_argument_group!(i32);
try_from_argument_group!(i16);
try_from_argument_group!(i8);

// try_from_argument_group!(u128);
// Can not cast i64 to u64 without loss
// try_from_argument_group!(u64);
try_from_argument_group!(u32);
try_from_argument_group!(u16);
try_from_argument_group!(u8);

// -------------------------Convert one argument to Self -----------------------

// Prevent having to copy post following code with little changes
// for each of the integer types
macro_rules! token_value_into_integer {
    ( $x:ty, $t:literal ) => {
        impl TryFromArgument for $x {
            fn try_from_argument(
                arg_opt: Option<&Argument>,
                _source: &str,
                diagnostics: &mut DiagnosticsInfo,
                add_diagnostics_on_err: bool,
            ) -> Result<Self, ()> {
                if let Some(arg) = arg_opt {
                    match arg.value {
                        TokenValue::TVInteger(i64_value) => {
                            if i64_value > (<$x>::MAX as i64) {
                                if add_diagnostics_on_err {
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg.node.get_range(),
                                            message_template_data: hash_map! {
                                                "max_value" => <$x>::MAX.to_string(),
                                            },
                                        },
                                        "too_large_int",
                                    );
                                }
                                return Err(());
                            }
                            if i64_value < (<$x>::MIN as i64) {
                                if add_diagnostics_on_err {
                                    diagnostics.add_message(
                                        DMExtraInfo {
                                            range: arg.node.get_range(),
                                            message_template_data: hash_map! {
                                                "min_value" => <$x>::MIN.to_string(),
                                            },
                                        },
                                        "too_small_int",
                                    );
                                }
                                return Err(());
                            }
                            Ok(i64_value as $x)
                        }
                        _ => {
                            if add_diagnostics_on_err {
                                diagnostics.add_message(
                                    DMExtraInfo {
                                        range: arg.node.get_range(),
                                        message_template_data: hash_map! {
                                            "expected_parameters" => Self::expected_argument_types(),
                                            "found_parameters" => argument_to_token_name(&arg.value),
                                        },
                                    },
                                    "wrong_arg_type",
                                );
                            }
                            Err(())
                        }
                    }
                } else {
                    Err(())
                }
            }

            fn expected_argument_types() -> String {
                format!("Integer ({})", $t)
            }
        }
    };
}

// token_value_into_integer!(i128);
token_value_into_integer!(i64, "i64");
token_value_into_integer!(i32, "i32");
token_value_into_integer!(i16, "i16");
token_value_into_integer!(i8, "i8");

// token_value_into_integer!(u128);
// Can not cast i64 to u64 without loss
// token_value_into_integer!(u64);
token_value_into_integer!(u32, "u32");
token_value_into_integer!(u16, "u16");
token_value_into_integer!(u8, "u8");

// -------------------------Convert from TokenValue -----------------------

macro_rules! token_value_from_integer {
    ( $x:ty ) => {
        impl From<$x> for TokenValue {
            fn from(item: $x) -> TokenValue {
                TokenValue::TVInteger(item as i64)
            }
        }

        impl From<Option<$x>> for TokenValue {
            fn from(item: Option<$x>) -> TokenValue {
                match item {
                    Some(v) => TokenValue::TVInteger(v as i64),
                    None => TokenValue::TVEmpty,
                }
            }
        }
    };
}

// token_value_from_integer!(i128);
token_value_from_integer!(i64);
token_value_from_integer!(i32);
token_value_from_integer!(i16);
token_value_from_integer!(i8);

// token_value_from_integer!(u128);
// Can not cast i64 to u64 without loss
// token_value_from_integer!(u64);
token_value_from_integer!(u32);
token_value_from_integer!(u16);
token_value_from_integer!(u8);

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_integer_correct() {
        let source = "header
            [REF:127]
            [REF:-127]
            [REF:-20]
            [REF:1555]
            [REF:4294967295]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test i8 ---
        // Deserialize the AST
        let test_i8: i8 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(127i8, test_i8);

        // ---- Test i8 as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i8_t: (i8,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((-127i8,), test_i8_t);

        // ---- Test i16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i16: i16 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(-20i16, test_i16);

        // ---- Test u16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u16: u16 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(1555u16, test_u16);

        // ---- Test u32 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u32: u32 =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(4294967295u32, test_u32);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_integer_overflow() {
        let source = "header
            [REF:130]
            [REF:-130]
            [REF:-32769]
            [REF:-1]
            [REF:4294967296]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test i8 ---
        // Deserialize the AST
        let test_i8: Option<i8> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i8);

        // ---- Test i8 as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i8_t: Option<(i8,)> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i8_t);

        // ---- Test i16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_i16: Option<i16> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_i16);

        // ---- Test u16 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u16: Option<u16> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_u16);

        // ---- Test u32 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u32: Option<u32> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_u32);

        test_common::assert_diagnostic_codes(
            &diagnostic_info.diagnostics,
            vec![
                "too_large_int".to_owned(),
                "too_small_int".to_owned(),
                "too_small_int".to_owned(),
                "too_small_int".to_owned(),
                "too_large_int".to_owned(),
            ],
        );
    }
}
