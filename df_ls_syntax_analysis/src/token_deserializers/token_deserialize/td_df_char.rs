use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::TokenDeserialize;
use df_ls_core::DFChar;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Deserialize a token with following pattern: `[REF:char/u8]` or `[REF:'c'/125]`
crate::token_deserialize_unary_token!(DFChar);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for DFChar {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, &mut diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, &mut diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for DFChar {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenValue::TVCharacter(v) => Ok(DFChar(v)),
                TokenValue::TVInteger(value_i64) => {
                    if value_i64 > (u8::MAX as i64) {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "max_value" => u8::MAX.to_string(),
                                    },
                                },
                                "too_large_int",
                            );
                        }
                        return Err(());
                    }
                    if value_i64 < (u8::MIN as i64) {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "min_value" => u8::MIN.to_string(),
                                    },
                                },
                                "too_small_int",
                            );
                        }
                        return Err(());
                    }
                    let value_u8: u8 = value_i64 as u8;
                    match df_cp437::convert_cp437_to_utf8_char(value_u8) {
                        Ok(value) => Ok(DFChar(value)),
                        Err(err) => {
                            error!("DFChar error: {}", err);
                            Err(())
                        }
                    }
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => argument_to_token_name(&arg.value),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "Integer (u8) or char".to_owned()
    }
}

// -------------------------Convert from TokenValue -----------------------

impl From<DFChar> for TokenValue {
    fn from(item: DFChar) -> TokenValue {
        TokenValue::TVCharacter(item.0)
    }
}

impl From<Option<DFChar>> for TokenValue {
    fn from(item: Option<DFChar>) -> TokenValue {
        match item {
            Some(v) => TokenValue::TVCharacter(v.0),
            None => TokenValue::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_df_char_correct() {
        test_common::setup_logger();
        let source = "header
            [REF:'c']
            [REF:''']
            [REF:'\"']
            [REF:'0']
            [REF:'P']
            [REF:'#']
            [REF:'*']
            [REF:'`']";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test 'c' ---
        // Deserialize the AST
        let test_c: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('c'), test_c);

        // ---- Test ''' as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_sq: (DFChar,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((DFChar('\''),), test_sq);

        // ---- Test '"' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_dq: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('\"'), test_dq);

        // ---- Test '0' (zero, number) ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_0: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('0'), test_0);

        // ---- Test 'P' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_p: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('P'), test_p);

        // ---- Test '#' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_hash: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('#'), test_hash);

        // ---- Test '*' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_star: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('*'), test_star);

        // ---- Test '`' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_bt: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('`'), test_bt);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_df_char_number_correct() {
        test_common::setup_logger();
        let source = "header
            [REF:99] 'c'
            [REF:39] '''
            [REF:34] '\"'
            [REF:48] '0'
            [REF:80] 'P'
            [REF:35] '#'
            [REF:42] '*'
            [REF:96] '`'
            [REF:184] '╕'
            [REF:219] '█'
            [REF:231] 'τ'
            [REF:1] '☺'
            [REF:12] '♀'
            ";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test 'c' ---
        // Deserialize the AST
        let test_c: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('c'), test_c);

        // ---- Test ''' as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_sq: (DFChar,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((DFChar('\''),), test_sq);

        // ---- Test '"' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_dq: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('\"'), test_dq);

        // ---- Test '0' (zero, number) ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_0: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('0'), test_0);

        // ---- Test 'P' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_p: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('P'), test_p);

        // ---- Test '#' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_hash: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('#'), test_hash);

        // ---- Test '*' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_star: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('*'), test_star);

        // ---- Test '`' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_bt: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('`'), test_bt);

        // ---- Test '╕' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_ch184: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('╕'), test_ch184);

        // ---- Test '█' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_ch219: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('█'), test_ch219);

        // ---- Test 'τ' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_ch231: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('τ'), test_ch231);

        // ---- Test '☺' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_ch1: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('☺'), test_ch1);

        // ---- Test '♀' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_ch12: DFChar =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(DFChar('♀'), test_ch12);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
