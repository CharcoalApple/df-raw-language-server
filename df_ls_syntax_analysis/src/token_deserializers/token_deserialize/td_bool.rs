use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::TokenDeserialize;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Deserialize a token with following pattern: `[REF:bool]` or `[REF:1]`
crate::token_deserialize_unary_token!(bool);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for bool {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, &mut diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, &mut diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for bool {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenValue::TVInteger(v) => match v {
                    0 => Ok(false),
                    1 => Ok(true),
                    x if x > 1 => {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "max_value" => "1".to_owned(),
                                    },
                                },
                                "too_large_int",
                            );
                        }
                        Err(())
                    }
                    _ => {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "min_value" => "0".to_owned(),
                                    },
                                },
                                "too_small_int",
                            );
                        }
                        Err(())
                    }
                },
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => argument_to_token_name(&arg.value),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "bool".to_owned()
    }
}

// -------------------------Convert from TokenValue -----------------------

impl From<bool> for TokenValue {
    fn from(item: bool) -> TokenValue {
        TokenValue::TVInteger(match item {
            false => 0,
            true => 1,
        })
    }
}

impl From<Option<bool>> for TokenValue {
    fn from(item: Option<bool>) -> TokenValue {
        match item {
            Some(v) => TokenValue::TVInteger(match v {
                false => 0,
                true => 1,
            }),
            None => TokenValue::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_bool_correct() {
        test_common::setup_logger();
        let source = "header
            [REF:1]
            [REF:0]
            [REF:1]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test 1 ---
        // Deserialize the AST
        let test_1: bool =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert!(test_1);

        // ---- Test 0 ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_0: bool =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert!(!test_0);

        // ---- Test 1 as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_1t: (bool,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((true,), test_1t);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_bool_integer_overflow() {
        let source = "header
            [REF:5]
            [REF:-1]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test bool ---
        // Deserialize the AST
        let test_too_large_int: Option<bool> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_too_large_int);

        // ---- Test bool ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_too_small_int: Option<bool> = match TokenDeserialize::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(value) => Some(value),
            Err(_) => None,
        };
        assert_eq!(None, test_too_small_int);

        test_common::assert_diagnostic_codes(
            &diagnostic_info.diagnostics,
            vec!["too_large_int".to_owned(), "too_small_int".to_owned()],
        );
    }
}
