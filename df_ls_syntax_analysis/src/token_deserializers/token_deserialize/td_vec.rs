use super::super::{Token, TryFromArgumentGroup};
use super::{LoopControl, TokenDeserialize, TokenValue};
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a list of tokens
impl<T> TokenDeserialize for Vec<T>
where
    T: TokenDeserialize + Default + std::fmt::Debug,
{
    /// Function is same as `token_deserialize` default function
    /// Except for storing the first tokens Reference.
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        let mut new_self = Self::default();
        // Start special code
        let mut token_reference = None;
        // End special code
        loop {
            let node = cursor.node();
            match node.kind().as_ref() {
                "general_token" => {
                    // Start special code
                    if token_reference.is_none() {
                        // Set `token_reference` to the name of the token
                        let token =
                            Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
                        token_reference = match token.get_current_arg_opt() {
                            Some(arg) => Some(arg.value.clone()),
                            None => Some(TokenValue::default()),
                        }
                    }
                    // End special code
                    if let Some(token_reference) = &token_reference {
                        match deserialize_general_token_custom::<T>(
                            &mut cursor,
                            &source,
                            &mut diagnostics,
                            new_self,
                            token_reference,
                        ) {
                            (LoopControl::DoNothing, new_self_result) => {
                                // Do nothing
                                new_self = new_self_result;
                            }
                            (LoopControl::Break, new_self_result) => {
                                new_self = new_self_result;
                                break;
                            }
                            (LoopControl::Continue, new_self_result) => {
                                new_self = new_self_result;
                                // Only go to next sibling if there is one, if none: break.
                                // We have reached the end of the file, so have to go up the stack
                                let new_node = cursor.node();
                                if new_node.next_sibling().is_none() {
                                    cursor.goto_parent();
                                    break;
                                }
                                // If node did not change: break
                                // This will prevent infinite loops
                                if new_node == node {
                                    break;
                                }
                                continue;
                            }
                            (LoopControl::ErrBreak, _new_self_result) => {
                                return Err(());
                            }
                        }
                    } else {
                        unreachable!("token_reference not set");
                    }
                }
                "comment" => {
                    if Token::consume_token(cursor).is_err() {
                        break;
                    }
                }
                "EOF" => break,
                others => {
                    error!("Found an unknown node of kind: {}", others);
                    break;
                }
            }
            // If node did not change: break
            // This will prevent infinite loops
            let new_node = cursor.node();
            if new_node == node {
                break;
            }
        }
        Ok(new_self)
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (T::get_vec_loopcontrol(), new_self)
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

fn deserialize_general_token_custom<T: TokenDeserialize>(
    mut cursor: &mut TreeCursor,
    source: &str,
    mut diagnostics: &mut DiagnosticsInfo,
    mut new_self: Vec<T>,
    token_reference: &TokenValue,
) -> (LoopControl, Vec<T>) {
    // Needs to check if next token has the right reference
    let token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics).unwrap();
    // This does not consume the token, this is done when parameters are stored.
    let current_reference = match token.get_current_arg_opt() {
        Some(arg) => &arg.value,
        None => {
            // The Cursor is at a token that does not start with a Reference
            // This token can not be parsed, go to next
            Token::consume_token(&mut cursor).expect("Token does not have a next sibling");
            return (LoopControl::Continue, new_self);
        }
    };
    let allowed_tokens = T::get_allowed_tokens();
    let mut token_allowed = false;
    // Check if token is allowed in this loop
    if let Some(allowed_tokens) = &allowed_tokens {
        debug!(
            "Vec<T> check {:?} with allow list: {:?}",
            current_reference, allowed_tokens
        );
        // This is used by all `Struct` and `Enum` (but not `enum_value`, see #61) types
        for allowed_token in allowed_tokens {
            if current_reference == allowed_token {
                token_allowed = true;
                break;
            }
        }
    } else {
        debug!(
            "Vec<T> check {:?} with token ref: {:?}",
            current_reference, token_reference
        );
        // If all tokens allowed, make sure all tokens have the
        // same first token reference aka first argument
        // This is used for `Vec<Reference>` and similar
        if current_reference == token_reference {
            token_allowed = true;
        }
    }

    if !token_allowed {
        if new_self.is_empty() {
            return (LoopControl::ErrBreak, new_self);
        } else {
            return (LoopControl::Break, new_self);
        }
    }

    let value = TokenDeserialize::deserialize_tokens(&mut cursor, &source, &mut diagnostics);
    if let Ok(value) = value {
        new_self.push(value);
        let loop_control = T::get_vec_loopcontrol();
        // String, i32, Tuple and type likes that => DoNothing
        // Other type => Continue
        (loop_control, new_self)
    } else {
        // Else message should be already added to diagnostics
        // if nothing changed (added), do error break.
        if new_self.is_empty() {
            (LoopControl::ErrBreak, new_self)
        } else {
            (LoopControl::Break, new_self)
        }
    }
}

// ------------------------- Convert a group of arguments to Self -----------------------

/// Parse all left over arguments, no arguments required.
/// This is used to parse infinite amount of arguments
impl<T> TryFromArgumentGroup for Vec<T>
where
    T: TryFromArgumentGroup,
{
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut result = vec![];
        while let Some(_arg) = token.get_current_arg_opt() {
            result.push(T::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?);
        }
        Ok(result)
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    // use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_vec_string_correct() {
        let source = "header
            [REF:some string]
            [REF:some other string]
            [REF:short]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        let test1: Vec<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            test1,
            vec![
                "some string".to_owned(),
                "some other string".to_owned(),
                "short".to_owned()
            ]
        );

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_vec_string_other_ref() {
        let source = "header
            [REF:some string]
            [REF:some other string]
            [REFB:short]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        let test1: Vec<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(
            test1,
            vec!["some string".to_owned(), "some other string".to_owned(),]
        );
        // The `[REFB:short]` has not been deserialized yet
        let test2: String =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(test2, "short".to_owned());

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_vec_u8_try_from_argument_group_correct() {
        let source = "header
            [REF:8:5:152:0:26:96]
            [REF:5]
            [REF]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        let test1: Option<Vec<u8>> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(test1, Some(vec![8u8, 5, 152, 0, 26, 96]));

        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test2: Option<Vec<u8>> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(test2, Some(vec![5u8]));

        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test3: (Vec<u8>,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(test3, (Vec::<u8>::new(),));

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
