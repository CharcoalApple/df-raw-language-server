use super::super::{argument_to_token_name, Token, TokenValue, TryFromArgumentGroup};
use super::{LoopControl, TokenDeserialize};
use df_ls_core::Choose;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

/// Deserialize a token with following pattern: `[REF:Choose<T1,T2>]`
impl<T1, T2> TokenDeserialize for Choose<T1, T2>
where
    T1: Default + TryFromArgumentGroup,
    T2: Default + TryFromArgumentGroup,
{
    fn deserialize_tokens(
        mut cursor: &mut TreeCursor,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
    ) -> Result<Self, ()> {
        // Get arguments from token
        let mut token = Token::deserialize_tokens(&mut cursor, &source, &mut diagnostics)?;
        Token::consume_token(&mut cursor)?;
        // Arg 0
        token.check_token_arg0(source, &mut diagnostics, true)?;
        token.consume_argument();
        // Arg 1
        let result = Self::try_from_argument_group(&mut token, source, &mut diagnostics, true);
        token.check_all_arg_consumed(source, &mut diagnostics, true)?;
        result
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        new_self: Self,
    ) -> (LoopControl, Self) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens() -> Option<Vec<TokenValue>> {
        None
    }
}

// ------------------------- Convert a group of arguments to Self -----------------------

impl<T1, T2> TryFromArgumentGroup for Choose<T1, T2>
where
    T1: TryFromArgumentGroup,
    T2: TryFromArgumentGroup,
{
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        let mut token_clone1 = token.clone();
        let mut token_clone2 = token.clone();
        // Check which option finished without errors
        let option1 =
            T1::try_from_argument_group(&mut token_clone1, source, &mut diagnostics, false);
        let option2 =
            T2::try_from_argument_group(&mut token_clone2, source, &mut diagnostics, false);

        if option1.is_ok() {
            // Use option 1, even if option2 is valid
            // Add potential error messages
            let result = T1::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;
            Ok(Choose::Choice1(result))
        } else if option2.is_ok() {
            // Add potential error messages
            let result = T2::try_from_argument_group(
                &mut token,
                source,
                &mut diagnostics,
                add_diagnostics_on_err,
            )?;
            Ok(Choose::Choice2(result))
        } else {
            if add_diagnostics_on_err {
                let current_arg = token.get_current_arg()?;
                diagnostics.add_message(
                    DMExtraInfo {
                        range: current_arg.node.get_range(),
                        message_template_data: hash_map! {
                            "expected_parameters" => "One or multiple arguments".to_owned(),
                            "found_parameters" => argument_to_token_name(&current_arg.value),
                        },
                    },
                    "wrong_arg_type",
                );
            }
            token.consume_argument();
            Err(())
        }
    }
}

// -------------------------Convert one argument to Self -----------------------
// TryFromArgument is not used, use TryFromArgumentGroup instead

// -------------------------Convert from TokenValue -----------------------

impl<T1, T2> From<Choose<T1, T2>> for TokenValue
where
    TokenValue: From<T1>,
    TokenValue: From<T2>,
{
    fn from(item: Choose<T1, T2>) -> TokenValue {
        match item {
            Choose::Choice1(v) => TokenValue::from(v),
            Choose::Choice2(v) => TokenValue::from(v),
        }
    }
}

impl<T1, T2> From<Option<Choose<T1, T2>>> for TokenValue
where
    TokenValue: From<T1>,
    TokenValue: From<T2>,
{
    fn from(item: Option<Choose<T1, T2>>) -> TokenValue {
        match item {
            Some(item) => match item {
                Choose::Choice1(v) => TokenValue::from(v),
                Choose::Choice2(v) => TokenValue::from(v),
            },
            None => TokenValue::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_choose_correct() {
        let source = "header
            [REF:string]
            [REF:83]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test String -> Choose<u8, String> ---
        // Deserialize the AST
        let test_string: Choose<u8, String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(Choose::Choice2("string".to_owned()), test_string);

        // ---- Test u8 -> Choose<u8, String> ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u8: Choose<u8, String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(Choose::Choice1(83u8), test_u8);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_choose_enum_correct() {
        let source = "header
            [REF:string]
            [REF:83]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test String -> Choose<u8, String> ---
        // Deserialize the AST
        let test_string: (Choose<u8, String>,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((Choose::Choice2("string".to_owned()),), test_string);

        // ---- Test u8 -> Choose<u8, String> ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_u8: (Choose<u8, String>,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!((Choose::Choice1(83u8),), test_u8);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
