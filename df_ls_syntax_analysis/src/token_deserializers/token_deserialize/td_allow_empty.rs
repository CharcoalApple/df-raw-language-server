use super::super::{Token, TokenValue, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::AllowEmpty;
use df_ls_diagnostics::DiagnosticsInfo;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Deserialize a token with following pattern: `[REF:AllowEmpty<T>]`
crate::token_deserialize_unary_token!(AllowEmpty<T>, T);

// ------------------------- Convert a group of arguments to Self -----------------------

impl<T> TryFromArgumentGroup for AllowEmpty<T>
where
    T: TryFromArgumentGroup,
{
    fn try_from_argument_group(
        mut token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, &mut diagnostics, add_diagnostics_on_err)?;
        let arg_opt = token.get_current_arg_opt();
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenValue::TVEmpty => {
                    token.consume_argument();
                    Ok(AllowEmpty::None)
                }
                _ => {
                    match T::try_from_argument_group(
                        &mut token,
                        source,
                        &mut diagnostics,
                        add_diagnostics_on_err,
                    ) {
                        Ok(value) => Ok(AllowEmpty::Some(value)),
                        Err(_) => Err(()),
                    }
                }
            }
        } else {
            token.consume_argument();
            Err(())
        }
    }
}

// -------------------------Convert one argument to Self -----------------------
// TryFromArgument is not used, use TryFromArgumentGroup instead

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_allow_empty_string_correct() {
        let source = "header
            [REF:optional string]
            [REF:]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        let test1: AllowEmpty<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(AllowEmpty::Some("optional string".to_owned()), test1);

        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test2: AllowEmpty<String> =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(AllowEmpty::None, test2);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }

    #[test]
    fn test_allow_empty_string_to_many_args() {
        let source = "header
            [REF::]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match AllowEmpty::<String>::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_number".to_owned()]);
    }

    #[test]
    fn test_allow_empty_string_to_few_args() {
        let source = "header
            [REF]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match AllowEmpty::<String>::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_number".to_owned()]);
    }

    #[test]
    fn test_allow_empty_string_wrong_arg_type() {
        let source = "header
            [REF:56]";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // Deserialize the AST
        match AllowEmpty::<String>::deserialize_tokens(
            &mut tree_cursor,
            &source,
            &mut diagnostic_info,
        ) {
            Ok(_) => panic!("This should be an error, got correct result"),
            Err(_) => {}
        }

        let diagnostic_list = diagnostic_info.diagnostics;
        test_common::assert_diagnostic_codes(&diagnostic_list, vec!["wrong_arg_type".to_owned()]);
    }
}
