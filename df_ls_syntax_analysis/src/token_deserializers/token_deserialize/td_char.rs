use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::TokenDeserialize;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Deserialize a token with following pattern: `[REF:char]` or `[REF:'c']`
crate::token_deserialize_unary_token!(char);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for char {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, &mut diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, &mut diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for char {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenValue::TVCharacter(v) => Ok(v),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => argument_to_token_name(&arg.value),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "char".to_owned()
    }
}

// -------------------------Convert from TokenValue -----------------------

impl From<char> for TokenValue {
    fn from(item: char) -> TokenValue {
        TokenValue::TVCharacter(item)
    }
}

impl From<Option<char>> for TokenValue {
    fn from(item: Option<char>) -> TokenValue {
        match item {
            Some(v) => TokenValue::TVCharacter(v),
            None => TokenValue::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::super::super::test_common;
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_char_correct() {
        test_common::setup_logger();
        let source = "header
            [REF:'c']
            [REF:''']
            [REF:'\"']
            [REF:'0']
            [REF:'P']
            [REF:'#']
            [REF:'*']
            [REF:'`']";
        // Parse Source to AST
        let (tree, diagnostic_list_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
        println!("Lexer: {:#?}", diagnostic_list_lexer);
        assert_eq!(diagnostic_list_lexer, vec![]);
        let mut tree_cursor = tree.walk();
        // go to "header"
        tree_cursor.goto_first_child();
        // go to "comment"
        tree_cursor.goto_next_sibling();
        // go to "general_token"
        tree_cursor.goto_next_sibling();
        let mut diagnostic_info = DiagnosticsInfo::default();
        println!("{}", tree.root_node().to_sexp(0));

        // ---- Test 'c' ---
        // Deserialize the AST
        let test_c: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('c', test_c);

        // ---- Test ''' as Tuple ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_sq: (char,) =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!(('\'',), test_sq);

        // ---- Test '"' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_dq: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('\"', test_dq);

        // ---- Test '0' (zero, number) ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_0: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('0', test_0);

        // ---- Test 'P' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_p: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('P', test_p);

        // ---- Test '#' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_hash: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('#', test_hash);

        // ---- Test '*' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_star: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('*', test_star);

        // ---- Test '`' ---
        // skip "comment", go to "general_token"
        tree_cursor.goto_next_sibling();
        let test_bt: char =
            TokenDeserialize::deserialize_tokens(&mut tree_cursor, &source, &mut diagnostic_info)
                .expect("Failed to deserialize token");
        assert_eq!('`', test_bt);

        assert_eq!(diagnostic_info.diagnostics, vec![]);
    }
}
