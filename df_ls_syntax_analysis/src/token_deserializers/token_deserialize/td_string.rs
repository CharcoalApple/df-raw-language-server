use super::super::{
    argument_to_token_name, Argument, Token, TokenValue, TryFromArgument, TryFromArgumentGroup,
};
use super::TokenDeserialize;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};

// Deserialize a token with following pattern: `[REF:STRING]`
crate::token_deserialize_unary_token!(String);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for String {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        mut diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, &mut diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, &mut diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for String {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenValue::TVString(v) => Ok(v.clone()),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => argument_to_token_name(&arg.value),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "String".to_owned()
    }
}

// -------------------------Convert from TokenValue -----------------------

impl From<String> for TokenValue {
    fn from(item: String) -> TokenValue {
        TokenValue::TVString(item)
    }
}

impl From<Option<String>> for TokenValue {
    fn from(item: Option<String>) -> TokenValue {
        match item {
            Some(v) => TokenValue::TVString(v),
            None => TokenValue::TVEmpty,
        }
    }
}
