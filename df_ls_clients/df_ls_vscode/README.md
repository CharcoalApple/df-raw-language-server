# Dwarf Fortress RAW VSCode extension
[![Discord](https://img.shields.io/discord/762761192510455850?logo=discord)](https://discord.gg/6eKf5ZY)

This is a VSCode extension for Dwarf Fortress RAW files.
This project uses the [DF Raw Language Server][DF_RAW_LS].

## Features

Syntax highlighting and error reporting in
[most DF Raw files](https://gitlab.com/df-modding-tools/df-raw-language-server#syntax).

**NOTE: This extension is currently still in development,
so don't expect this to work perfectly (yet).**

![general](image/general.png)

## Known Issues

This extension is currently still in development and does not support all RAW files yet.
There are also known bugs with specific parts of the language server.
For more info about known bug or to report bugs, [see out bug tracker][Bug_tracker].

If you have questions join our [Discord][Discord].

## Manually install the extension

### Linux & Mac OS
To start using this extension with VS Code, copy it into the 
`<user home>/.vscode/extensions` folder and restart Code.

### Windows
To start using this extension with VS Code, copy it into the 
`%USERPROFILE%\.vscode\extensions` folder and restart Code.

## Contribute

If you want to contribute, join our [Discord][Discord].

## License

The code in this project is licensed under the MIT or Apache 2.0 license.

For more info about the licensing for the language server see:
https://gitlab.com/df-modding-tools/df-raw-language-server

All contributions to this project will be similarly licensed.

[DF_RAW_LS]: https://gitlab.com/df-modding-tools/df-raw-language-server
[Bug_tracker]: https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues
[Discord]: https://discord.gg/6eKf5ZY