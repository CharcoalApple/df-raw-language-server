# Dwarf Fortress RAW language server
[![Discord](https://img.shields.io/discord/762761192510455850?logo=discord)](https://discord.gg/6eKf5ZY)

[![pipeline status](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/pipeline.svg)](https://gitlab.com/df-modding-tools/df-raw-language-server/-/commits/master)
[![coverage report](https://gitlab.com/df-modding-tools/df-raw-language-server/badges/master/coverage.svg)](https://codecov.io/gl/df-modding-tools/df-raw-language-server)

<!--
Extensions:
[![VSCode extension](https://img.shields.io/visual-studio-marketplace/i/df-modding-tools.dwarf-fortress-raw?label=VSCode%20Installs&logo=visual-studio-code)][VSCode_Marketplace]
-->

Dwarf Fortress RAW language server is an application that allows
[Dwarf Fortress RAW files](https://dwarffortresswiki.org/index.php/Raw_file)
to be checked for correctness.
This allows modders for game [Dwarf Fortress][Dwarf_Fortress] to detect problems in there mod files.

This repo is work in progress and might not work in some cases.

We are currently in the following [pre release stage](#pre_release_stage): **Closed Alpha**.<br/>
Latest version: [`0.2.0-alpha.1` (2021-08-01)][Development_build]

## What can it do?

Syntax Highlighting and Error reporting:<br />
![general](image/general.png)

<details>
<summary><b>View more Features and Screenshots</b></summary>

1. It reads a DF Raw file and add Syntax Highlighting:<br />
![syntax_highlighting](image/syntax_highlighting.png)

1. And works correctly with your theme:<br />
![syntax_highlighting](image/theme2.png)
![syntax_highlighting](image/theme3.png)

1. It checks if all the tokens are correct and have the correct arguments:<br />
![wrong_arg_type](image/wrong_arg_type.png)<br /><br />
![wrong_enum_value](image/wrong_enum_value.png)<br /><br />
![wrong_arg_number_to_many](image/wrong_arg_number_to_many.png)

1. It warns you of about best practices:<br />
![warnings](image/warnings.png)

And much more to come!
</details>

### Future plans

We have many things we still want to add.
If you want to see what we have planned check out our [Roadmap](Roadmap.md).
But adding these features might take some time. [So all help is welcome!](#contribute)

## Supported Editors and IDEs

We are currently only supporting [VS Code](https://code.visualstudio.com/).
But the language server is able to work on other Editors and IDEs.

- ✅: Working and tested
- 👥: Community member creation (not part of this repo or group)
- 🛠: Work in progress, partly implemented
- ❌: Not Implemented
- ⛔: Known to not (yet) support language servers

| IDE                                                      | Status & Download |
| -------------------------------------------------------- | ----------------- |
| [VS Code](https://code.visualstudio.com/)                | ✅ [Download][VSCode_Marketplace]|
| [Visual Studio](https://visualstudio.microsoft.com/)     | ❌     |
| [Atom](https://atom.io/)                                 | ❌     |
| [Sublime Text](https://www.sublimetext.com/)             | ❌     |
| [Eclipse](https://www.eclipse.org/ide/)                  | ❌     |
| [Vim/NeoVim](https://neovim.io/)                         | ❌     |
| [Notepad++](https://notepad-plus-plus.org/)              | ⛔     |
| ... others ...                                           | ❌     |

## Implemented

- ✅: Implemented
- 🕗: Work in progress, partly implemented
- ❌: Not Implemented

| Token                         | Status  |
| ----------------------------- | ------- |
| `[OBJECT:BODY]`               | ✅      |
| `[OBJECT:BODY_DETAIL_PLAN]`   | ✅      |
| `[OBJECT:BUILDING]`           | ✅      |
| `[OBJECT:CREATURE]`           | 🕗      |
| `[OBJECT:CREATURE_VARIATION]` | ❌      |
| `[OBJECT:DESCRIPTOR_COLOR]`   | ✅      |
| `[OBJECT:DESCRIPTOR_PATTERN]` | ✅      |
| `[OBJECT:DESCRIPTOR_SHAPE]`   | ✅      |
| `[OBJECT:ENTITY]`             | ✅      |
| `[OBJECT:GRAPHICS]`           | ❌      |
| `[OBJECT:INTERACTION]`        | ✅      |
| `[OBJECT:INORGANIC]`          | ❌      |
| `[OBJECT:ITEM]`               | 🕗      |
| `[OBJECT:LANGUAGE]`           | ✅      |
| `[OBJECT:MATERIAL_TEMPLATE]`  | ✅      |
| `[OBJECT:PLANT]`              | ✅      |
| `[OBJECT:REACTION]`           | ✅      |
| `[OBJECT:TISSUE_TEMPLATE]`    | ✅      |

## Contribute
<a name="contribute"></a>

If you want to contribute, join our [Discord][Discord].
We are always looking for more people to help us develop, test, research, ...
You don't need to be a programmer or know about modding in DF to help us out.

There are a few good places to start:
- Look at the open 
[beginner issues](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues?label_name%5B%5D=Beginner)
or [research needed issues](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues?label_name%5B%5D=Research+Needed)
- Check the [`#syntax-room[1-2]`][Discord] channel for any questions that we might have in there.
- Ask in [`#development`][Discord] channel what we need help with, we would love to see yo join!
- Just test the application and report bugs, through [Discord][Discord] or 
[GitLab](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues).

## Pre-release stage
<a name="pre_release_stage"></a>
- **Closed Alpha**: Ask people in the [Discord][Discord] to install it, test it out and
see if it works. This stage is mainly to see if executable works and does not crash.
This also involves fixing tokens that are already [implemented](#Implemented).
Target amount of testers: 3+ (at least 1 Windows user).
- **Open Alpha**: Post on DF forum, and other Discords if people want to try it,
testing the currently implemented tokens and see if we missed anything in those tokens.
During this stage we will also add more tokens, like graphics, creatures, ...
We will also add support for macOS.
Target amount of testers: 5+ (at least 3 Windows users).
- **Closed Beta**: All tokens are have been added. So we need to make sure and did not miss anything.
We will also test if everything we currently have is working as expected.
Target amount of testers: 5+ (at least 1 Mac user)
- **Open Beta**: Post on reddit, discord, forum, ... and see if there are still problems or things
we missed/have not found before.
Target amount of testers: 8+ (at least 1 user on each OS)
- **Release**: Most, if not all, problems are fixed and the application is stable.

Note: "Closed" in this project means we do not actively promote it outside of our [Discord][Discord], 
but it is still publicly available, just join us on [Discord][Discord].

## Test the VSCode extension

Because we are currently testing the plugin here are some instructions that help you with installing
and debugging the plugin.

### Installing

In order to install the plugin there are 2 options:
- ~~Download the latest version through the [VS Marketplace][VSCode_Marketplace]~~ (outdated)
- Download the
[latest version](https://gitlab.com/df-modding-tools/df-raw-language-server/-/jobs/artifacts/master/raw/artifacts/dwarf-fortress-raw-vscode-latest.vsix?job=vscode_client_package)
from the [GitLab pipeline](https://gitlab.com/df-modding-tools/df-raw-language-server/-/pipelines).

#### VSCode Marketplace
~~Download the latest version through the [VS Marketplace][VSCode_Marketplace]~~.<br/>
Note: This version is old and does not include the language server.
It only includes syntax highlighting.

The marketplace will be used after the Open Alpha stage starts.

#### Latest build
Download the [latest version][Development_build].
This is generated using out build pipeline and will always be the last (stable)version of
the language server. This does not automatically update, so still requires manual updates.

##### How to install
Download the zip file and unpack it, there should be a `.vsix` file inside.
This file, most likely `dwarf-fortress-raw-vscode-latest.vsix` it also an archive, 
but should not be unpacked.

Now we can install the `.vsix`:
- Open [VS Code](https://code.visualstudio.com/).
- Open the `Extensions (Ctrl+Shift+X)` -> `...` (top) -> `Install from VSIX...`.
- Select the file (`dwarf-fortress-raw-vscode-latest.vsix`) you just downloaded.
- It should show up in the list of extensions as `Dwarf Fortress RAW LS`.
- Done, now open a `.txt` or `.df` file.

![install vsix file](image/install_vsix.png)

### Debug Extension
To debug the extension there are a few options you have where you can find different info.
Open the output console at the bottom of your screen (`Ctrl + Shift + Y`) -> `Output` -> Dropdown:
 - `Log (Extension Host)` for install and activation info.
 - `Log (Window)` for process info (if server crashes).
 - `DF Raw Language Server` for info about what server responses are coming back.

Here you will find different info that might be useful for debugging any problems.
Better ways for debugging are being
[worked on](https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues/68).

## Development

NOTE: Following steps are mainly for Linux. Similar steps exist for Windows and Mac OS.
To compile and use this project you need the following:

* **Rust** (rustup + cargo) ([Linux](https://www.rust-lang.org/tools/install),
[Windows](https://www.rust-lang.org/tools/install?platform_override=win)) 
* **Node.js/NPM** [Node.js instructions](https://nodejs.org/en/)
* **TypeScript** [TypeScript intructions](https://www.npmjs.com/package/typescript)

Once you have the requirements follow the steps:
* Open the folder of the repo and open a terminal here.
* Build the Rust language server: `cargo run -- -d start`
** This should finish without any problems.
* Open project in VSCode:
  * run `code .` or
  * drag `df-raw-language-server` folder into the 'File Explorer'
* Build VSCode client
```bash
cd ./df_ls_clients/df_ls_vscode/
npm install --production=false
cd ../../
```
* Go to debug/run tab in VSCode.
* Run `Server + Client`
* A new VSCode window will open.
* Open a DF Raw File in the new window.

Run parser on the `debug-test.txt` file:
```
cargo run -- -d debug
```
This is useful for debugging 1 file with known problems. This will print a lot of extra info.

### Project structure info

This project is divided into several parts. 
Each part has its own part to play and will feed in to the next step:
- **1: Lexical Analysis**: This takes the source file and split the file into tokens. 
This step will output a `Tree` objects.
The `Tree` object is also referred to as an [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).
The Tokenizer will also report problems about the AST.
- **2: Syntax Analysis**: Also referred to as the first pass. 
This will take in the `Tree` object and the source and use this data to create a data structure. 
During this step more problems will be add to the diagnostic messages list.
- **3: Semantic Analysis**: Also referred to as the second pass. (still in development)
This step will take the data structure and check the structure so everything makes sense.
Some checks can only happen in this stage as it requires the full structure to be known.
This stage will return more diagnostic messages.

Then there are 2 more part to this project. These are more to create a functional program.
- **DF Language server**: This is the actual Language server and will create a TCP API server 
that follows the [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/).
- **DF LS Clients**: This is a collection of clients for the different 
[IDEs](https://en.wikipedia.org/wiki/Integrated_development_environment).
If more IDEs want to be supported this is the place to add them.

## License

The code in this project is licensed under the MIT or Apache 2.0 license.

All documentation[^1] is licensed under
[GNU FDL](https://www.gnu.org/licenses/fdl-1.3.html),
[MIT license](https://choosealicense.com/licenses/mit/) and/or
[Creative Commons Attribution-ShareAlike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/) 

This makes the documentation both compatible with the
[Dwarf Fortress Wiki](https://dwarffortresswiki.org) and [Wikipedia](https://www.wikipedia.org/).

All contributions, code and documentation, to this project will be similarly licensed.

[^1]: This includes all Rust Doc, documentation and other info in this codebase.

[Discord]: https://discord.gg/6eKf5ZY
[VSCode_Marketplace]: https://marketplace.visualstudio.com/items?itemName=df-modding-tools.dwarf-fortress-raw
[Dwarf_Fortress]: https://bay12games.com/dwarves/
[Development_build]: https://gitlab.com/df-modding-tools/df-raw-language-server/-/jobs/artifacts/master/raw/artifacts/dwarf-fortress-raw-vscode-latest.vsix?job=vscode_client_package